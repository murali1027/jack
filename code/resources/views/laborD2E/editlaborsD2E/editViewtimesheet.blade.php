<table class="table table-edit" align="center">
	<tr class="editfileds">
		<th class="tableheader">Job</th>
		<th class="tableheader">Cost Code</th>
		<th class="tableheader">Local</th>
		<th class="tableheader">Class</th>
		<th class="tableheader">Cert</th>
		<th class="tableheader">Date</th>
		<th class="tableheader">Shift</th>
		<th class="tableheader">Hours</th>
		<th class="tableheader">App</th>
		<th class="tableheader">Approver</th>
		<th class="tableaction" colspan="2">Action</th>
	</tr>
	@foreach($labore as $value)
		@if($value->Approved == 'R')
			<tr class="reject">
		@elseif($value->Approved == 'A')
			<tr class="approve">
		@else
			<tr>
		@endif
			    <td><span data-toggle="tooltip" class="red-tooltip" data-placement="right" title="{{$value->Job}}">{{substr($value->Job,0,10)}}</span></td>
		   		 <?php $CostCode=DB::table('ccod')->where('CostCode',$value->CostCode)->first();?>
		   		<td><span data-toggle="tooltip" class="red-tooltip" data-placement="right" title="{{$value->CostCode}} - {{$CostCode->DSCR}}">{{$value->CostCode}} - {{substr($CostCode->DSCR,0,2)}}</span></td>
				<td>{{$value->Local}}</td>
				<td>{{$value->Class}}</td>
				<td>{{$value->Cert}}</td>
				<td><?php $dayname = strtotime($value->Date);?>{{$day = date("D", $dayname)}}</td>
				<td>{{$value->Shift}}</td>
				<td>{{$value->Hours}}</td>
				<td>{{$value->Approved}}</td>
				<td>{{$value->Approver}}</td>
				@if($value->Approved == 'A')
					<td><a href="{{route('editlaborsD2E.edit',$value->Labore_ID)}}" class="btn btn-info date-days ed-btn" disabled>Edit</a></td>
					<td>
						{!! Form::open(['method' => 'DELETE', 'route'=>['editlaborsD2E.destroy', $value->Labore_ID]]) !!}
						{!! Form::submit('Delete', ['class' => 'btn btn-danger ed-btn','disabled']) !!}
						{!! Form::close() !!}
					</td>
				@else
					<td><a href="{{route('editlaborsD2E.edit',$value->Labore_ID)}}" class="btn btn-info date-days ed-btn">Edit</a></td>
					<td>
						<button type="button" class="btn btn-danger ed-btn" onclick="DeleteD2E(this.id)" id="{{$value->Labore_ID}}"
							value="{{$value->Labore_ID}}">Delete</button>
					</td>
				@endif
			</tr>
	@endforeach
</table>
	<div id="msg"><br>
<span id="norec" style="color:red;"></span>
</div>
<script>
function DeleteD2E(labore_id)
	{
		var deleted2e=labore_id;
		jQuery.ajax({
			type: "POST",
			dataType: "json",
			url: "DeleteD2E",
			data: {laboreid:deleted2e},
			success:function(data){
				var role= document.getElementById("role").value;
				if(role == 'USR'){}
				else
				{
					var empid= document.getElementById("ename").value; 
				}
				var weeks_start = $('#weeks option:selected').attr('data-id');
				var weeks_end= document.getElementById("weeks").value;
				jQuery.ajax({
					type: "POST",
					dataType: "json",
					url: "EditTimesheetD2E",
					data: {empid:empid,weeks_start:weeks_start,weeks_end:weeks_end},
					success:function(data){
						$('#clear').empty();
						$('.clear').html(data.edit);
					},
					error:function (){ 
					}
					});
			},
			error:function (){ 
			}
		});
	}
</script>
<script>
	$(document).ready(function(){
    	$('[data-toggle="tooltip"]').tooltip();   
	});
</script>