@include('includes.header')
<div class="container-fluid">
	<div class="row" >
		<div class="col-md-12">
			<div class="panel">
				<table class="table table-bordered" align="center">
					<tr>
						<td> 
							<h2 class="msg">Update Time Entry for <span class="cname">{{ trans('menus.de') }}</span></h2>
							<div class="col-md-11 ename" align="center">
								<table  class="empname" width="57.5%">
									<tr>
										<th class="week-day emp-name" colspan="8">
										    <?php $empname=DB::table('empe')->where('EmpeID',$edit->EmpeID)->first();?>
											<p class="emp-id">{{$empname->ENAME}}- {{ $edit->EmpeID }}</p>
										</th>
									</tr>
								</table><br>
							</div>
							<div class="panel-body">
								{!! Form::model($edit,['method' => 'PATCH', 'class'=>'form-horizontal', 'id'=>'edittimesheet-form',
								'route'=>['editlaborsD2E.update',$edit->LABORE_ID]])!!}
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<input type="hidden" name="empeid"  value="{{ $edit->EmpeID }}">
									<div class="form-group">
										<label class="col-sm-4 control-labels">Date</label>
										<div class="col-sm-7">
											<input type="text" class="form-control"  name="date" value="{{$edit->DATE_WORK}}" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-labels">Job</label>
										<div class="col-sm-7">
											<select name="jobid" id="jobid" class="form-control drop-box">
											@foreach($jname as $value)
												<option data-id="{{$edit->EXTRA}}" value="{{$edit->JobID}}">{{$edit->JobID}} {{$edit->EXTRA}} - 
												{{$value->JNAME}}</option>
													
											@endforeach	
											@foreach($job as $value)
												<option data-id="{{$value->EXTRA}}" value="{{$value->JobID}}">{{$value->JOB}} - {{$value->JNAME}}</option>
											@endforeach
											</select>
										</div>
									</div> 
									<div class="form-group">
										<label class="col-sm-4 control-labels">Cost Code</label>
										<div class="col-sm-7">
											<select name="ccode" id="ccode" class="form-control drop-box removesel">
												<?php $CostCode=DB::table('ccod')->where('CostCode',$edit->CostCode)->where('EXTRA',$edit->EXTRA)->first();?>
												<option value="{{$edit->CostCode}}">{{$edit->CostCode}} - {{$CostCode->DSCR}}</option>
												    @foreach($ccod as $value)
													<option value="{{$value->CostCode}}">{{$value->CostCode}}-{{$value->DSCR}}</option>
													@endforeach
											</select>
										</div>
									</div> 
									<input type="hidden" id="extra" name="extra">
									<div class="form-group">
										<label class="col-sm-4 control-labels">Union Local</label>
										<div class="col-sm-7">
											<input type="text" class="form-control drop-box" name="ulocal" id="ulocal" readonly value="{{$edit->ULOCAL}}"/>
										</div>
									</div> 
									<div class="form-group">
											<label class="col-sm-4 control-labels">Union Class</label>
											<div class="col-sm-7">
												<select name="uclass" id="uclass" class="form-control drop-box">
												<option value="{{$edit->UCLASS}}">{{$edit->UCLASS}}</option>
												@foreach($uclass as $value)
													<option value="{{$value->UCLASS}}">{{$value->UCLASS}}</option>
												@endforeach
												</select>
											</div>
										</div> 
									<div class="form-group">
										<label class="col-sm-4 control-labels">Cert Class</label>
										<div class="col-sm-7">
											<select name="certe" id="certe" class="form-control drop-box removesel">
												<option value="{{$edit->CERTE}}">{{$edit->CERTE}}</option>
												    @foreach($certe as $value)
												   <option value="{{$value->CERTE}}">{{$value->CERTE}}</option>
													@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-labels">Shift</label>
										<div class="col-sm-7">
											<select name="shift" id="shift" class="form-control drop-box">
												<option value="1">Day</option>
												<option value="2">Swing</option>
												<option value="3">Night</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-labels">Hours</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" id="hours" name="hours" value="{{$edit->HOURS}}">
										</div>
									</div>
									<div id="msg"></div>
									@if (session('message'))
									<div class="alert alert-danger">
										{{ session('message') }}
									</div>
									@endif
									<div class="form-group">
										<div class="col-md-6 col-md-offset-2">
											{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
										</div>
									</div>
									</br>
								{!! Form::close() !!}
							</div>
						</td>
					</tr>
				</table>
			</div>
			<div class="pull-left"><strong>User: </strong><span  id="user_name">{{ Auth::user()->FNAME }} {{ Auth::user()->LNAME }}</span></div>
			<div class="pull-right"><strong>Date: </strong><span  id="currentdate">{{date('m/d/Y')}}</span></div>
		</div>
	</div>
</div>
@include('includes.footer')


