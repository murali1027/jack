<div class="form-group">
	<label class="col-sm-4 control-labels">Job</label>
	<div class="col-sm-7">
		<select name="jobid" id="jobid" class="form-control drop-box" tabindex="2">
			@foreach($empid as $value)
				@if($value->JOB_DEFAULT != 0)
				   @foreach($extra as $cvalue)
						<option data-id="{{$cvalue->Extra}}" value="{{$value->JobID}}">{{$value->JobID}} {{$cvalue->Extra}} - {{$value->JNAME}}</option>
					@endforeach
				    <option value=""></option>
					@else
						<option value="" disabled selected>Select a Job</option>
				@endif
				@foreach($job as $value)
				    <option data-id="{{$value->EXTRA}}" value="{{$value->JobID}}">{{$value->JOB}} - {{$value->JNAME}}</option>
				@endforeach
			@endforeach
		</select>
	</div>
</div> 
<div class="form-group">
	<label class="col-sm-4 control-labels">Cost Code</label>
	<div class="col-sm-7">
		<select name="ccode" id="ccode" class="form-control drop-box" tabindex="3">
			<option value="" disabled selected>Select a Cost Code</option>
			@foreach($ccode as $value)
				<option value="{{$value->CostCode}}">{{$value->CostCode}} - {{$value->DSCR}}</option>
			@endforeach
		</select>
	</div>
</div> 
<div class="form-group">
	<label class="col-sm-4 control-labels">Union Local</label>
	<div class="col-sm-7">
		@forelse($ulocal as $value)
			<input type="text" class="form-control drop-box" id="ulocal" disabled value="{{$value->ULOCAL}}"/>
			@empty
			<input type="text" class="form-control drop-box" id="ulocal" disabled  value=""/>
		@endforelse
	</div>
</div> 
<div class="form-group">
	<label class="col-sm-4 control-labels">Union Class</label>
	<div class="col-sm-7">
		<select name="uclass" id="uclass" class="form-control drop-box" tabindex="4">
			@if(Auth::check())
				@if(Auth::user()->ROLE == 'USR')
					@foreach($uclass as $value)
						<option value="{{$value->UCLASS}}">{{$value->UCLASS}}</option>
					@endforeach
				@endif
			@endif
			@foreach($empid as $value)
				<option value="{{$value->UCLASS}}">{{$value->UCLASS}}</option>
			@endforeach
			<option value=""></option>
			@foreach($uclass as $value)
				<option value="{{$value->UCLASS}}">{{$value->UCLASS}}</option>
			@endforeach
		</select>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-4 control-labels">Cert Class</label>
	<div class="col-sm-7">
		<select name="certe" id="certe" class="form-control drop-box" tabindex="5">
		   @foreach($certe as $value)
			<option value="{{$value->CERTE}}">{{$value->CERTE}}</option>
			@endforeach
		</select>
	</div>
</div>  
<script>
	$('#jobid').on('change',function(e)
	{
		var job_id = e.target.value;
		var extra = $('#jobid option:selected').attr('data-id');
		$.get('PostCcod?job_id=' + job_id + '&extra='+ extra, function(data){
			$('#ccode').empty();
			$('#ccode').append('<option value="" disabled selected>Select a Cost Code</option>');
			$.each(data,function(index,subcatObj){
				$('#ccode').append('<option value="'+subcatObj.CostCode+'">'+subcatObj.CostCode+'&nbsp;'+'-' +'&nbsp;'+subcatObj.DSCR+'</option>');
			});
		});
	});
	$('#jobid').on('change',function(e)
	{
		var job_id = e.target.value;
		$.get('Postulocal?job_id=' + job_id, function(data){
			$('#ulocal').empty();
			if(data.length ==  0){
				 $('#ulocal').val("");
			}
			else{
				$.each(data,function(index,subcatObj){
				$('#ulocal').val(subcatObj.ULOCAL);
			});
			}
		});
	});
	$('#jobid').on('change',function(e)
	{
		var job_id = e.target.value;
		$.get('Postcerte?job_id=' + job_id, function(data){
			$('#certe').empty();
			$.each(data,function(index,subcatObj){
				$('#certe').append('<option value="'+subcatObj.CERTE+'">'+subcatObj.CERTE+'</option>');
			});
		});
	});
</script>