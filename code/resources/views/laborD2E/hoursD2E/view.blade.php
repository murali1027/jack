<br><br><br>
<table class="table table-hours" align="center">
	<tr class="editfileds">
	    <th class="tableheader">Employee</th>
		<th class="tableheader">Enter</th>
		<th class="tableheader">Approve</th>
		<th class="tableheader">Reject</th>
	</tr>
	@foreach($labore_hours as $value)
	<tr>
		<?php $dates=array();?>
		@foreach ($labore_date as $date)
		<?php $dates[]=$date->DATE_WORK;?>
		@endforeach
		<?php 	
			$empid=array();
			$empid[]=$value->EmpeID;
			$labore_add=DB::table('labore as l')
						->join('empe as e', 'l.EmpeID', '=', 'e.EmpeID')
						->whereIn('l.EmpeID',$empid)
						->whereIN('l.DATE_WORK',$dates)
						->where('l.DELETED', '=', 'N')
						->select(DB::raw('SUM(CASE WHEN l.APPROVED = "N" THEN l.HOURS ELSE 0 END) As HoursEntered'),
					         DB::raw('SUM(CASE WHEN l.APPROVED="A" THEN l.HOURS ELSE 0 END) As HoursApproved'),
				      	     DB::raw('SUM(CASE WHEN l.APPROVED="R" THEN l.HOURS ELSE 0 END) As HoursRejected'))
						->get();
		?>
		@foreach($labore_add as $labore)
		
			@if($labore->HoursEntered!= NULL || $labore->HoursApproved!= NULL || $labore->HoursRejected!= NULL )
				<td class="hours-name">{{$value->ENAME}}</td>
			@else
				<td class="reject hours-name">{{$value->ENAME}}</td>
			@endif
			
			@if($labore->HoursEntered!= NULL)
				<td>{{$labore->HoursEntered}}</td>
			@else
				<td class="reject">0</td>
			@endif
			
			@if($labore->HoursApproved!= NULL)
				<td>{{$labore->HoursApproved}}</td>
			@else
				<td class="reject">0</td>
			@endif
			@if($labore->HoursRejected!= NULL)
				<td>{{$labore->HoursRejected}}</td>
			@else
				<td class="reject">0</td>
			@endif
			@endforeach
	</tr>
	@endforeach
</table>


	