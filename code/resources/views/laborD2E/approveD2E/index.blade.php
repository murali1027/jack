@include('includes.header')
<h3 class="msg">Timesheet Approve and Reject for <span class="cname">{{ trans('menus.de') }}</span></h3>
<table class="table table-edit" align="center">
	<tr>
		<th class="appempid" colspan="12">
			<div class="form-group">
				<div class="col-sm-12">
				    <input type="hidden" name="role" id="role" value="{{ Auth::user()->ROLE }}">
					<input type="hidden" id="approver" value="approveD2E">
					    Approver Name: {{ Auth::user()->FNAME }} {{ Auth::user()->LNAME }} - {{ Auth::user()->EmpsID }}
				</div>
			</div>
		</th>
	</tr>
</table>
<div class="clear">
	<div id="clear">
		<table class="table table-edit" align="center">
			<tr class="editfileds">
				<th class="tableheader">Job</th>
				<th class="tableheader">Cost Code</th>
				<th class="tableheader">Local</th>
				<th class="tableheader">Class</th>
				<th class="tableheader">Cert</th>
				<th class="tableheader">Date</th>
				<th class="tableheader">Hours</th>
				<th class="tableheader">App</th>
				<th class="tableheader">Approver</th>
				<th class="tableaction" colspan="2">Action</th>
			</tr>
		</table>
	</div>
</div>
<div class="grouping"></div>
<div class="form-group">
	<div class="col-md-7">
		<select name="ename-approve" id="ename" onchange="ApproveEmpDetailD2E()" class="form-control drop-box">
			<option value="" disabled selected>Select an Employee</option>
			@foreach($empid as $value)
			<option data-id="{{$value->ENAME}}-{{ $value->EmpeID}}" value="{{$value->EmpeID}}">{{$value->ENAME}} {{"-"}} {{ $value->EmpeID}}</option>
			@endforeach
		</select><br>
	</div>
	<div class="col-md-5">
		<select name="weeks-approve" id="weeks" onchange="ApproveEmpDetailD2E()" class="form-control drop-box"></select>
	</div>
	<div class="col-md-6 col-md-offset-5">
		<input type="button" class="btn btn-primary" value="Reset" onClick="window.location.reload()">
	</div>
</div>
@include('includes.footer')
