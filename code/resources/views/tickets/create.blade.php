@include('includes.header')
<h2><center>{{trans('tables.adticket')}}</center></h2>
@if (session('status'))
	<div class="alert alert-success">
		{{ session('status') }}
	</div>
@endif
@if (count($errors) > 0)
	<div class="alert alert-danger">
		<strong>Whoops!</strong> There were some problems with your input.<br><br>
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif
{!! Form::open(['url' => 'tickets','enctype'=>'multipart/form-data']) !!}
	<div class="form-group col-xs-4">
		{!! Form::label('category', trans('tables.cat'),['class' => 'control-label header_tickets']) !!}
		{!! Form::select('category',array(''=>'Select a catagory','1'=>'Admin','2'=>'Misc','3'=>'New Hire','4'=>'Payroll','5'=>'Website'),
			null,['class' => 'form-control']) !!}
	</div> 
	<div class="form-group col-xs-10">
		{!! Form::label('description', trans('tables.desc'),['class' => 'header_tickets']) !!}<br>
		{!! Form::textarea('description',null,['class' => 'form-control desc','rows'=>'8'])!!}<br>
		{!! Form::file('file')!!}<br>
		{!! Form::submit(trans('forms.subtket'), ['class' => 'btn btn-primary']) !!}
	</div>
{!! Form::close() !!}
@include('includes.footer')