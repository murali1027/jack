@include('includes.header')
<br>
	@foreach($ticket as $val)
	    <div class="ticketOuter">
			<div class="form-group col-xs-12">
				<div class="fLeft"><label>{{trans('tables.ticno')}}</label> {{$val->TICKET_ID}}</div>
				@if($val->OPENED_BY != 0)
					<?php $admin=DB::table('users')->where('USER_ID',$val->OPENED_BY)->first(); ?>
						<div class="fCenter"><label>{{trans('tables.opby')}}</label> {{$admin->FNAME}}&nbsp;{{$admin->LNAME}}</div>
				@endif	
				@if($val->OPENED_BY == 0)
					<div class="fCenter"><label>{{trans('tables.opby')}}</label>{{$val->OPENED_BY}}</div>
				@endif
				<div class="fRight"><label>{{trans('tables.dopen')}}</label> {{$val->DATE_OPENED}}</div>
				<div>
					<textarea class="tixket-txtArea" name="solution" rows=0  disabled>{{trans('tables.pblm')}}: {{$val->PROBLEM}}</textarea><br>
				</div>
				@if($val->SOLUTION != NULL)
					<div>
						<textarea class="tixket-txtArea" name="solution" rows=0 disabled>{{trans('tables.slution')}}: {{$val->SOLUTION}}</textarea><br>
					</div>
				@endif	
				<div class="fLeft"><label>{{trans('tables.status')}}</label> {{$val->TSTATUS}}</div>
				<div class="fCenter">
					@if($val->CLOSED_BY != 0)
						<?php $admin_id=DB::table('users')->where('USER_ID',$val->CLOSED_BY)->first();?>
							<label>{{trans('tables.cloby')}}</label> {{$admin_id->FNAME}}&nbsp;{{$admin_id->LNAME}}
					@endif	
					@if($val->CLOSED_BY == 0)
							<label class="header_open">{{trans('tables.cloby')}}</label> None
					@endif
				</div>
				<div class="fRight"><label>{{trans('tables.dclos')}}</label> {{$val->DATE_OPENED}}</div>
			</div>
		</div><br>
	@endforeach
@include('includes.footer')