@include('includes.header')
<h2><center>{{trans('tables.upslution')}}</center></h2>
{!! Form::model($tickets,['method' => 'PATCH', 'class'=>'form-horizontal','route'=>['tickets.update',$tickets->TICKET_ID]]) !!}
	<div class="col-md-12">
		<label class="header_tickets">{{trans('tables.pblm')}}</label><br>
		{!! Form::textarea('PROBLEM',null,['class'=>'form-control update_ticket', 'readonly']) !!}
	</div>
	<div class="col-xs-12">
		<label class="header_tickets">{{trans('tables.slution')}}</label><br>
		{!! Form::textarea('SOLUTION',null,['class'=>'form-control update_ticket']) !!}
		{!! Form::submit(trans('forms.subslu'), ['class' => 'btn btn-primary']) !!}
	</div>
{!! Form::close() !!}
 @include('includes.footer')