@include('includes.header')
<br>
<div class="table-responsive">
	<table class="table tables table-bordered ticket-td" align="center">
		<tr class="warning">
			<th>{{trans('tables.no')}}</th>
			<th>{{trans('tables.pblm')}}</th>
			<th>{{trans('tables.slution')}}</th>
			<th>{{trans('tables.attach')}}</th>
			<th>{{trans('tables.create')}}</th>
			<th>{{trans('tables.updated')}}</th>
			<th>{{trans('tables.updteby')}}</th>
			<th>{{trans('forms.action')}}</th>
		</tr>
		@if (session('status'))
			<div class="alert alert-success">
				{{ session('status') }}
			</div>
		@endif	
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		@foreach($ticket as $tickets)
			<tr>
				<td>{{$tickets->TICKET_ID}}</td>
				<td>{{str_limit($tickets->PROBLEM,150)}}</td>
				<td>{{str_limit($tickets->SOLUTION,150)}}</td>
				<td><a href="uploads/{{$tickets->ATTACH}}" target="_blank">{{$tickets->ATTACH}}</a></td>
				<td width="10%">{{$tickets->CREATED_AT}}</td>
				<td width="10%">{{$tickets->UPDATED_AT}}</td>
				<td>
					@if($tickets->CLOSED_BY != 0)
						<?php $admin=DB::table('users')->where('USER_ID',$tickets->CLOSED_BY)->first();?>
						{{$admin->FNAME}}&nbsp;{{$admin->LNAME}}
					@endif	
					@if($tickets->CLOSED_BY == 0)
						{{$tickets->CLOSED_BY}}
					@endif
				</td>
				<td><a href="{{route('tickets.edit',$tickets->TICKET_ID)}}" class="btn btn-info ed-btn date-days">{{trans('forms.edit')}}</a></td>
			</tr>
		@endforeach
	</table>
</div>
@include('includes.footer')