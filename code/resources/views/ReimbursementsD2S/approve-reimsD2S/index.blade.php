@include('includes.header')
<h3 class="msg">Reimbursements Approve and Reject for <span class="cname">{{ trans('menus.ds') }}</span></h3>
<table class="table table-edit" align="center">
	<tr>
		<th class="appempid" colspan="12">
			<div class="form-group">
				<div class="col-sm-12">
				   Approver Name: {{ Auth::user()->FNAME }} {{ Auth::user()->LNAME }} - {{ Auth::user()->EmpsID }}
				</div>
			</div>
		</th>
	</tr>
</table>
<div class="grouping"></div>
@forelse($emps as $empvalue)
	<?php 
	    $mailid  = Auth::user()->EMAIL;  
		$user = strstr($mailid, '@', true);  
	?>
	@if($empvalue->Approver == $user)
	<table class="table table-edit" align="center">
		<tr>
			<th class="editempsid" colspan="12">
				<div class="form-group">
					<div class="col-sm-12">
						{{$empvalue->Ename}} - {{$empvalue->EmpsID}}
				     </div>
				  </div>
			</th>
		</tr>
		<tr class="editfileds">
			<th class="tableheader">Date</th>
			<th class="tableheader">Loc</th>
			<th class="tableheader">Job</th>
			<th class="tableheader">CostCode</th>
			<th class="tableheader">Cat</th>
			<th class="tableheader">Description</th>
			<th class="tableheader">Amount</th>
			<th class="tableheader">Attach</th>
			<th class="tableheader">Approver</th>
			<th class="tableaction" colspan="2">
				<span id="action">
					<a id="{{$empvalue->EmpsID}}-location[]" class="btn btn-success date-days check-all" href="javascript:void(0);">
					Approve All</a>
				</span>
			</th>
		</tr>
		<?php 
   			$reims=DB::table('reims as r')
		        ->join('emps as e', 'r.EmpsID', '=', 'e.EmpsID')
				->join('job as j', 'r.JobID', '=', 'j.JobID')
				->where('r.DELETED', '=', 'N')
				->where('r.DOWNLOADED', '=', 'N')
				->whereIn('r.APPROVED',['N','R','I'])
				->where('e.ENAME',$empvalue->Ename)
				->select(DB::raw('CONCAT(r.JobID, "-", j.JNAME)  Job'),'r.JobID as JobID','r.CostCode as CostCode',
					'r.EmpsID as EmpsID','r.RDATE as Date','r.LOC as Location','r.DSCR as Description', 'r.CAT as Category',
				    'r.AMOUNT as Amount','r.REIMS_ID as Reims_ID','r.APPROVED as Approved','r.ATTACH as Attach',
				    DB::raw('(IF(e.APVTYPE = "MANAGER", e.MANAGER, j.APPROVER)) as Approver'))
				    ->orderBy('r.RDATE','ASC')
				    ->get();
		?>
		@foreach($reims as $value)
		    @if($value->Approver == $user)
			@if($value->Approved == 'R')
			<tr class="reject">
			@elseif($value->Approved == 'I')
			<tr id="{{$value->Reims_ID}}-val" class="approve">
			@else
			<tr id="{{$value->Reims_ID}}-val">
			@endif
   				<td width="70">{{$value->Date}}</td>
				<td width="70">{{$value->Location}}</td>
				<td><span data-toggle="tooltip" class="red-tooltip" data-placement="right" title="{{$value->Job}}">{{substr($value->Job,0,10)}}</span></td>
		    	<?php $CostCode=DB::table('ccod')->where('CostCode',$value->CostCode)->first();?>
		    	<td><span data-toggle="tooltip" class="red-tooltip" data-placement="right" title="{{$value->CostCode}} - {{$CostCode->DSCR}}">{{$value->CostCode}} - {{substr($CostCode->DSCR,0,2)}}</span></td>
				<td>{{$value->Category}}</td>
				<td width="90">{{$value->Description}}</td>
				<td>{{$value->Amount}}</td>
				<?php $imagepath = env('UPLOAD_REIMS');$image=$value->Attach;$ext = pathinfo($image, PATHINFO_EXTENSION);?>
				@if($value->Attach == '')
          		<td><img height="23" src="{{url('images/cross.jpg')}}"></td>
        		@elseif($ext == 'pdf')
         		<td>
         			<a href="<?php echo url('pdf.php')?>?img={{$value->Attach}}&path=<?php echo $imagepath?>" target="_blank">
               		<img height="23" src="{{url('images/attachment.png')}}"></a>
               	</td>
       			@elseif($ext == 'txt')
           		<td>
           			<a href="<?php echo url('textattachement')?>?file={{$value->Attach}}" target="_blank">
           			<img height="23" src="{{url('images/attachment.png')}}"></a>
           		</td>
        		@else
        		<td>
        			<a href="<?php echo url('textattachement')?>?file={{$value->Attach}}" target="_blank">
        	    	<img height="23" src="{{url('images/attachment.png')}}"></a>
        		</td>
        		@endif
				<td>{{trim($value->Approver)}}</td>
				@if($value->Approved != 'N')
				<td class="ar-btn">
					<button type="button" class="btn-approve btn-circle btn-success" onclick="Approve(this.id)" id="{{$value->Reims_ID}}" disabled value="{{$value->Reims_ID}}">A</button>
				</td>
				<td class="ar-btn">
					<button type="button" class="btn-approve btn-circle btn-danger" onclick="Reject(this.id)" id="{{$value->Reims_ID}}"
					value="{{$value->Reims_ID}}">R</button>
				</td>	
				@else
				<td class="ar-btn">
					<button type="button" class="btn-approve btn-circle btn-success" onclick="Approve(this.id)" id="{{$value->Reims_ID}}" 
					value="{{$value->Job}}">A</button>
					<input type="checkbox" name="{{$empvalue->EmpsID}}-location[]" id="{{$value->Reims_ID}}-rem" value="{{$value->Reims_ID}}"
				    style="display:none"/>
				</td>
				<td class="ar-btn">
					<button type="button" class="btn-approve btn-circle btn-danger" onclick="Reject(this.id)" id="{{$value->Reims_ID}}" 
					value="{{$value->Reims_ID}}">R</button>
				</td>
				@endif
			</tr>
			@endif
		@endforeach
	</table>
	<div class="grouping"></div>
	@endif
@empty
	<div class="msg">
 		<p class="red">No Rembursement Record to Approve.</p>
	</div>
@endforelse
<script type="text/javascript">
	$(document).ready(function() {
	$('.check-all').click(function(){
		var labors_id=this.id;
		$("input[name='"+ labors_id +"']").attr('checked',true);
		var checkboxes = document.getElementsByName(labors_id);
		var vals = "";
		for (var i=0, n=checkboxes.length;i<n;i++) 
		{
			if (checkboxes[i].checked) 
			{
				vals += checkboxes[i].value;
				if(i<(n-1))
				{
					vals += ","	;				
				}
			}
		}
		if (vals) vals = vals.substring(0);
		var approve_values=vals;
		jQuery.ajax({
					type: "POST",
					dataType: "json",
					url: "ApproveReimsallD2S",
					data: {approveall:approve_values},
					success:function(data){
						for (var i=0, n=checkboxes.length;i<n;i++) 
			    		{
	                		$("#"+checkboxes[i].value+"-val").css("background-color",'#D3D3D3')
	                		document.getElementById(checkboxes[i].value).disabled=true;
			    		 }
					},
					error:function (){ 
					}
					});
			});
 		});
 	function Approve(labors_id)
	{
		var approve=labors_id;
		jQuery.ajax({
			type: "POST",
			dataType: "json",
			url: "ApproveReims",
			data: {reimsid:approve},
			success:function(data){
             $("#"+approve+"-val").css("background-color",'#D3D3D3');
             document.getElementById(approve).disabled=true;
              $('#'+approve+"-rem").remove();
			},
			error:function (){ 
			}
		});

		
	}
	function Reject(labors_id)
	{
		var reject=labors_id;
		jQuery.ajax({
			type: "POST",
			dataType: "json",
			url: "RejectReims",
			data: {reimsid:reject},
			success:function(data){
				$("#"+reject+"-val").css("background-color",'#ea9b9b');
				 document.getElementById(reject).disabled=true;
				 $('#'+reject+"-rem").remove();
			},
			error:function (){ 
			}
		});
	}
</script>
@include('includes.footer')
