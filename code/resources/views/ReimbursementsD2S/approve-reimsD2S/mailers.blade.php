<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<style>
		.mail-btn
		{
			display: inline-block;
			padding: 6px 12px;
			margin-bottom: 0;
			font-size: 14px;
			font-weight: 400;
			line-height: 1.42857143;
			text-align: center;
			white-space: nowrap;
			vertical-align: middle;
		   touch-action: manipulation;
			cursor: pointer;
			user-select: none;
			background-image: none;
			border: 1px solid transparent;
			border-radius: 4px;
        }
		.mail-primary
		{
			color: #fff;
			background-color: #337ab7;
			border-color: #2e6da4;
			text-decoration:none;
			margin-left: 25%;
		}	
		.site-content{
			margin-left: 25%;
		}
		.mail-header{
			width:45%;
			padding:5px;
			margin:auto;
		}
		.mail-body
		{
			background-color:#f1f1f1; width:50%;margin-left:30%;
		}
		.mail-content
		{
			width:88%; margin:auto; padding:5px;
		}
		.mail-tag
		{
			width:88%; margin:auto; padding:5px;
		}
		</style>
	</head>
	<body  class="mail-body">
		<div class='mail-header'>
			<h3>Rejected Reimbursement</h3>
		</div>
		<div class="mail-content">
			<i>Hi {{ $fname }},</i>
			<p>
				Your Reimbursement for the period {{ $dates }} has been rejected.<br><br>
				Please correct any errors and resubmit the reimbursement,<br>
				If you have any questions or concerns Please reach out to your manager or administrator for further assistance.
			</p>
			<a class="mail-btn mail-primary" href="http://myvpaz.com/auth/login">Resubmit Reimbursement</a>
		</div>
		<div class="mail-tag"><br>
			<a  class='site-content' href='http://www.myvpaz.com/'>www.myvpaz.com</a>
		</div>
	</body>
</html>