@include('includes.header')
<h3 class="msg">Reimbursements Edit for <span class="cname">{{ trans('menus.ds') }}</span></h3>
@forelse($emps as $empsVal)
<table class="table table-edit" align="center">
	<tr>
	<th class="reimsempid" colspan="12">
		<div class="form-group">
			<div class="col-sm-12">
				   {{$empsVal->Ename}} - {{$empsVal->EmpsID}}
			</div>
		</div>
	</th>
	</tr>
	<tr class="editfileds">
		<th class="tableheader">Date</th>
		<th class="tableheader">Loc</th>
		<th class="tableheader">Job</th>
		<th class="tableheader">CostCode</th>
		<th class="tableheader">Cat</th>
		<th class="tableheader">Description</th>
		<th class="tableheader">Attach</th>
		<th class="tableheader">Amount</th>
		<th class="tableaction" colspan="2">Action</th>
	</tr>
	<?php 
	    $reims=DB::table('reims as r')
		        ->join('emps as e', 'r.EmpsID', '=', 'e.EmpsID')
				->join('job as j', 'r.JobID', '=', 'j.JobID')
				->where('e.EmpsID', $empsVal->EmpsID)
				->where('r.DELETED', '=', 'N')
				->where('r.DOWNLOADED', '=', 'N')
				->whereIn('r.APPROVED',['N','R','I'])
				->select(DB::raw('CONCAT(r.JobID, "-", j.JNAME)  Job'),'e.ENAME as Ename','r.EmpsID as EmpsID','r.RDATE as Date',
					    'r.CostCode as CostCode', 'r.LOC as Location','r.DSCR as Description', 'r.CAT as Category','r.AMOUNT as Amount',
				        'r.REIMS_ID as Reims_ID','r.APPROVED as Approved','r.ATTACH as Attach',
				        DB::raw('(IF(e.APVTYPE = "MANAGER", e.MANAGER, j.APPROVER)) as Approver') )
				->orderBy('r.RDATE','ASC')
				->get();?>
	@foreach($reims as $reimsVal)
    @if($reimsVal->Approved == 'R')
		<tr class="reject">
	@elseif($reimsVal->Approved == 'I')
		<tr class="approve">
	@else
		<tr>
	@endif
		    <td width="70">{{$reimsVal->Date}}</td>
			<td width="70">{{$reimsVal->Location}}</td> 
			<td><span data-toggle="tooltip" class="red-tooltip" data-placement="right" title="{{$reimsVal->Job}}">{{substr($reimsVal->Job,0,10)}}</span></td>
		    <?php $CostCode=DB::table('ccod')->where('CostCode',$reimsVal->CostCode)->first();?>
		    <td><span data-toggle="tooltip" class="red-tooltip" data-placement="right" title="{{$reimsVal->CostCode}} - {{$CostCode->DSCR}}">{{$reimsVal->CostCode}} - {{substr($CostCode->DSCR,0,2)}}</span></td>
			<td>{{$reimsVal->Category}}</td>
			<td width="100">{{$reimsVal->Description}}</td>
			<?php $imagepath = env('UPLOAD_REIMS');$image=$reimsVal->Attach;$ext = pathinfo($image, PATHINFO_EXTENSION);?>
			@if($reimsVal->Attach == '')
	        <td><img height="23" src="{{url('images/cross.jpg')}}"></td>
	        @elseif($ext == 'pdf')
	        <td>
	          	<a href="<?php echo url('pdf.php')?>?img={{$reimsVal->Attach}}&path=<?php echo $imagepath?>" target="_blank">
	            <img height="23" src="{{url('images/attachment.png')}}"></a>
	        </td>
	        @elseif($ext == 'txt')
	          <td><a href="<?php echo url('textattachement')?>?file={{$reimsVal->Attach}}" target="_blank">
	          		<img height="23" src="{{url('images/attachment.png')}}"></a></td>
	        @else
	        <td>
	        	<a href="<?php echo url('textattachement')?>?file={{$reimsVal->Attach}}" target="_blank">
	        		<img height="23" src="{{url('images/attachment.png')}}"></a>
	        </td>
	        @endif
			<td>{{$reimsVal->Amount}}</td>
			@if($reimsVal->Approved == 'I')
				<td><a href="{{route('enter-reimsD2S.edit',$reimsVal->Reims_ID)}}" class="btn btn-info ed-btn date-days" disabled>Edit</a></td>
				<td>
					{!! Form::open(['method' => 'DELETE', 'route'=>['enter-reimsD2S.destroy', $reimsVal->Reims_ID]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger ed-btn','disabled']) !!}
					{!! Form::close() !!}
				</td>
			@else
				<td><a href="{{route('enter-reimsD2S.edit',$reimsVal->Reims_ID)}}" class="btn btn-info ed-btn date-days">Edit</a></td>
				<td>
					{!! Form::open(['method' => 'DELETE', 'route'=>['enter-reimsD2S.destroy', $reimsVal->Reims_ID]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger ed-btn']) !!}
					{!! Form::close() !!}
				</td>
			@endif
		</tr>
		@endforeach
</table>
<div class="grouping"></div>
@empty
<div class="msg">
 <p class="red">No Rembursement Record to Edit.</p>
</div>
@endforelse
@include('includes.footer')
