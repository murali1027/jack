@include('includes.header')
<div class="container-fluid">
	<div class="row" >
		<div class="col-md-12">
			<div class="panel">
				<table class="table table-bordered" align="center">
					<tr>
						<td> 
							<h2 class="msg">Update Time Entry for <span class="cname">{{ trans('menus.ds') }}</span></h2>
							<div class="col-md-11 ename" align="center">
								<table  class="empname" width="57%">
									<tr>
										<th class="week-day emp-name" colspan="8">
										    <?php $empname=DB::table('emps')->where('EmpsID',$edit->EmpsID)->first();?>
											<p class="emp-id">{{$empname->ENAME}} - {{ $edit->EmpsID }}</p>
										</th>
									</tr>
								</table><br>
							</div>
							<div class="panel-body">
								{!! Form::model($edit,['method' => 'PATCH', 'class'=>'form-horizontal', 'id'=>'edittimesheet-form',
								'route'=>['editlaborsD2S.update',$edit->LABORS_ID]])!!}
									<input type="hidden" name="empsid"  value="{{ $edit->EmpsID }}">
									<div class="form-group">
										<label class="col-sm-4 control-labels">Date</label>
										<div class="col-sm-7">
											<input type="text" class="form-control"  name="date" value="{{$edit->DATE_WORK}}" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-labels">Job</label>
										<div class="col-sm-7">
											<select name="jobid" id="jobid" class="form-control drop-box">
											@foreach($jname as $value)
											   <option data-id="{{$edit->Extra}}" value="{{$edit->JobID}}">{{$edit->JobID}} {{$edit->EXTRA}} - {{$value->JNAME}}</option>
											@endforeach	
											@foreach($job as $value)
												<option data-id="{{$value->EXTRA}}" value="{{$value->JobID}}">{{$value->JOB}} - {{$value->JNAME}}</option>
											@endforeach
											</select>
										</div>
									</div> 
									<input type="hidden" id="extra" name="extra">
									<div class="form-group">
										<label class="col-sm-4 control-labels">Cost Code</label>
										<div class="col-sm-7">
											<select name="ccode" id="ccode" class="form-control drop-box removesel">
											<?php $CostCode=DB::table('ccod')->where('CostCode',$edit->CostCode)->where('EXTRA',$edit->EXTRA)->first();?>
												<option value="{{$edit->CostCode}}">{{$edit->CostCode}} - {{$CostCode->DSCR}}</option>
												    @foreach($ccod as $value)
													<option value="{{$value->CostCode}}">{{$value->CostCode}} - {{$value->DSCR}}</option>
													@endforeach
											</select>
										</div>
									</div> 
									<div class="form-group">
										<label class="col-sm-4 control-labels">Cert Class</label>
										<div class="col-sm-7">
											<select name="certs" id="certs" class="form-control drop-box removesel">
												  <option value="{{$edit->CERTS}}">{{$edit->CERTS}}</option>
												   @foreach($certs as $value)
												   <option value="{{$value->CERTS}}">{{$value->CERTS}}</option>
													@endforeach
											</select>
										</div>
									</div>  
									<div class="form-group">
										<label class="col-sm-4 control-labels">Shift</label>
										<div class="col-sm-7">
											<select name="shift" id="shift" class="form-control drop-box">
												<option value="1">Day</option>
												<option value="2">Swing</option>
												<option value="3">Night</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-labels">Hours</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" id="hours" name="hours" value="{{$edit->HOURS}}">
										</div>
									</div>
									<div id="msg"></div>
									@if (session('message'))
									<div class="alert alert-danger">
										{{ session('message') }}
									</div>
									@endif
									<div class="form-group">
										<div class="col-md-6 col-md-offset-2">
											{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
										</div>
									</div>
									</br>
								{!! Form::close() !!}
							</div>
						</td>
					</tr>
				</table>
			</div>
			<div class="pull-left"><strong>User: </strong><span  id="user_name">{{ Auth::user()->FNAME }} {{ Auth::user()->LNAME }}</span></div>
			<div class="pull-right"><strong>Date: </strong><span  id="currentdate">{{date('m/d/Y')}}</span></div>
		</div>
	</div>
</div>
@include('includes.footer')


