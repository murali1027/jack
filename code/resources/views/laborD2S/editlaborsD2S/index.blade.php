@include('includes.header')
<h3 class="msg">Timesheet Edit for <span class="cname">{{ trans('menus.ds') }}</span></h3>
<table class="table table-edit" align="center">
	<tr>
		<th class="editempid" colspan="12">
		    <input type="hidden" name="role" id="role" value="{{ Auth::user()->ROLE }}">
		    <input type="hidden"  id="domain" value="{{ Auth::user()->DOMAIN }}">
			<input type="hidden"  id="page" value="editlaborsD2S">
			@if(Auth::check())
				@if(Auth::user()->ROLE == 'USR')
					<p>{{ Auth::user()->FNAME }} {{ Auth::user()->LNAME }} - {{ Auth::user()->EmpsID }}</p>
				@else<br>
					<div class="form-group">
						<div class="col-sm-12">
							<select name="ename" id="ename" class="form-control drop-box">
								<option value="" disabled selected>Select an Employee</option>
								@foreach($empid as $value)
									<option value="{{$value->EmpsID}}">{{$value->ENAME}} {{"-"}} {{ $value->EmpsID}}</option>
								@endforeach
							</select><br>
						</div>
					</div>
				@endif
				<div class="form-group">
					<div class="col-sm-12">
						<select name="weeks" id="weeks" class="form-control drop-box"></select>
					</div>
				</div>
			@endif
		</th>
	</tr>
</table>
<div class="clear">
	<div id="clear">
		<table class="table table-edit" align="center">
			<tr class="editfileds">
				<th class="tableheader">Job</th>
				<th class="tableheader">Cost Code</th>
				<th class="tableheader">Cert</th>
				<th class="tableheader">Date</th>
				<th class="tableheader">Shift</th>
				<th class="tableheader">Hours</th>
				<th class="tableheader">App</th>
				<th class="tableheader">Approver</th>
				<th class="tableaction" colspan="2">Action</th>
			</tr>
		</table>
	</div>
</div>
@include('includes.footer')
