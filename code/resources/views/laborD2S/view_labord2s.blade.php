@include('includes.header')
	<div class="container-fluid" >
		<div class="row" >
			<div class="col-md-12">
				<div class="panel">
					<table class="table table-bordered" align="center">
						<tr>
							<td><form class="form-horizontal" role="form" id="timesheet-form" method="POST">
							 <h2 class="msg">Time Entry for <span class="cname">{{ trans('menus.ds') }}</span></h2>
								<div class="col-md-12" align="center">
									<table  class="empname" width="60%">
									    <tr>
											<th class="week-day emp-name" colspan="8">
												@if(Auth::check())
												@if(Auth::user()->ROLE == 'USR')
												<p class="emp-id">{{ Auth::user()->FNAME }} {{ Auth::user()->LNAME }} - {{ Auth::user()->EmpsID }}</p>
												@else
												<div class="form-group">
													<div class="col-sm-12">
														<select name="ename" id="ename" onchange="ViewEmpDetail()"  tabindex="1" 
														class="form-control drop-box" />
															<option value="" disabled selected>Select an Employee</option>
															@foreach($empid as $value)
															<option value="{{$value->EmpsID}}">{{$value->ENAME}} {{"-"}} {{ $value->EmpsID}}</option>
															@endforeach
														</select>
													</div>
												</div>
												@endif
												@endif
											</th>
										</tr>
										<tr>
											<th class="week-day week-month" colspan="8">
												<div class="pre"><button type="button" onclick="Prev()" id="previous"><</button></div>
												Week Ending: <span id="endDate"></span>
												<div class="nxt"><button type="button"  onclick="Next()">></button></div>
											</th>
										</tr>
										<tr class="days">
										    <th class="days_hours"><input type="hidden" id="sunDate"><span id="sunday"></span>-Sun</th>
											<th class="days_hours"><input type="hidden" id="monDate"><span id="monday"></span>-Mon</th>
											<th class="days_hours"><input type="hidden" id="tueDate"><span id="tuesday"></span>-Tue</th>
											<th class="days_hours"><input type="hidden" id="wedDate"><span id="wednesday"></span>-Wed</th>
											<th class="days_hours"><input type="hidden" id="thuDate"><span id="thursday"></span>-Thu</th>
											<th class="days_hours"><input type="hidden" id="friDate"><span id="friday"></span>-Fri</th>
											<th class="days_hours"><input type="hidden" id="satDate"><span id="saturday"></span>-Sat</th>
											<th class="days_hours">Total</th>
										</tr>
										<tr>
											<td class="days_hours"><span id="sun"></span></td>
											<td class="days_hours"><span id="mon"></span></td>
											<td class="days_hours"><span id="tue"></span></td>	
											<td class="days_hours"><span id="wed"></span></td>
											<td class="days_hours"><span id="thu"></span></td>
											<td class="days_hours"><span id="fri"></span></td>
											<td class="days_hours"><span id="sat"></span></td>
											<td class="days_hours total"><span id="total">0</span></td>
										</tr>
									</table><br>
								</div>
								<div class="panel-body">
								    
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<input type="hidden" name="role" id="role" value="{{ Auth::user()->ROLE }}">
									
										<div class="form-group">
											<div class="col-sm-6">
												<input type="hidden" class="form-control drop-box" id="date" name="date"/>
											</div>
										</div>
										
										<div class="clear">
											<div id="clear"><!--return response div-->
										<div class="form-group">
											<label class="col-sm-4 control-labels">Job</label>
											<div class="col-sm-7">
												<select name="jobid" id="jobid" class="form-control drop-box" tabindex="2">
													@if(Auth::check())
														@if(Auth::user()->ROLE == 'USR')
															@foreach($empid as $value)
																@if($value->JOB_DEFAULT != 0)
																   @foreach($extra as $cvalue)	
																	<option data-id="{{$cvalue->Extra}}" value="{{$value->JobID}}">
																	{{$value->JobID}} {{$cvalue->Extra}} - {{$value->JNAME}}</option>
																	@endforeach
																    <option value=""></option>
																@else
																    <option value="" disabled selected>Select a Job</option>
																@endif
															@endforeach
															@else
															<option value="" disabled selected>Select a Job</option>
														@endif
													@endif
													@foreach($job as $value)
														<option data-id="{{$value->EXTRA}}" value="{{$value->JobID}}">
														{{$value->JOB}} - {{$value->JNAME}}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-labels">Cost Code</label>
											<div class="col-sm-7">
												<select name="ccode" id="ccode" class="form-control drop-box removesel" tabindex="3">
												   <option value="" disabled selected>Select a Cost Code</option>
												   @if(Auth::user()->ROLE == 'USR')
												    @foreach($ccode as $value)
													<option value="{{$value->CostCode}}">{{$value->CostCode}} - {{$value->DSCR}}</option>
													@endforeach
													@endif
												</select>
											</div>
										</div> 
										<div class="form-group">
											<label class="col-sm-4 control-labels">Cert Class</label>
											<div class="col-sm-7">
												<select name="certs" id="certs" class="form-control drop-box" tabindex="4">
												   @if(Auth::user()->ROLE == 'USR')
												    @foreach($certs as $value)
													    <option value="{{$value->CERTS}}">{{$value->CERTS}}</option>
													@endforeach
													@endif
												</select>
											</div>
										</div>  
										</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-labels">Shift</label>
											<div class="col-sm-7">
												<select name="shift" id="shift" class="form-control drop-box" tabindex="5">
												    <option value="1">Day</option>
													<option value="2">Swing</option>
													<option value="3">Night</option>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4  hours-lable">Hours</label>
											<div class="col-sm-7">
												<div class="form-group col-xs-3 col-md-3">
													<label for="hours" class="control-label ">Sun-<span id="sundays"></span></label>
													<input type="text" name="sun_hours" class="form-control dates" id="sun_hours"
													onkeypress="return validate(event)" tabindex="6">
												</div>
												<div class="form-group col-xs-3 col-md-3">
													<label for="hours" class="control-label">Mon-<span id="mondays"></span></label>
													<input type="text" name="mon_hours" class="form-control dates" id="mon_hours" 
													onkeypress="return validate(event)" tabindex="7">
												</div>
												<div class="form-group col-xs-3 col-md-3">
													<label for="hours" class="control-label">Tue-<span id="tuesdays"></span></label>
													<input type="text" name="tue_hours" class="form-control dates" id="tue_hours" 
													onkeypress="return validate(event)" tabindex="8" >
												</div>
												<div class="form-group col-xs-3 col-md-3">
													<label for="hours" class="control-label">Wed-<span id="wednesdays"></span></label>
													<input type="text" name="wed_hours" class="form-control dates"  id="wed_hours"
													onkeypress="return validate(event)" tabindex="9">
												</div>
												<div class="form-group col-xs-3 col-md-3 ">
													<label for="hours" class="control-label">Thu-<span id="thursdays"></span></label>
													<input type="text" name="thu_hours" class="form-control dates" id="thu_hours" 
													onkeypress="return validate(event)" tabindex="10">
												</div>
												<div class="form-group col-xs-3 col-md-3 ">
													<label for="hours" class="control-label">Fri-<span id="fridays"></span></label>
													<input type="text" name="fri_hours" class="form-control dates" id="fri_hours"
													onkeypress="return validate(event)" tabindex="11">
												</div>
												<div class="form-group col-xs-3 col-md-3">
													<label for="hours" class="control-label">Sat-<span id="saturdays"></span></label>
													<input type="text" name="sat_hours" class="form-control dates" id="sat_hours" 
													onkeypress="return validate(event)" tabindex="12">
												</div>
											</div>
										</div>
										<br>
										<div id="msg"></div>
											
										<div class="form-group">
											<div class="col-md-6 col-md-offset-3"></br>
												<button type="button" id="submit"  class="btn btn-primary" tabindex="13">Submit</button>
											</div>
										</div>
										</br>
									
								</div>
							</td></form>
						</tr>
					</table>
				</div>
				<div class="pull-left"><strong>User: </strong><span  id="user_name">{{ Auth::user()->FNAME }} {{ Auth::user()->LNAME }}</span></div>
				<div class="pull-right"><strong>Date: </strong><span  id="currentdate">{{date('m/d/Y')}}</span></div>
			</div>
		</div>
	</div>
@include('includes.footer')
