 <div class="form-group">
	<label class="col-sm-4 control-labels">Job</label>
	<div class="col-sm-7">
		<select name="jobid" id="jobid" class="form-control drop-box" tabindex="2">
			@foreach($empid as $value)
				@if($value->JOB_DEFAULT != 0)
				    @foreach($extra as $cvalue)
					<option data-id="{{$cvalue->Extra}}" value="{{$value->JobID}}">{{$value->JobID}} {{$cvalue->Extra}} - {{$value->JNAME}}</option>
					@endforeach
				    <option value=""></option>
				    @else
					<option value="" disabled selected>Select a Job</option>
				@endif
				@foreach($job as $value)
				    <option data-id="{{$value->EXTRA}}" value="{{$value->JobID}}">{{$value->JOB}} - {{$value->JNAME}}</option>
				@endforeach
			@endforeach
		</select>
	</div>
</div> 
<div class="form-group">
	<label class="col-sm-4 control-labels">Cost Code</label>
	<div class="col-sm-7">
		<select name="ccode" id="ccode" class="form-control drop-box" tabindex="3">
		  <option value="" disabled selected>Select a Cost Code</option>
		    @foreach($ccode as $value)
				<option value="{{$value->CostCode}}">{{$value->CostCode}} - {{$value->DSCR}}</option>
			@endforeach
		</select>
	</div>
</div> 
<div class="form-group">
	<label class="col-sm-4 control-labels">Cert Class</label>
	<div class="col-sm-7">
		<select name="certs" id="certs" class="form-control drop-box" tabindex="4">
			 @foreach($certs as $value)
			<option value="{{$value->CERTS}}">{{$value->CERTS}}</option>
			@endforeach
		</select>
	</div>
</div>  
<script>
	$('#jobid').on('change',function(e)
	{
		var job_id = e.target.value;
		var extra = $('#jobid option:selected').attr('data-id');
		$.get('PostCcod?job_id=' + job_id + '&extra='+ extra, function(data){
			$('#ccode').empty();
			$('#ccode').append('<option value="" disabled selected>Select a Cost Code</option>');
			$.each(data,function(index,subcatObj){
				$('#ccode').append('<option value="'+subcatObj.CostCode+'">'+subcatObj.CostCode+'&nbsp;'+'-' +'&nbsp;'+subcatObj.DSCR+'</option>');
			});
		});
	});
	$('#jobid').on('change',function(e)
	{
		var job_id = e.target.value;
		$.get('Postcerts?job_id=' + job_id, function(data){
			$('#certs').empty();
			$.each(data,function(index,subcatObj){
				$('#certs').append('<option value="'+subcatObj.CERTS+'">'+subcatObj.CERTS+'</option>');
			});
		});
	});
</script>