<br><br><br>
<table class="table table-hours" align="center">
	<tr class="editfileds">
	    <th class="tableheader">Employee</th>
		<th class="tableheader">Enter</th>
		<th class="tableheader">Approve</th>
		<th class="tableheader">Reject</th>
	</tr>
	@foreach($labors_hours as $value)
	<tr>
		<?php $dates=array();?>
		@foreach ($labors_date as $date)
			<?php $dates[]=$date->DATE_WORK;?>
		@endforeach
		<?php 	
			$empid=array();
			$empid[]=$value->EmpsID;
			$labors_add=DB::table('labors as l')
						->join('emps as e', 'l.EmpsID', '=', 'e.EmpsID')
						->whereIn('l.EmpsID',$empid)
						->whereIN('l.DATE_WORK',$dates)
						->where('l.DELETED', '=', 'N')
						->select(DB::raw('SUM(CASE WHEN l.APPROVED = "N" THEN l.HOURS ELSE 0 END) As HoursEntered'),
					         DB::raw('SUM(CASE WHEN l.APPROVED="A" THEN l.HOURS ELSE 0 END) As HoursApproved'),
				      	     DB::raw('SUM(CASE WHEN l.APPROVED="R" THEN l.HOURS ELSE 0 END) As HoursRejected'))
						->get();
		?>
		@foreach($labors_add as $labors)
			@if($labors->HoursEntered!= NULL || $labors->HoursApproved!= NULL || $labors->HoursRejected!= NULL )
				<td class="hours-name">{{$value->ENAME}}</td>
			@else
				<td class="reject hours-name">{{$value->ENAME}}</td>
			@endif
			@if($labors->HoursEntered!= NULL)
				<td>{{$labors->HoursEntered}}</td>
			@else
				<td class="reject">0</td>
			@endif
			@if($labors->HoursApproved!= NULL)
				<td>{{$labors->HoursApproved}}</td>
			@else
				<td class="reject">0</td>
			@endif
			@if($labors->HoursRejected!= NULL)
				<td>{{$labors->HoursRejected}}</td>
			@else
				<td class="reject">0</td>
			@endif
		@endforeach
	</tr>
	@endforeach
</table>


	