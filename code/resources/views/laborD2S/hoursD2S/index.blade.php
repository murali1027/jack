@include('includes.header')
<h3 class="msg">Time Entry Status for <span class="cname">{{ trans('menus.ds') }}</span></h3>
<input type="hidden"  id="hourd2s" value="HoursD2S">
<div class="form-group">
	<div class="col-sm-5 col-md-offset-3">
		<select name="weeks" id="weeks" onchange="HoursEmpDetailD2S()" class="form-control drop-box hours-drop"></select>
	</div>
</div>
<div class="clear">
	<div id="clear">
		<table class="table table-hours" align="center">
			<tr class="editfileds">
				<th class="tableheader">Employee</th>
				<th class="tableheader">Enter</th>
				<th class="tableheader">Approve</th>
				<th class="tableheader">Reject</th>
			</tr>
		</table>
	</div>
</div>
@include('includes.footer')
