<div class="grouping"></div>
@foreach($emps as $emp)
<table class="table table-edit" align="center">
	<tr>
		<th class="editempid" colspan="12">
			<div class="form-group">
				<div class="col-sm-12">
				    <input type="hidden" name="role" id="role" value="{{ Auth::user()->ROLE }}">
					<input type="hidden" id="approver" value="approveD2S">
					{{$emp->ENAME}} - {{$emp->EmpsID}}
					<?php $hours=0; ?>
	                 @foreach($labors as $value)
					   <?php $hours += $value->Hours;?>
					@endforeach
					<span class="ttlhrs">Total Hours: {{$hours}}</span>
				</div>
			</div>
		</th>
	</tr>
</table>
@endforeach
<table class="table table-edit" align="center">
	<tr class="editfileds">
		<th class="tableheader">Job</th>
		<th class="tableheader">Cost Code</th>
		<th class="tableheader">Cert</th>
		<th class="tableheader">Date</th>
		<th class="tableheader">Hours</th>
		<th class="tableheader">App</th>
		<th class="tableheader">Approver</th>
		<th class="tableaction" colspan="2"><span id="action"><a id="checks-all" class="btn btn-success date-days check-all"  href="javascript:void(0);">Approve All</a></span></th>
	</tr>
	    
	    @foreach($labors as $value)
		<?php $mailid  = Auth::user()->EMAIL;  
			$user = strstr($mailid, '@', true);  
		?>
		@if($value->Approved == 'R')
			<tr class="reject">
		@elseif($value->Approved == 'A')
			<tr class="approve">
		@elseif($user != trim($value->Approver))
		    <tr class="approve">
		@else
			<tr>
		@endif
			<td><span data-toggle="tooltip" class="red-tooltip" data-placement="right" title="{{$value->Job}}">{{substr($value->Job,0,10)}}</span></td>
		    <?php $CostCode=DB::table('ccod')->where('CostCode',$value->CostCode)->first();?>
		   	<td><span data-toggle="tooltip" class="red-tooltip" data-placement="right" title="{{$value->CostCode}} - {{$CostCode->DSCR}}">{{$value->CostCode}} - {{substr($CostCode->DSCR,0,2)}}</span></td>
			<td>{{$value->Cert}}</td>
			<td><?php $dayname = strtotime($value->Date);?>{{$day = date("D", $dayname)}}</td>
			<td>{{$value->Hours}}</td>
			<td>{{$value->Approved}}</td>
			<td>{{trim($value->Approver)}}</td>
			@if($user == trim($value->Approver))
				@if($value->Approved != 'N')
				<td class="ar-btn">
					<button type="button" class="btn-approve btn-circle btn-success" onclick="Approve(this.id)" id="{{$value->Labors_ID}}" disabled value="{{$value->Labors_ID}}">A</button>
				</td>
				<td class="ar-btn">
					<button type="button" class="btn-approve btn-circle btn-danger" onclick="Reject(this.id)" id="{{$value->Labors_ID}}"  value="{{$value->Labors_ID}}">R</button>
				</td>	
				@else
			    <td class="ar-btn">
					<button type="button" class="btn-approve btn-circle btn-success" onclick="Approve(this.id)" id="{{$value->Labors_ID}}" value="{{$value->Job}}">A</button>
					<input type="checkbox" name="locations[]" id="{{$value->Labors_ID}}" value="{{$value->Labors_ID}}" style="display:none;" />
				</td>
				<td class="ar-btn">
					<button type="button" class="btn-approve btn-circle btn-danger" onclick="Reject(this.id)" id="{{$value->Labors_ID}}" value="{{$value->Labors_ID}}">R</button>
				</td>
				@endif
            @else
			<td class="ar-btn">
			    <button type="button" class="btn-approve btn-circle btn-success" onclick="Approve(this.id)" id="{{$value->Labors_ID}}" disabled value="{{$value->Labors_ID}}">A</button>
			</td>
			<td class="ar-btn">
				<button type="button" class="btn-approve btn-circle btn-danger" onclick="Reject(this.id)" id="{{$value->Labors_ID}}" disabled value="{{$value->Labors_ID}}">R</button>
			</td>
            @endif		
		</tr>
		
	@endforeach
	
</table>
<br>
 
<div id="msg">
<span id="norec" style="color:red;"></span>
</div>

<script>
$(document).ready(function() {
	$('#checks-all').click(function(){
		$("input:checkbox").attr('checked', true);
		var checkboxes = document.getElementsByName('locations[]');
		var vals = "";
		for (var i=0, n=checkboxes.length;i<n;i++) 
		{
			if (checkboxes[i].checked) 
			{
				vals += checkboxes[i].value;
				if(i<(n-1))
				{
					vals += ","					
				}
			}
		}
		if (vals) vals = vals.substring(0);
		var approve_values=vals;
		jQuery.ajax({
					type: "POST",
					dataType: "json",
					url: "ApproveallD2S",
					data: {approveall:approve_values},
					success:function(data){
							var empid= document.getElementById("ename").value;
							var weeks_start = $('#weeks option:selected').attr('data-id');
							var weeks_end= document.getElementById("weeks").value;
							jQuery.ajax({
								type: "POST",
								dataType: "json",
								url: "ApproveTimesheetD2S",
								data: {empid:empid,weeks_start:weeks_start,weeks_end:weeks_end},
								success:function(data){
									$('#clear').empty();
									$('.clear').html(data.edit);
								},
								error:function (){ 
								}
								});
					},
					error:function (){ 
					}
					});
	});
 });
 	function Approve(labors_id)
	{
		var approve=labors_id;
		jQuery.ajax({
			type: "POST",
			dataType: "json",
			url: "Approve",
			data: {laborsid:approve},
			success:function(data){
				var empid= document.getElementById("ename").value;
				var weeks_start = $('#weeks option:selected').attr('data-id');
				var weeks_end= document.getElementById("weeks").value;
				jQuery.ajax({
					type: "POST",
					dataType: "json",
					url: "ApproveTimesheetD2S",
					data: {empid:empid,weeks_start:weeks_start,weeks_end:weeks_end},
					success:function(data){
						$('#clear').empty();
						$('.clear').html(data.edit);
					},
					error:function (){ 
					}
					});

			},
			error:function (){ 
			}
		});
	}
	function Reject(labors_id)
	{
		var reject=labors_id;
		jQuery.ajax({
			type: "POST",
			dataType: "json",
			url: "Reject",
			data: {laborsid:reject},
			success:function(data){
				var empid= document.getElementById("ename").value;
				var weeks_start = $('#weeks option:selected').attr('data-id');
				var weeks_end= document.getElementById("weeks").value;
				jQuery.ajax({
					type: "POST",
					dataType: "json",
					url: "ApproveTimesheetD2S",
					data: {empid:empid,weeks_start:weeks_start,weeks_end:weeks_end},
					success:function(data){
						$('#clear').empty();
						$('.clear').html(data.edit);
					},
					error:function (){ 
					}
					});
			},
			error:function (){ 
			}
		});
	}

	</script>
	<script>
	$(document).ready(function(){
    	$('[data-toggle="tooltip"]').tooltip();   
	});
</script>