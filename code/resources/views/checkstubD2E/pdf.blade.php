<html>
<head>
	<style>
	.table-heading{background-color: #6495ed !important;}
	.name-field{margin-left:18px !important;}
	 html, body {width: 100%;}
	.Table-Normal {margin: 10px auto;padding: 0;width: 80%;border-collapse: collapse;font-family: calibri;}
	.emp-field thead tr {font-weight: normal;}
	.Table-Normal thead tr  {font-weight:bold;text-align: left;}
	.Table-Normal thead tr th {font-weight:bold;text-align: center;}
	.Table-Normal tr { margin: 0; padding: 4px 8px;; border: 0;width: 100%;}
	.Table-Normal tr td {margin: 0;padding: 4px 8px;border: 1px solid #e2dcdc;text-align: left; font-size: 14px;}
	.container1{position: relative;margin: 10px auto;padding: 0;width: 70%;height: auto; border-collapse: collapse;}
	.container2{width: 1%;margin: 10px auto;}
	.ytd{margin-left:30%;font-size: 14px;font-weight:bold;}
	.week{margin-left: 47%;font-size: 14px;font-weight:bold;}
	table.Table-Normal td div{width:65px; text-align:right;}
</style>
</head>
<body>
	<div>
        <div class="container1">
            <a> <img src="{{URL::to('images/delogo.jpg')}}"></a>
		</div>
		<div>
			<table class="Table-Normal">
				<thead>
					<tr>
						<td class='emp-field'>Employee ID</td>
						<td>{{$EmpsID}}</td>
					</tr>
					<tr>
						<td class='emp-field'>Name</td>
						<td>{{$ename}}</td>
					</tr>
					<tr>
						<td class='emp-field'>Email</td>
						<td>{{$email}}</td>
					</tr>
					<tr>
						<td class='emp-field'>Check Date</td>
						<td>{{$checkdate}}</td>
					</tr>
					<tr>
						<td class='emp-field'>Week Ending</td>
						<td>{{$weekending}}</td>
					</tr>
					<tr>
						<td class='emp-field'>PTO Avaliable</td>
						@foreach($pto as $pto_val)
							<td>{{$pto_val->PTOAvaliable + $pto_val->PTOAccural - $pto_val->PTOAccuralUsed }}</td>
						@endforeach
					</tr>
					<tr>
						<td class='emp-field'>Floating Holiday</td>
						@foreach($float as $float_val)
							<td>{{$float_val->Floating_holiday - $float_val->Floating_holiday_Taken  }}</td>
						@endforeach
					</tr>
				</thead>
			</table>
		</div>
		<div class="container1">
			<label class="week">Week</label><label class="ytd">YTD</label>
		</div>
		<div>
			<table class="Table-Normal">
				<thead>
					<tr class='table-heading'>
						<th>Pays</th>
						<th>Units</th>
						<th>Amount</th>
						<th>Units</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody><?php $fmt = new NumberFormatter( 'en_US', NumberFormatter::CURRENCY );?>
					<?php $pays_total=0; $pays_ytdtotal=0; ?>
					@foreach($pays as $pays_val)
						<tr>
							<td width="50%">{{$pays_val->CAT}}</td>
							<td width="30%" align="right"><div>{{$pays_val->CurrentUnits}}</div></td>
							<td width="30%" align="right"><div>{{$fmt->formatCurrency($pays_val->CurrentAmount, 'USD')}}</div></td>
							<td width="30%" align="right"><div>{{$pays_val->YTDUnits}}</div></td>
							<td width="30%" align="right"><div>{{$fmt->formatCurrency($pays_val->YTDAmount, 'USD')}}</div></td>
						</tr>
						<?php $pays_total += $pays_val->CurrentAmount; $pays_ytdtotal += $pays_val->YTDAmount;?>
					@endforeach
			   <tr><td width="60%">Total</td>
			   
			   <td width="30%"></td>
			   <td width="30%"><div><b>{{$fmt->formatCurrency($pays_total, 'USD')}}</b></div></td>
			   <td width="30%"></td>
               <td width="30%"><div><b>{{$fmt->formatCurrency($pays_ytdtotal, 'USD')}}</b></div></td>
			   </tr>
			   </tbody>
			</table>
			
		</div>
		<div>
			<table class="Table-Normal">
				<thead>
					<tr class='table-heading'>
                        <th>Taxes</th>
                        <th>Taxable</th>
                        <th>Amount</th>
						<th>Taxable</th>
                        <th>Amount</th>
                    </tr>
				</thead>
				<tbody>
					<?php $tax_total=0; $ytdtax=0;?>
					@foreach($taxs as $taxs_val)
						<tr>
							<td width="50%">{{$taxs_val->CAT}}</td>
							<td width="30%"><div>{{$fmt->formatCurrency($taxs_val->CurrentWage, 'USD')}}
							</div></td>
							<td width="30%"><div>{{$fmt->formatCurrency($taxs_val->CurrentTax, 'USD')}}
							</div></td>
							<td width="30%"><div>{{$fmt->formatCurrency($taxs_val->YTDWage, 'USD')}}</div></td>
							<td width="30%"><div>{{$fmt->formatCurrency($taxs_val->YTDTax, 'USD')}}</div></td>
					   </tr>
					   <?php $tax_total += $taxs_val->CurrentTax; $ytdtax += $taxs_val->YTDTax;?>
					@endforeach
			   <tr><td width="60%">Total</td>
			   
			   <td width="30%"></td>
			   <td width="30%"><div><b>{{$fmt->formatCurrency($tax_total, 'USD')}}</b></div></td>
			   <td width="30%"></td>

			   <td width="30%"><div><b>{{$fmt->formatCurrency($ytdtax, 'USD')}}</b></div></td>
			   </tr>
			   
			   </tbody>
			</table>
			
		</div>
		<div>
			<table class="Table-Normal">
				<thead>
					<tr class='table-heading'>
						<th>Deductions</th>
                        <th>Basis</th>
                        <th>Amount</th>
						<th>Basis</th>
                        <th>Amount</th>
					</tr>
				</thead>
				<tbody>
					<?php $sub_total=0; $deduct_total=0;?>
					@foreach($deducts as $deducts_val)
					  	<tr>
							<td width="50%">{{$deducts_val->CAT}}</td>
							<td width="30%"><div>{{$fmt->formatCurrency($deducts_val->CurrentBasis, 'USD')}}
							</div></td>
							<td width="30%"><div>{{$fmt->formatCurrency($deducts_val->CurrentAmount, 'USD')}}
							</div></td>
							<td width="30%"><div>{{$fmt->formatCurrency($deducts_val->YTDBasis, 'USD')}}
							</div></td>
							<td width="30%"><div>{{$fmt->formatCurrency($deducts_val->YTDAmount, 'USD')}}
							</div></td>
						</tr>
						<?php $sub_total += $deducts_val->CurrentAmount; $deduct_total += $deducts_val->YTDAmount;?>
					@endforeach
				<tr><td width="60%">Total</td>
			   <td width="30%"></td>
			   <td width="30%"><div><b>{{$fmt->formatCurrency($sub_total, 'USD')}}</b></div></td>
			   <td width="30%"></td>

			   <td width="30%"><div><b>{{$fmt->formatCurrency($deduct_total, 'USD')}}</b></div></td>
			   </tr>	
			   </tbody>
			</table>
		</div>
		<div>
			<table  class="Table-Normal">
                <thead>
                    <tr class='table-heading'>
                        <th>Direct Deposits</th>
						<th></th>
                        <th>Amount</th>
						<th></th>
						<th>Amount</th>
                    </tr>
                </thead>
                <tbody>
					@foreach($deposit as $deposit_val)
						<tr>
							<td width="60%">{{trim($deposit_val->CAT)}}</td>
							<td width="30%"><div></div></td>
							<td width="30%"><div>{{$fmt->formatCurrency($deposit_val->CurrentAmount, 'USD')}}</div></td>
							<td width="30%"><div></div></td>
							<td width="30%"><div>{{$fmt->formatCurrency($deposit_val->YTDAmount, 'USD')}}
							</div></td>
					   </tr>
					 @endforeach
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>

