@include('includes.header')
<h3 id="msg">Check Stub for <span class="cname">D2Electric</span></h3>
@if (session('checkstub'))
	<div class="alert alert-danger">
		{{ session('checkstub') }}
	</div>
@endif
<table class="table table-edit" align="center">
	<tr>
		<form method="POST" action="{{url('checkstubD2E')}}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<th class="editempid" colspan="12">
			   	@if(Auth::check())
					@if(Auth::user()->ROLE != 'ADM')
						<p>{{ Auth::user()->FNAME }} {{ Auth::user()->LNAME }} - {{ Auth::user()->EmpsID }}</p>
					@else<br><div class="form-group">
							<div class="col-sm-12">
								<select name="employee" id="ename" class="form-control drop-box">
									<option value="" disabled selected>Select an Employee</option>
									@foreach($empid as $value)
										<option value="{{$value->EmpeID}}"@if (old('employee') == $value->EmpeID) selected="selected" @endif>{{$value->ENAME}} {{"-"}} {{ $value->EmpeID}}</option>
									@endforeach
								</select><br>
							</div>
						</div>
					@endif
					@if ($errors->has('employee'))<p class="red">{!!$errors->first('employee')!!}</p>@endif 	
					<div class="form-group">
						<div class="col-sm-12">
							<input type="text" name="checkstubDate" id="checkstubDate" class="form-control" placeholder="Choose Week Ending Date" value="{{ old('checkstubDate') }}"><br>
						</div>
					</div>
					@if ($errors->has('checkstubDate'))<p class="red">{!!$errors->first('checkstubDate')!!}</p>@endif 	
						<div class="form-group">
						<div class="col-sm-12">
							<input type="submit" class="btn btn-primary"><br><br>
						</div>
					</div>
                 @endif
			</th>
		</form>
	</tr>
</table>
@include('includes.footer')
