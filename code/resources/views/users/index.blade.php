@include('includes.header')
<br>
@if (session('status'))
	<div class="alert alert-success">
		{{ session('status') }}
	</div>
@endif	
<div>
	<a id='edit' href="{{URL::to('/auth/register')}}" class="btn btn-success date-days Add">{{trans('forms.Adusr')}}</a>
</div>
<div class="table-responsive">
	<table class="table tables table-bordered" align="center">
		<tr class="warning">
			<th>{{trans('forms.uname')}}</th>
			<th>{{trans('forms.empid')}}</th>
			<th>{{trans('forms.fname')}}</th>
			<th>{{trans('forms.lname')}}</th>
			<th>{{trans('forms.role')}}</th>
			<th colspan="2">{{trans('forms.action')}}</th>
		</tr>
		@foreach($users as $user)
			<tr>
				<td>{{$user->USERNAME}}</td>
				<td>{{$user->EmpsID}}</td>
				<td>{{$user->FNAME}}</td>
				<td>{{$user->LNAME}}</td>
				<td>{{$user->ROLE}}</td>
				<td><a href="{{route('users.edit',$user->USER_ID)}}" class="btn btn-info ed-btn date-days">{{trans('forms.edit')}}</a></td>
				<td>
					{!! Form::open(['method' => 'DELETE', 'route'=>['users.destroy', $user->USER_ID]]) !!}
					{!! Form::submit(trans('forms.del'), ['class' => 'btn ed-btn btn-danger']) !!}
					{!! Form::close() !!}
				</td>
			</tr>
		@endforeach
	</table>
	<div id="msg">
	   {!! $users->setPath('users')->render() !!}
    </div>
</div>
@include('includes.footer')