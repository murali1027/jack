@include('includes.header')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">{{trans('forms.upuser')}}</div>
				<div class="panel-body">
					{!! Form::model($user,['method' => 'PATCH', 'class'=>'form-horizontal','route'=>['users.update',$user->USER_ID]]) !!}
						<div class="form-group">
							{!! Form::label('USERNAME', trans('forms.uname'),['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('USERNAME',null,['class'=>'form-control']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('EmpsID', trans('forms.empid'),['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('EmpsID',null,['class'=>'form-control']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('FNAME', trans('forms.fname'),['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('FNAME',null,['class'=>'form-control']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('LNAME', trans('forms.lname'),['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('LNAME',null,['class'=>'form-control']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('EMAIL', trans('forms.email'),['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('EMAIL',null,['class'=>'form-control']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('domain', trans('forms.domain'),['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::select('domain',array($user->DOMAIN=>$user->DOMAIN,
								trans('menus.d2e')=>trans('menus.d2e'),trans('menus.d2s')=>trans('menus.d2s'),trans('menus.dx')=>trans('menus.dx')),
								null,['class' => 'form-control']) !!}
							</div>
						</div> 
						<div class="form-group">
							{!! Form::label('role', trans('forms.role'),['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::select('role',array($user->ROLE=>$user->ROLE,trans('menus.adm')=>trans('menus.adm'),trans('menus.app')=>trans('menus.app'),trans('menus.usr')=>trans('menus.usr')),
								null,['class' => 'form-control']) !!}
							</div>
						</div> 
						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								{!! Form::submit(trans('forms.update'), ['class' => 'btn btn-primary']) !!}
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@include('includes.footer')
