@include('includes.header')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">{{trans('forms.register')}}</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">{{trans('forms.uname')}}</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="name" value="{{ old('name') }}">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">{{trans('forms.empid')}}</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="empid" value="{{ old('empid') }}">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">{{trans('forms.fname')}}</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="fname" value="{{ old('fname') }}">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">{{trans('forms.lname')}}</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="lname" value="{{ old('lname') }}">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">{{trans('forms.email')}}</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">{{trans('forms.domain')}}</label>
							<div class="col-md-6">
								<select name="domain" class="form-control">
									<option value="">{{trans('forms.sdomain')}}</option>
									<option value="{{trans('menus.d2e')}}">{{trans('menus.d2e')}}</option>
									<option value="{{trans('menus.d2s')}}">{{trans('menus.d2s')}}</option>
									<option value="{{trans('menus.dx')}}">{{trans('menus.dx')}}</option>
								</select>
							</div>
						</div> 
						
						<div class="form-group">
							<label class="col-md-4 control-label">{{trans('forms.role')}}</label>
							<div class="col-md-6">
								<select name="role" class="form-control">
									<option value="">{{trans('forms.srole')}}</option>
									<option value="{{trans('menus.adm')}}">{{trans('menus.adm')}}</option>
									<option value="{{trans('menus.app')}}">{{trans('menus.app')}}</option>
									<option value="{{trans('menus.usr')}}">{{trans('menus.usr')}}</option>
								</select>
							</div>
						</div> 
						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">{{trans('forms.register')}}</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@include('includes.footer')
