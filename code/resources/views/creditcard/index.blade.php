@include('includes.header')
<h3 id="msg">Edit for Credit Cards</h3>
<table class="table table-edit" align="center">
	<tr class="editfileds">
		<th class="tableheader">Date</th>
		<th class="tableheader">CCardID</th>
		<th class="tableheader">Location</th>
		<th class="tableheader">Job</th>
		<th class="tableheader">CostCode</th>
		<th class="tableheader">Category</th>
		<th class="tableheader">Description</th>
		<th class="tableheader">Amount</th>
		<th class="tableaction" colspan="2">Action</th>
	</tr>
	@foreach($ccard as $value)
		@if($value->Approved == 'R')
			<tr class="reject">
	    @elseif($value->Approved == 'A')
			<tr class="approve">
		@else
			<tr>
		@endif
		        <td>{{$value->RDATE}}</td>
		        <td>{{$value->CCardID}}</td>
				<td>{{$value->LOC}}</td> 
				<td>{{$value->JobID}}</td>
				<td>{{$value->CostCode}}</td>
				<td>{{$value->CAT}}</td>
				<td>{{$value->DSCR}}</td>
				<td>{{$value->AMOUNT}}</td>
				@if($value->Approved == 'A')
					<td><a href="{{route('creditcard.edit',$value->CCARD_ID)}}" class="btn btn-info ed-btn date-days" disabled>Edit</a></td>
					<td>
						{!! Form::open(['method' => 'DELETE', 'route'=>['creditcard.destroy', $value->CCARD_ID]]) !!}
						{!! Form::submit('Delete', ['class' => 'btn btn-danger ed-btn','disabled']) !!}
						{!! Form::close() !!}
					</td>
				@else
					<td><a href="{{route('creditcard.edit',$value->CCARD_ID)}}" class="btn ed-btn btn-info date-days">Edit</a></td>
					<td>
						{!! Form::open(['method' => 'DELETE', 'route'=>['creditcard.destroy', $value->CCARD_ID]]) !!}
						{!! Form::submit('Delete', ['class' => 'btn btn-danger ed-btn']) !!}
						{!! Form::close() !!}
					</td>
				@endif
			</tr>
	@endforeach
</table>
@include('includes.footer')
