@include('includes.header')
	<div class="container-fluid" >
		<div class="row" >
			<div class="col-md-12">
				<div class="panel">
					<table class="table table-bordered" align="center">
						<tr>
							<td>
							    <h2 class="msg">Entry for <span class="cname">Credit Card</span></h2>
								<div class="panel-body">
									@if (session('status'))
										<div class="alert alert-success">
											{{ session('status') }}
										</div>
									@endif
									{!! Form::open(['url' => 'creditcard','class'=>'form-horizontal','id'=>'ccard',
										'enctype'=>'multipart/form-data']) !!}
										<input type="hidden" name="role" id="role" value="{{ Auth::user()->ROLE }}">
										<div class="form-group">
									    	{!! Form::label('test', 'Date',['class' => 'col-sm-4 control-labels']) !!}
									    	<div class="col-sm-7">
                                           		{!! Form::text('RDATE',null,['class'=>'form-control drop-box','tabindex'=> '1' ,'id'=>'test','placeholder'=>'Choose Credit Card Date']) !!}
											</div>
										</div>

										<div class="form-group">
										    {!! Form::label('ccard', 'Credit card',['class' => 'col-sm-4 control-labels']) !!}
											<div class="col-sm-7">
												<select name="ccard" id="ccard" tabindex="2"
												    class="form-control drop-box" />
													<option value="" disabled selected>Select a Credit card</option>
													@foreach($ccard as $cvalue)
													<option value="{{$cvalue->CCardID}}">{{$cvalue->CCardID}} - {{$cvalue->DSCR}}</option>
													@endforeach
												</select>
											</div>
										</div>

										<div class="form-group">
											{!! Form::label('jobid', 'Job',['class' => 'col-sm-4 control-labels']) !!}
											<div class="col-sm-7">
												<select name="jobid" id="jobcat" class="form-control drop-box" tabindex="3">
													<option value="" disabled selected>Select a Job</option>
													@foreach($jobcat as $value)
													<option data-id="{{$value->Extra}}" value="{{$value->JobID}}">{{$value->JOB}} - {{$value->JNAME}}</option>
													@endforeach
												</select>
											</div>
										</div>
                                          {!! Form::hidden('extra',null,['id'=>'extra']) !!}
										<div class="form-group">
											{!! Form::label('ccode', 'Cost Code',['class' => 'col-sm-4 control-labels']) !!}
											<div class="col-sm-7">
												<select name="ccode" id="ccode" class="form-control drop-box" tabindex="4">
												   <option value="" disabled selected>Select a Cost Code</option>
												   <option value=""></option>
												</select>
											</div>
										</div>

										<div class="form-group">
										    {!! Form::label('cat', 'Cat',['class' => 'col-sm-4 control-labels']) !!}
											<div class="col-sm-7">
												<select name="cat" id="cat" class="form-control drop-box" tabindex="5">
													  <option value="" disabled selected>Select a Catagory</option>
												</select>
											</div>
										</div>  
										
										<div class="form-group">
									    	{!! Form::label('location', 'Location',['class' => 'col-sm-4 control-labels']) !!}
									    	<div class="col-sm-7">
                                           		{!! Form::text('LOC',null,['class'=>'form-control drop-box','tabindex'=> '6','placeholder'=>'Enter Your Location']) !!}
											</div>
										</div>
										
									   <div class="form-group">
									    	{!! Form::label('desc', 'Description',['class' => 'col-sm-4 control-labels']) !!}
									    	<div class="col-sm-7">
                                           		{!! Form::text('DSCR',null,['class'=>'form-control drop-box','tabindex'=> '7','placeholder'=>'Enter Your Description']) !!}
											</div>
										</div>

										<div class="form-group">
									    	{!! Form::label('amount', 'Amount',['class' => 'col-sm-4 control-labels']) !!}
									    	<div class="col-sm-7">
                                           		{!! Form::text('AMOUNT',null,['class'=>'form-control drop-box','tabindex'=> '8','onkeypress'=>'return validateFloatKeyPress(this,event)', 'placeholder'=>'Enter Your Amount']) !!}
											</div>
										</div>
                                        
										<div class="form-group">
									    	{!! Form::label('file', 'Attachment',['class' => 'col-sm-4 control-labels']) !!}
									    	<div class="col-sm-7">
                                           		{!! Form::file('file',null,['class'=>'form-control drop-box','tabindex'=> '9']) !!}
											</div>
										</div>
											
										<div class="form-group">
											<div class="col-md-6 col-md-offset-3"></br>
											    {!! Form::submit('Submit', ['class' => 'btn btn-primary','tabindex'=> '10']) !!}
											</div>
										</div>
									{!! Form::close() !!}
								</div>
							</td> 
						</tr>
					</table>
				</div>
				<div class="pull-left"><strong>User: </strong><span  id="user_name">{{ Auth::user()->FNAME }} {{ Auth::user()->LNAME }}</span></div>
				<div class="pull-right"><strong>Date: </strong><span  id="currentdate">{{date('m/d/Y')}}</span></div>
			</div>
		</div>
	</div>
@include('includes.footer')
