<!DOCTYPE html>
<html> 
<head>
	<meta charset="utf-8" />
	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame Remove this if you use the .htaccess -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ trans('menus.title') }}</title>
	<meta name="description" content="" /><meta/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
	<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
	<link rel="shortcut icon" href="/favicon.ico" />
	<link rel="apple-touch-icon" href="/apple-touch-icon.png" />
	<link href="{{URL::to('css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{URL::to('css/app.css')}}"   rel="stylesheet" type="text/css">
	<link href="{{URL::to('css/jquery-ui.css')}}"  rel="stylesheet"  type="text/css">
	<link href="{{URL::to('css/jquery.dataTables.min.css')}}"  rel="stylesheet"  type="text/css">
	<script type="text/javascript" src="{{URL::to('js/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('js/jquery-ui.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('js/bootstrap.min.js')}}"></script>
	<script  type="text/javascript" src="{{URL::to('js/jquery.validate.min.js')}}"></script>
	<script type="text/javascript">
    	$.ajaxSetup({
        	headers: {
            	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        	}
   		 });
	</script>
</head>
<body>
    <div class="container outer-border">
		<header>
			<div>
				<img src="{{URL::to('images/header3.jpg')}}" width="100%">
			</div>
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<!-- <a class="navbar-brand" href="#">Project name</a> -->
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<li><a href="{{ url('/') }}" class="strclr">{{ trans('menus.home') }}</a></li>
							<li class="dropdown">
								<a href="{{ url('/') }}"  class="dropdown-toggle strclr" data-toggle="dropdown" role="button"
								 aria-expanded="false">{{ trans('menus.labor') }}<span class="caret"></span></a>
								<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                                    <li class="dropdown-submenu">
                                        <a tabindex="-1" href="#">{{ trans('menus.ds') }}</a>
                                        <ul class="dropdown-menu">
                                            @if(Auth::check())
										 	@if(Auth::user()->DOMAIN == trans('menus.d2s') || Auth::user()->DOMAIN == trans('menus.dx') || Auth::user()->ROLE != trans('menus.usr'))
											<li><a class="strclr" href="{{URL::to('ViewLaborD2S')}}">{{ trans('menus.enter') }}</a></li>
											<li><a class="strclr" href="{{URL::to('editlaborsD2S')}}">{{ trans('menus.edit') }}</a></li>
											@if(Auth::user()->ROLE == trans('menus.app'))
											<li><a class="strclr" href="{{URL::to('approveD2S')}}">{{ trans('menus.Approve') }}</a></li>
										    @endif
											<li><a class="strclr" href="{{ url('HoursD2S')}}" >{{ trans('menus.hours') }}</a></li>
											<li><a class="strclr" href="{{ url('checkstubD2S')}}" >{{ trans('menus.cstub') }}</a></li>
											@endif
											@endif
										</ul>
									</li><br>
									<li class="dropdown-submenu">
										<a tabindex="-1" href="#">{{ trans('menus.de') }}</a>
										<ul class="dropdown-menu">
										    @if(Auth::check())
										    @if(Auth::user()->DOMAIN == trans('menus.d2e') || Auth::user()->DOMAIN == trans('menus.dx') || Auth::user()->ROLE != trans('menus.usr'))	
											<li><a class="strclr" href="{{URL::to('ViewLaborD2E')}}">{{ trans('menus.enter') }}</a></li>
											<li><a class="strclr" href="{{URL::to('editlaborsD2E')}}">{{ trans('menus.edit') }}</a></li>
											@if(Auth::user()->ROLE == trans('menus.app'))
											<li><a class="strclr" href="{{URL::to('approveD2E')}}">{{ trans('menus.Approve') }}</a></li>
										    @endif
											<li><a class="strclr" href="{{ url('HoursD2E')}}">{{ trans('menus.hours') }}</a></li>
											<li><a class="strclr" href="{{ url('checkstubD2E')}}">{{ trans('menus.cstub') }}</a></li>
											@endif
											@endif
										</ul>
									</li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle strclr" data-toggle="dropdown" role="button" aria-expanded="false">
								{{ trans('menus.expense') }}
								<span class="caret"></span></a>
								<ul class="dropdown-menu">
							<!--		 <li class="dropdown-submenu">
										<a tabindex="-1" href="#">{{ trans('menus.ccard') }}</a>
										<ul class="dropdown-menu">
										    @if(Auth::check())
												<li><a class="strclr" href="{{URL::to('creditcard/create')}}">{{trans('menus.encard')}}</a></li>
												<li><a class="strclr" href="{{URL::to('creditcard')}}">{{ trans('menus.edcard') }}</a></li>
												@if(Auth::user()->ROLE == trans('menus.app'))
												<li><a class="strclr" href="{{URL::to('#')}}">{{ trans('menus.apcard') }}</a></li>
												@endif
											 @endif  
										</ul>
									</li>
									<li><a class="strclr" href="#">{{ trans('menus.invoice') }}</a></li>
								-->	<li class="dropdown-submenu">
										<a tabindex="-1" href="#">{{ trans('menus.reims') }}</a>
										<ul class="dropdown-menu">
										    <li class="dropdown-submenu">
										       	<a tabindex="-1" href="#">{{ trans('menus.ds') }}</a>
										       	<ul class="dropdown-menu">
										       	    @if(Auth::check())
										       	    @if(Auth::user()->DOMAIN == trans('menus.d2s') || Auth::user()->DOMAIN == trans('menus.dx') || Auth::user()->ROLE != trans('menus.usr') )	
										    		<li>
											    		@if(Auth::user()->ROLE == trans('menus.usr'))
											    		<a class="strclr" href="{{URL::to('enter-reimsD2S/create')}}">
											    		{{ trans('menus.enreims') }}</a>
											    		@else
											    		<a class="strclr" href="{{URL::to('enterReims')}}">
											    		{{ trans('menus.enreims') }}</a>
											    		@endif
											    	</li>
													<li>
													    <a class="strclr" href="{{URL::to('enter-reimsD2S')}}">{{ trans('menus.edreims') }}</a>
													</li>
													@if(Auth::user()->ROLE == trans('menus.app'))
													<li>
														<a class="strclr" href="{{URL::to('approve-reimsD2S')}}">{{ trans('menus.apreims') }}</a>
													</li>
													@endif
													@if(Auth::user()->ROLE == trans('menus.fapp'))
													<li>
														<a class="strclr" href="{{URL::to('FinancialD2S')}}">{{ trans('menus.fapreims') }}</a>
													</li>
													@endif
											        @endif
											        @endif
											   </ul>
										    </li><br>
										    <li class="dropdown-submenu">
										       	<a tabindex="-1" href="#">{{ trans('menus.de') }}</a>
										       	<ul class="dropdown-menu">
										         	@if(Auth::check())
										       	    @if(Auth::user()->DOMAIN == trans('menus.d2e') || Auth::user()->DOMAIN == trans('menus.dx') || Auth::user()->ROLE != trans('menus.usr') )
													<li>	
										    		@if(Auth::user()->ROLE == trans('menus.usr'))
										    		    <a class="strclr" href="{{URL::to('enter-reimsD2E/create')}}">
										    			{{ trans('menus.enreims')}}</a>
										    		@else
										    			<a class="strclr" href="{{URL::to('enterReime')}}">{{ trans('menus.enreims')}}</a>
										    		@endif
										    		</li>
										    		<li><a class="strclr" href="{{URL::to('enter-reimsD2E')}}">{{ trans('menus.edreims')}}</a>
													</li>
													@if(Auth::user()->ROLE == trans('menus.app'))
													<li><a class="strclr" href="{{URL::to('approve-reimsD2E')}}">{{ trans('menus.apreims')}}</a></li>
													@endif
													@if(Auth::user()->ROLE == trans('menus.fapp'))
													<li>
														<a class="strclr" href="{{URL::to('FinancialD2E')}}">{{ trans('menus.fapreims') }}</a>
													</li>
													@endif
											        @endif
											        @endif
											    </ul>
										    </li>
										</ul>
									</li>
								</ul>
							</li>

							<li class="dropdown">
								<a href="#" class="dropdown-toggle strclr" data-toggle="dropdown" role="button" aria-expanded="false">
								{{ trans('menus.help')}}<span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="{{URL::to('/tickets/show')}}" class="strclr">{{ trans('menus.ticket')}}</a></li>
									<li><a href="{{url('/tickets/create')}}" class="strclr">{{ trans('menus.adticket')}}</a></li>
								</ul>
							</li>
							@if(Auth::check())
						    @if(Auth::user()->ROLE == trans('menus.adm'))
							<li class="dropdown">
								<a href="#" class="dropdown-toggle strclr" data-toggle="dropdown" role="button" aria-expanded="false">
								{{ trans('menus.admin')}}<span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="{{ url('users') }}" class="strclr">{{ trans('menus.user')}}</a></li>
									<li><a href="{{ url('tickets') }}" class="strclr">{{ trans('menus.edticket')}}</a></li>
									<li><a href="{{ url('ImportExport')}}" class="strclr">{{ trans('menus.report')}}</a></li>
									<li><a href="{{ url('weeklyreportD2S')}}" class="strclr">{{ trans('menus.weekly')}}</a></li>
								</ul>
							</li>
							@endif
						    @endif
							@if (Auth::guest())
							<li><a href="{{ url('/auth/login') }}" class="strclr">{{ trans('menus.login')}}</a></li>
							@else
							<li><a href="{{ url('/auth/logout') }}" class="strclr">{{ trans('menus.logout')}}</a></li>	
							@endif
						</ul>
					</div><!--/.nav-collapse -->
				</div><!--/.container-fluid -->
			</nav>
		</header>