	</div>
	<footer class="footer">
		<div class="container footers">
			<p class="text-muted"></p>
		</div>
		<script  type="text/javascript" src="{{URL::to('js/jquery.dataTables.min.js')}}"></script>
		<script type="text/javascript" src="{{URL::to('js/common.js')}}"></script>
		<script type="text/javascript" src="{{URL::to('js/app.js')}}"></script>
		<script  type="text/javascript" src="{{URL::to('js/edit.js')}}"></script>
		<script  type="text/javascript" src="{{URL::to('js/laborshours.js')}}"></script>
		<script  type="text/javascript" src="{{URL::to('js/laborehours.js')}}"></script>
		<script  type="text/javascript" src="{{URL::to('js/laborsapprove.js')}}"></script>
	    <script  type="text/javascript" src="{{URL::to('js/laboreapprove.js')}}"></script>
		<script  type="text/javascript" src="{{URL::to('js/laborsedit.js')}}"></script>
	    <script  type="text/javascript" src="{{URL::to('js/laboreedit.js')}}"></script>
	    <script  type="text/javascript" src="{{URL::to('js/reims.js')}}"></script>
	    <script  type="text/javascript" src="{{URL::to('js/enterReims.js')}}"></script>
	    <script  type="text/javascript" src="{{URL::to('js/enterReime.js')}}"></script>
	    <script  type="text/javascript" src="{{URL::to('js/ccard.js')}}"></script>
		<div class="container footerimg">
			<font class="lowerNav fmenu">
				<a href="#">Home</a> |
				<a href="#">Payroll</a> | 
				<a href="#">Help</a> |
			</font>
			<br><font class="small">&copy {{date("Y")}} AZ Web Apps &nbsp;&#149;&nbsp; Developed & Maintained by DIR
		</div>
	</footer>
	<script>
	var APP_URL = {!! json_encode(url('/')) !!};
	$('#jobid').on('change',function(e)
	{
		var job_id = e.target.value;
		var extra = $('#jobid option:selected').attr('data-id');
		$('#extra').val(extra);
		$.get(APP_URL + '/PostCcod?job_id=' + job_id + '&extra='+ extra, function(data){
			$('#ccode').empty();
			$('#ccode').append('<option value="" disabled selected>Select a Cost Code</option>');
			$.each(data,function(index,subcatObj){
				$('#ccode').append('<option value="'+subcatObj.CostCode+'">'+subcatObj.CostCode+'&nbsp;'+'-' +'&nbsp;'+subcatObj.DSCR+'</option>');
			});
		});
	});

	$('#jobid').on('change',function(e)
	{
		var job_id = e.target.value;
		$.get(APP_URL +'/Postcerts?job_id=' + job_id, function(data){
			$('#certs').empty();
			$.each(data,function(index,subcatObj){
				$('#certs').append('<option value="'+subcatObj.CERTS+'">'+subcatObj.CERTS+'</option>');
			});
		});
	});

	$('#jobid').on('change',function(e)
	{
		var job_id = e.target.value;
		$.get(APP_URL + '/Postulocal?job_id=' + job_id, function(data){
			$('#ulocal').empty();
			if(data.length ==  0){
				 $('#ulocal').val("");
			}
			else{
				$.each(data,function(index,subcatObj){
				$('#ulocal').val(subcatObj.ULOCAL);
			});
			}
		});
	});

	$('#jobid').on('change',function(e)
	{
		var job_id = e.target.value;
		$.get(APP_URL + '/Postcerte?job_id=' + job_id, function(data){
			$('#certe').empty();
			$.each(data,function(index,subcatObj){
				$('#certe').append('<option value="'+subcatObj.CERTE+'">'+subcatObj.CERTE+'</option>');
			});
		});
	});

	$('#jobcat').on('change',function(e)
	{
		var job_id = e.target.value;
		var extra = $('#jobcat option:selected').attr('data-id');
		$('#extra').val(extra);
		$.get(APP_URL + '/PostJobcat?job_id=' + job_id + '&extra='+ extra, function(data){
			$('#ccode').empty();
			$('#ccode').append('<option value="" disabled selected>Select a Cost Code</option>');
			$.each(data,function(index,subcatObj){
				$('#ccode').append('<option value="'+subcatObj.CostCode+'">'+subcatObj.CostCode+'&nbsp;'+ '-' +'&nbsp;'+subcatObj.DSCR+'</option>');
			});
		});
	});
	
    $('#ccode').on('change',function(e)
	{
		var jobid=document.getElementById('jobcat').value;
		var ccode = e.target.value;
		$.get(APP_URL + '/Postcat?job_id=' + jobid + '&ccode='+ ccode, function(data){
			$('#cat').empty();
			$('#cat').append('<option value="" disabled selected>Select a Category</option>');
			$.each(data,function(index,subcatObj){
				$('#cat').append('<option value="'+subcatObj.CAT+'">'+subcatObj.CATEGORY+'</option>');
				var usedNames = {};
			   $(" #cat option ").each(function () {
			       if (usedNames[this.value]) {
			           $(this).remove();
			       } else {
			           usedNames[this.value] = this.text;
			       }
			   });
			});
		});
	});
	</script>
	<script>
		var seen = {};
		jQuery('.removesel').children().each(function() {
		    var txt = jQuery(this).attr('value');
		    if (seen[txt]) {
		        jQuery(this).remove();
		    } else {
		        seen[txt] = true;
		    }
		});
	</script>
	<script>
		$(document).ready(function(){
    		$('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
</body>
</html>
