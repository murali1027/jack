@include('includes.header')
	<div class="container-fluid" >
		<div class="row" >
			<div class="col-md-12">
				<div class="panel">
					<table class="table table-bordered table-reims" align="center">
						<tr>
							<td>
							    <h2 class="msg">{{trans('tables.reimburse')}} <span class="cname"> {{ trans('menus.de') }} </span>
							    </h2>
								<div class="panel-body">
									@if (session('status'))
										<div class="alert alert-success">
											{{ session('status') }}
										</div>
									@endif
									{!! Form::open(['url' => 'enter-reimsD2E','class'=>'form-horizontal',
									'id'=>'reims','enctype'=>'multipart/form-data']) !!}
										<input type="hidden" name="role" id="role" value="{{ Auth::user()->ROLE }}">
										<input type="hidden" id="enterReime" value="enterReime">
										<div class="form-group">
									    	{!! Form::label('test', trans('forms.date'),['class' => 'col-sm-4 control-labels']) !!}
									    	<div class="col-sm-7">
                                           		{!! Form::text('RDATE',null,['class'=>'form-control drop-box','tabindex'=> '1' ,'id'=>'test','placeholder'=>'Choose Reimbursement Date']) !!}
											</div>
										</div>
                                       <div class="form-group">
										    {!! Form::label('ename', trans('forms.emp'),['class' => 'col-sm-4 control-labels']) !!}
											<div class="col-sm-7">
												<select name="ename" id="ename" tabindex="2"  
												    class="form-control drop-box empname" />
													<option value="" disabled selected>Select an Employee</option>
													@foreach($empid as $value)
													<option value="{{$value->EmpeID}}" @if (old('ename') == $value->EmpeID) selected="selected" @endif>{{$value->ENAME}} {{"-"}} {{ $value->EmpeID}}</option>
													@endforeach
											    </select>
											</div>
										</div>
                                       <div class="form-group">
											{!! Form::label('jobid', trans('forms.job'),['class' => 'col-sm-4 control-labels']) !!}
											<div class="col-sm-7">
												<select name="jobid" id="jobcat" class="form-control drop-box" tabindex="3">
													<option value="" disabled selected>Select a Job</option>
													@foreach($jobcat as $value)
													<option data-id="{{$value->Extra}}" value="{{$value->JobID}}">{{$value->JOB}} -  {{$value->JNAME}}</option>
													@endforeach
												</select>
											</div>
										</div>
                                        {!! Form::hidden('extra',null,['id'=>'extra']) !!}
										<div class="form-group">
											{!! Form::label('ccode', trans('forms.ccode'),['class' => 'col-sm-4 control-labels']) !!}
											<div class="col-sm-7">
												<select name="ccode" id="ccode" class="form-control drop-box" tabindex="4">
												   <option value="" disabled selected>Select a Cost Code</option>
												    <option value=""></option>
												</select>
											</div>
										</div>

										<div class="form-group">
										    {!! Form::label('cat', trans('forms.cat'),['class' => 'col-sm-4 control-labels']) !!}
											<div class="col-sm-7">
												<select name="cat" id="cat" class="form-control drop-box" tabindex="5">
													  <option value="" disabled selected>Select a Category</option>
												</select>
											</div>
										</div>  
										
										<div class="form-group">
									    	{!! Form::label('location', trans('forms.loc'),['class' => 'col-sm-4 control-labels']) !!}
									    	<div class="col-sm-7">
                                           		{!! Form::text('LOC',null,['class'=>'form-control drop-box','tabindex'=> '6','placeholder'=>'Enter Your Location']) !!}
											</div>
										</div>
										
									   <div class="form-group">
									    	{!! Form::label('desc', trans('forms.desc'),['class' => 'col-sm-4 control-labels']) !!}
									    	<div class="col-sm-7">
                                           		{!! Form::text('DSCR',null,['class'=>'form-control drop-box','tabindex'=> '7','placeholder'=>'Enter Your Description']) !!}
											</div>
										</div>

										<div class="form-group">
									    	{!! Form::label('amount', trans('forms.amt'),['class' => 'col-sm-4 control-labels']) !!}
									    	<div class="col-sm-7">
                                           		{!! Form::text('AMOUNT',null,['class'=>'form-control drop-box','tabindex'=> '8','onkeypress'=>'return validateFloatKeyPress(this,event)', 'placeholder'=>'Enter Your Amount']) !!}
											</div>
										</div>
                                        
										<div class="form-group">
									    	{!! Form::label('attach', trans('forms.attach'),['class' => 'col-sm-4 control-labels']) !!}
									    	<div class="col-sm-7">
                                           		{!! Form::file('file',null,['class'=>'form-control drop-box','tabindex'=> '9']) !!}
											</div>
										</div>
											
										<div class="form-group">
											<div class="col-md-6 col-md-offset-3"></br>
											    {!! Form::submit(trans('forms.submit'), ['class' => 'btn btn-primary reims-btn','tabindex'=> '10']) !!}
											</div>
										</div>
									{!! Form::close() !!}
								</div>
							</td> 
						</tr>
					</table>
				</div>
				<div class="pull-left"><strong>{{trans('forms.user')}}: </strong><span  id="user_name">{{ Auth::user()->FNAME }} {{ Auth::user()->LNAME }}</span></div>
				<div class="pull-right"><strong>{{trans('forms.date')}}: </strong><span  id="currentdate">{{date('m/d/Y')}}</span></div>
			</div>
		</div>
	</div>
	<div class="clear">
		<div id="clear">

	</div>
	</div>
		@include('includes.footer')
