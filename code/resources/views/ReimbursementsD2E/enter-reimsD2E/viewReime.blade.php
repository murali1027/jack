<div class="grouping"></div>
	<table class="table table-edit reims" align="center">
			<tr>
				<th class="editempsid" colspan="12">
					<div class="form-group">
						<div class="col-sm-12">
						 @foreach($reime as $value) @endforeach{{$value->ENAME}} - {{$value->EmpeID}}
					    </div>
					</div>
				</th>
			</tr>
				<tr class="editfileds">
					<th class="tableheader">{{trans('forms.date')}}</th>
					<th class="tableheader">{{trans('forms.location')}}</th>
					<th class="tableheader">{{trans('forms.job')}}</th>
					<th class="tableheader">{{trans('forms.ccode')}}</th>
					<th class="tableheader">{{trans('forms.category')}}</th>
					<th class="tableheader">{{trans('forms.desc')}}</th>
					<th class="tableheader">{{trans('forms.amt')}}</th>
				</tr>
				<?php 
				    $fmt = new NumberFormatter( 'en_US', NumberFormatter::CURRENCY );
					$amount=0;
				?>
				@foreach($reime as $value)
			    <tr>
					<td>{{$value->Date}}</td>
					<td>{{$value->Location}}</td> 
					<td>{{$value->Job}}</td>
					<td>{{$value->CostCode}}</td>
					<td>{{$value->Category}}</td>
					<td>{{$value->Description}}</td>
					<td><div>{{$value->Amount}}</div></td><?php $amount +=$value->Amount;?>
				</tr>
				@endforeach
				<tr>
					<td><b>{{trans('forms.total')}}</b></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td><div><b>{{$fmt->formatCurrency($amount, 'USD')}}</b></div></td>
				</tr>
			</table>
 			 </br>
	