@include('includes.header')
<h3 class="msg">Reimbursements Edit for <span class="cname">{{ trans('menus.de') }}</span></h3>
@forelse($empe as $empeVal)
<table class="table table-edit" align="center">
	<tr>
	<th class="reimsempid" colspan="12">
		<div class="form-group">
			<div class="col-sm-12">
				   {{$empeVal->Ename}} - {{$empeVal->EmpeID}}
			</div>
		</div>
	</th>
	</tr>
	<tr class="editfileds">
		<th class="tableheader">Date</th>
		<th class="tableheader">Loc</th>
		<th class="tableheader">Job</th>
		<th class="tableheader">CostCode</th>
		<th class="tableheader">Cat</th>
		<th class="tableheader">Description</th>
		<th class="tableheader">Attach</th>
		<th class="tableheader">Amount</th>
		<th class="tableaction" colspan="2">Action</th>
	</tr>
	<?php 
	    $reime=DB::table('reime as r')
		        ->join('empe as e', 'r.EmpeID', '=', 'e.EmpeID')
				->join('job as j', 'r.JobID', '=', 'j.JobID')
				->where('e.EmpeID', $empeVal->EmpeID)
				->where('r.DELETED', '=', 'N')
				->where('r.DOWNLOADED', '=', 'N')
				->whereIn('r.APPROVED',['N','R','I'])
				->select(DB::raw('CONCAT(r.JobID, "-", j.JNAME)  Job'),'e.ENAME as Ename','r.EmpeID as EmpeID','r.RDATE as Date',
					    'r.CostCode as CostCode','r.LOC as Location','r.DSCR as Description', 'r.CAT as Category','r.AMOUNT as Amount',
				        'r.REIME_ID as Reime_ID','r.APPROVED as Approved','r.ATTACH as Attach',
				        DB::raw('(IF(e.APVTYPE = "MANAGER", e.MANAGER, j.APPROVER)) as Approver') )
				->orderBy('r.RDATE','ASC')
				->get();?>
	@foreach($reime as $reimeVal)
    @if($reimeVal->Approved == 'R')
		<tr class="reject">
	@elseif($reimeVal->Approved == 'I')
		<tr class="approve">
	@else
		<tr>
	@endif
		    <td width="70">{{$reimeVal->Date}}</td>
			<td width="70">{{$reimeVal->Location}}</td> 
			<td><span data-toggle="tooltip" class="red-tooltip" data-placement="right" title="{{$reimeVal->Job}}">{{substr($reimeVal->Job,0,10)}}</span></td>
		    	<?php $CostCode=DB::table('ccod')->where('CostCode',$reimeVal->CostCode)->first();?>
		    	<td><span data-toggle="tooltip" class="red-tooltip" data-placement="right" title="{{$reimeVal->CostCode}} - {{$CostCode->DSCR}}">{{$reimeVal->CostCode}} - {{substr($CostCode->DSCR,0,2)}}</span></td>
			<td>{{$reimeVal->Category}}</td>
			<td width="100">{{$reimeVal->Description}}</td>
			<?php $imagepath = env('UPLOAD_REIME');$image=$reimeVal->Attach;$ext = pathinfo($image, PATHINFO_EXTENSION);?>
			@if($reimeVal->Attach == '')
	           <td><img height="23" src="{{url('images/cross.jpg')}}"></td>
	        @elseif($ext == 'pdf')
	          <td><a href="<?php echo url('pdf.php')?>?img={{$reimeVal->Attach}}&path=<?php echo $imagepath?>" target="_blank">
	         	    <img height="23" src="{{url('images/attachment.png')}}"></a></td>
	        @elseif($ext == 'txt')
	          <td><a href="<?php echo url('attachement')?>?file={{$reimeVal->Attach}}" target="_blank">
	          		<img height="23" src="{{url('images/attachment.png')}}"></a></td>
	        @else
	        <td>
	        	<a href="<?php echo url('attachement')?>?file={{$reimeVal->Attach}}" target="_blank">
	        		<img height="23" src="{{url('images/attachment.png')}}"></a>
	        </td>
	        @endif
			<td>{{$reimeVal->Amount}}</td>
			@if($reimeVal->Approved == 'I')
				<td><a href="{{route('enter-reimsD2E.edit',$reimeVal->Reime_ID)}}" class="btn btn-info ed-btn date-days" disabled>Edit</a></td>
				<td>
					{!! Form::open(['method' => 'DELETE', 'route'=>['enter-reimsD2S.destroy', $reimeVal->Reime_ID]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger ed-btn','disabled']) !!}
					{!! Form::close() !!}
				</td>
			@else
				<td><a href="{{route('enter-reimsD2E.edit',$reimeVal->Reime_ID)}}" class="btn btn-info ed-btn date-days">Edit</a></td>
				<td>
					{!! Form::open(['method' => 'DELETE', 'route'=>['enter-reimsD2S.destroy', $reimeVal->Reime_ID]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger ed-btn']) !!}
					{!! Form::close() !!}
				</td>
			@endif
		</tr>
		@endforeach
</table>
<div class="grouping"></div>
@empty
<div class="msg">
 <p class="red">No Rembursement Record to Edit.</p>
</div>
@endforelse

@include('includes.footer')
