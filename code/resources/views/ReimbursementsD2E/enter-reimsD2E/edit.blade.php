@include('includes.header')
<div class="container-fluid">
	<div class="row" >
		<div class="col-md-12">
			<div class="panel">
				<table class="table table-bordered" align="center">
					<tr>
						<td> 
							<h2 class="msg">Update Reimbursements for <span class="cname">{{ trans('menus.de') }}</span></h2>
							<div class="col-md-11 ename" align="center">
								<table  class="empname" width="57%">
									<tr>
										<th class="week-day emp-name" colspan="8">
										<?php $empname=DB::table('empe')->where('EmpeID',$edit->EmpeID)->first();?>
											<p class="emp-id">{{$empname->ENAME}}- {{ $edit->EmpeID }}</p>
										</th>
									</tr>
								</table><br>
							</div>
							<div class="panel-body">
								{!! Form::model($edit,['method' => 'PATCH', 'class'=>'form-horizontal', 'id'=>'reims',
								 'enctype'=>'multipart/form-data','route'=>['enter-reimsD2E.update',$edit->REIME_ID]])!!}
									
									<div class="form-group">
									    {!! Form::label('test', 'Date',['class' => 'col-sm-4 control-labels']) !!}
									    <div class="col-sm-7">
                                           	{!! Form::text('RDATE',null,['class'=>'form-control drop-box','tabindex'=> '1' ,'id'=>'test','placeholder'=>'Choose Reimbursement Date']) !!}
										</div>
									</div>

									<div class="form-group">
										{!! Form::label('jobid', 'Job',['class' => 'col-sm-4 control-labels']) !!}
										<div class="col-sm-7">
											<select name="jobid" id="jobcat" class="form-control drop-box" tabindex="3">
											    @foreach($jname as $value)
												    <option value="{{$edit->JobID}}">{{$edit->JobID}} {{$edit->EXTRA}} - {{$value->JNAME}}</option>
											    @endforeach	
												@foreach($jobcat as $value)
													<option data-id="{{$value->Extra}}" value="{{$value->JobID}}">{{$value->JOB}} - {{$value->JNAME}}</option>
												@endforeach
											</select>
										</div>
									</div>

									<div class="form-group">
										{!! Form::label('ccode', 'Cost Code',['class' => 'col-sm-4 control-labels']) !!}
										<div class="col-sm-7">
											<select name="ccode" id="ccode" class="form-control drop-box removesel" tabindex="4">
											   <?php $CostCode=DB::table('ccod')->where('CostCode',$edit->CostCode)->first();?>
												<option value="{{$edit->CostCode}}">{{$edit->CostCode}} - {{$CostCode->DSCR}}</option>
											   @foreach($ccode as $value)
												<option value="{{$value->CostCode}}">{{$value->CostCode}} - {{$value->DSCR}}</option>
												@endforeach
											</select>
										</div>
									</div>

									<div class="form-group">
									    {!! Form::label('cat', 'Cat',['class' => 'col-sm-4 control-labels']) !!}
										<div class="col-sm-7">
											<select name="cat" id="cat" class="form-control drop-box removesel" tabindex="5">
											<?php $ccat=DB::table('jobcat')->where('CAT',$edit->CAT)->first();?>
										        <option value="{{$edit->CAT}}">{{$edit->CAT}}-{{$ccat->DSCR}}</option>
												@foreach($cat as $value)
												<option value="{{$value->CAT}}">{{$value->DSCR}}</option>
												@endforeach
											</select>
										</div>
									</div>  
										
									<div class="form-group">
								    	{!! Form::label('location', 'Location',['class' => 'col-sm-4 control-labels']) !!}
								    	<div class="col-sm-7">
                                       		{!! Form::text('LOC',null,['class'=>'form-control drop-box','tabindex'=> '6','placeholder'=>'Enter Your Location']) !!}
										</div>
									</div>
										
								   <div class="form-group">
								    	{!! Form::label('desc', 'Description',['class' => 'col-sm-4 control-labels']) !!}
								    	<div class="col-sm-7">
                                       		{!! Form::text('DSCR',null,['class'=>'form-control drop-box','tabindex'=> '7','placeholder'=>'Enter Your Description']) !!}
										</div>
									</div>

									<div class="form-group">
								    	{!! Form::label('amount', 'Amount',['class' => 'col-sm-4 control-labels']) !!}
								    	<div class="col-sm-7">
                                       		{!! Form::text('AMOUNT',null,['class'=>'form-control drop-box','tabindex'=> '8','onkeypress'=>'return validateFloatKeyPress(this,event)', 'placeholder'=>'Enter Your Amount']) !!}
										</div>
									</div>

									{!! Form::hidden('ATTACH',null) !!}  {!! Form::hidden('EmpeID',null) !!}

									<div class="form-group">
									    	{!! Form::label('attach', 'Attachment',['class' => 'col-sm-4 control-labels']) !!}
									    	<div class="col-sm-7">
                                           		{!! Form::file('file',null,['class'=>'form-control drop-box','tabindex'=> '9']) !!}
											</div>
									</div>
                                        	
									<div class="form-group">
										<div class="col-md-6 col-md-offset-3"></br>
										    {!! Form::submit('Submit', ['class' => 'btn btn-primary','tabindex'=> '10']) !!}
										</div>
									</div>
								{!! Form::close() !!}
							</div>
						</td>
					</tr>
				</table>
			</div>
			<div class="pull-left"><strong>User: </strong><span  id="user_name">{{ Auth::user()->FNAME }} {{ Auth::user()->LNAME }}</span></div>
			<div class="pull-right"><strong>Date: </strong><span  id="currentdate">{{date('m/d/Y')}}</span></div>
		</div>
	</div>
</div>
@include('includes.footer')


