@include('includes.header')
<br>
<div class="row">
	<h4 class='export-import'>{{trans('tables.timesheet')}}</h4><br>
	<div class="btn-dsqure">
   		<a href="{{ URL::to('runExportProcessD2S') }}"><button class="btn btn-primary btn-script">{{trans('menus.ds')}}</button></a>
   		<a href="{{ URL::to('runExportProcessD2E') }}"><button class="btn btn-primary btn-script">{{trans('menus.de')}}</button></a>
		<div class='msg-labors'>
			@if (session('lastrun'))
				<p class="last-run"><b>{{trans('tables.lastrun')}}:</b> {{ session('lastrun') }}</p>
			@endif
			@if (session('export'))
				<div class="alert alert-success">
					{{ session('export') }}
				</div>
			@endif	
			@if (session('errexport'))
				<div class="alert alert-danger">
					{{ session('errexport') }}
				</div>
			@endif
			@if (session('exportD2E'))
				<div class="alert alert-success">
					{{ session('exportD2E') }}
				</div>
			@endif	
		</div>
   	</div>
</div>
<hr class='hr-line'  />
<!--<div class="row">
<h4 class='export-import'>Email Check Stubs</h4><br>
<div class="btn-dsqure">
   <a href="{{ URL::to('ExportpdfD2S') }}"><button class="btn btn-primary btn-script">DSquare</button></a>
   <a href="{{ URL::to('ExportpdfD2E') }}"><button class="btn btn-primary btn-script">D2Electric</button></a>
   <a href="{{ URL::to('AdminpdfD2S') }}"><button class="btn btn-primary btn-script">Linda</button></a>
  
   <div class='msg-labors'>
   @if (session('exportpdf'))
			<div class="alert alert-success">
				{{ session('exportpdf') }}
			</div>
		@endif
		 </div> </div>
</div>
<hr class='hr-line'  />

<div class="row">
<h4 class='export-import'>Email Check Stubs For Admin</h4><br>
<div class="btn-dsqure">
    <a href="{{ URL::to('AdminpdfD2S') }}"><button class="btn btn-primary btn-script">DSquare</button></a>
    <a href="{{ URL::to('AdminpdfD2E') }}"><button class="btn btn-primary btn-script">D2Electric</button></a>
  
   <div class='msg-labors'>
   @if (session('adminpdf'))
			<div class="alert alert-success">
				{{ session('adminpdf') }}
			</div>
		@endif
		 </div> </div>
</div>

<hr class='hr-line'  /> -->
<div class="row">
	<h4 class='export-import'>{{trans('tables.reims')}}</h4><br>
	<div class="btn-dsqure">
   		<a href="{{ URL::to('runReimsProcess') }}"><button class="btn btn-primary btn-script">{{trans('menus.ds')}}</button></a>
   		<a href="{{ URL::to('runReimeProcess') }}"><button class="btn btn-primary btn-script">{{trans('menus.de')}}</button></a>
		<div class='msg-labors'>
			@if (session('lastrunReims'))
				<p class="last-run"><b>{{trans('tables.lastrun')}}:</b> {{ session('lastrunReims') }}</p>
			@endif
			@if (session('exportReims'))
				<div class="alert alert-success">
					{{ session('exportReims') }}
				</div>
			@endif	
			@if (session('errexportReims'))
				<div class="alert alert-danger">
					{{ session('errexportReims') }}
				</div>
			@endif
			@if (session('exportReime'))
				<div class="alert alert-success">
					{{ session('exportReime') }}
				</div>
			@endif	
		</div>
   </div>
</div>
<hr class='hr-line'/>
<div id="msg">
    <h4 class='export-import'>{{trans('tables.import')}}</h4><br>
	<a href="{{ URL::to('runImportProcess') }}"><button class="btn btn-primary btn-script">{{trans('forms.runscript')}}</button></a>
	@if (session('params'))
		<p class="last-run"><b>{{trans('tables.lastrun')}}:</b> {{ session('params') }}</p>
    @endif
	@if (session('import'))
		<div class="alert alert-success">
			{{ session('import') }}
		</div>
	@endif
	@if (session('errimport'))
		<div class="alert alert-danger">
			{{ session('errimport') }}
		</div>
	@endif	
</div>
<hr class='hr-line'/>
<div id="msg">
    <h4 class='export-import'>{{trans('tables.bckmysql')}}</h4><br>
	<a href="{{ URL::to('runMysqlBackup') }}"><button class="btn btn-primary btn-script">{{trans('forms.runscript')}}</button></a>
	@if (session('param'))
		<p class="last-run"><b>{{trans('tables.lastrun')}}:</b> {{ session('param') }}</p>
    @endif
	@if (session('mysql'))
		<div class="alert alert-success">
			{{ session('mysql') }}
		</div>
	@endif
	@if (session('errmysql'))
		<div class="alert alert-danger">
			{{ session('errmysql') }}
		</div>
	@endif
</div>
<hr class='hr-line'  />
@include('includes.footer') 