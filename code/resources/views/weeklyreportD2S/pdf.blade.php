<!DOCTYPE html>
<html>
<head>
	<style>
		table, td, th { border: 1px solid black; font-family:caliberi;}
        table {border-collapse: collapse;width: 100%;}
        th, td {text-align: center;}
		tr, td div{text-align: right;}
		.hourstotal{color:#0003ff;}
		.outerborder{border:1px solid;margin-bottom: 30px;}
		.centerborder{background-color:#008000;color:#fff;}
		.innerborder{background-color:#000;color:#fff;}
		.week{text-align:center;}
		.timeheader{background-color:#ccc;}
	</style>
</head><?php $i=1;?>
<body>@foreach($ename as $enameval)
	<div class="outerborder">
    <table>
    		<tr>
				<th colspan="7"><p class="week">WEEKLY EMPLOYEE REPORT #<?php echo $i++;?></p></th>
			</tr>
   			<tr class="centerborder">
				<th colspan="7">Employee: {{$enameval->EmpsID}} Between: {{$startdate}} To {{$enddate}}</th>
			</tr>
			<tr class="innerborder">
				<th colspan="7"><?php $total=0;?> @foreach($timesheet as $timeval) @if($enameval->EmpsID == $timeval->EmpsID) <?php $total ++;?>  @endif @endforeach <?php echo $total;?> Labor Entries Found</th>
			</tr>
  			<tr class="timeheader">
			    <th>Job</th>
			    <th>CostCode</th>
			    <th>Local</th>
				 <th>Class</th>
			    <th>Type</th>
			    <th>Date</th>
				<th>Hours</th>
  			</tr><?php $hours_total=0;?>
       	@foreach($timesheet as $timeval)
        @if($enameval->EmpsID == $timeval->EmpsID)
  			<tr>
    			<td>{{$timeval->Job}}</td>
    			<td>{{$timeval->CostCode}}</td>
    		    <td>{{$timeval->Local}}</td>
			    <td>{{$timeval->Class}}</td>
    		    <td>REG</td>
    		    <td><?php echo date("l", strtotime($timeval->Date));?></td>
			    <td><div>{{$timeval->Hours}}</div></td>
			    <?php $hours_total += $timeval->Hours;?>
 		    </tr>
         @endif
            @endforeach

            <tr>
    		    <td colspan="5"></td>
    		    <td><b>Total</b></td>
    		    <td><div class="hourstotal"><b>{{sprintf('%0.2f', $hours_total)}}</b></div></td>
 			</tr>
 			<tr class="innerborder">
				<th colspan="7"><?php $total=0;?> @foreach($reims as $reimsval) @if($enameval->EmpsID == $reimsval->EmpsID) <?php $total ++;?>  @endif @endforeach <?php echo $total;?> Reimbursement Entries Found</th>
			</tr>
  			<tr class="timeheader">
    			<th>Job</th>
    		    <th>CostCode</th>
    			<th>Loc</th>
	 			<th>Cat</th>
    			<th>Type</th>
    			<th>Date</th>
				<th>Amount</th>
  			</tr>
            <?php $amt_total=0;?>
  			@foreach($reims as $reimsval)
        @if($enameval->EmpsID == $reimsval->EmpsID)
  			<tr>
    			<td>{{$reimsval->Job}}</td>
    			<td>{{$reimsval->CostCode}}</td>
    		    <td>{{$reimsval->Loc}}</td>
			    <td>{{$reimsval->Cat}}</td>
    		    <td>REG</td>
    		    <td><?php echo date("l", strtotime($reimsval->Date));?></td>
			    <td><div>{{$reimsval->Amount}}</div></td>
			    <?php $amt_total += $reimsval->Amount;?>
 		    </tr>
        @endif
            @endforeach
            <tr>
                <td colspan="5"></td>
                <td><b>Total</b></td>
                <td><div class="hourstotal"><b>{{sprintf('%0.2f', $amt_total)}}</b></div></td>
            </tr>
        </table>
    </div>@endforeach
    <br>
</body>
</html>