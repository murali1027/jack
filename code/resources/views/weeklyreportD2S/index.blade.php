@include('includes.header')
<h3 class="msg">Weekly Employee Report for <span class="cname">{{ trans('menus.ds') }}</span></h3>
@if (session('message'))
    <div class="alert alert-danger">
	{{ session('message') }}
	</div>
 @endif
<table class="table table-edit table-week" align="center">
	<tr>
		<form method="POST" id="weekReport" action="{{url('ReportD2S')}}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<th class="editempid" colspan="12">
				<table class="table table-bordered table-edit" id="users-table">
       				<thead>
           				<tr class="headercolor">
               				<th>EmpsID</th>
               				<th>Ename</th>
               				<th>Email</th>
               				<th><input type="checkbox" name="select_all" id="example-select-all"></th>
            			</tr>
        			</thead>
    			</table><br>
				<div class="form-group">
					<div class="col-sm-6">
						<input type="text" name="startdate" id="startdate" class="form-control" placeholder="Choose Starting Date"
						value="{{ old('startdate') }}"><br>
					</div>

					<div class="col-sm-6">
						<input type="text" name="enddate" id="enddate" class="form-control" placeholder="Choose Ending Date" 
						value="{{ old('enddate') }}"><br>
					</div>
				</div>
				<input type="hidden" id="selectedval" name="selectedval">
				@if ($errors->has('startdate'))<p class="red">{!!$errors->first('startdate')!!}</p>@endif
				@if ($errors->has('enddate'))<p class="red">{!!$errors->first('enddate')!!}</p>@endif

				<div class="form-group">
					<div class="col-sm-12">
						<input type="submit" id="week-submit" class="btn btn-primary" value="Generate Report"><br><br>
					</div>
				</div> 
			</th>
		</form>
	</tr>
</table>
<script>
$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
       ajax: '{!! route('weeklyreportD2S.data') !!}',
        columns: [
            { data: 'EmpsID', name: 'EmpsID'},
            { data: 'ENAME', name: 'ENAME' },
            { data: 'EMAIL', name: 'EMAIL' },
            { data: 'check', name: 'check', targets: 'no-sort', orderable: false, },
        ],
       "order": [[ 1, "asc" ]]
    });
});
$('#example-select-all').on('click', function(){
      $('input[type="checkbox"]').prop('checked', this.checked);
 });

document.getElementById("week-submit").addEventListener("click", Report);

function Report(){
var allInputs = document.getElementsByName("selected_users[]");
    var empsid = "";
	for (var i=0, n=allInputs.length;i<n;i++) 
	{
    	if (allInputs[i].checked) 
			{
				empsid += allInputs[i].value;
				if(i<(n-1))
				{
					empsid += ",";				
				}
			}
	}
	$("#selectedval").val(empsid);
}

</script>

@include('includes.footer')
