<?php

return [

   /*Tickest*/

    'ticno'		=>	'Ticket No:',
    'opby'		=>	'Opened by:',
    'dopen'		=>	'Date Opened:',
    'pblm'		=>	'Problem',
    'slution'	=>	'Solution',
    'status'	=>	'Status:',
    'cloby'		=>	'Closed by:',
    'dclos'		=>	'Date Closed:',	
    'cat' 		=>	'Catagory',
    'desc'		=>	'Description',
    'no'		=>	'No',
    'attach'	=>	'Attachment',
    'create'	=>	'Created',
    'updated'	=>	'Updated',
    'updteby'	=>	'Updated_By',
    'lastrun'	=>	'Last Run',

    /*Header Name*/
    'adticket'	=>	'Add Ticket',
    'upslution'	=>	'Update Ticket Solution',
    'timesheet'	=>	'Export Labors Timesheet',
    'reims'		=>	'Export Reimbursements',
    'import'	=>	'Import Tables',
    'bckmysql'	=>	'Backup MySQL',
    'reimburse'	=>	'Reimbursements for',
];
