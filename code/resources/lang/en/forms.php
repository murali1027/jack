<?php

return [

   /*Forms*/

    'uname'		=>	'User Name',
    'empid'		=>	'EmpID',
    'fname'		=>	'First Name',
    'lname'		=>	'Last Name',
    'email'		=>	'E-Mail Address',
    'domain'    =>	'Domain',
    'role'		=>	'Role',
	'pwd'		=>	'Password',
	'cpwd'		=>	'Confirm Password',
	'rme'		=>	'Remember Me',
	'fpwd'      =>	'Forgot Your Password?',
	'rpwd'		=>	'Reset Password',
	'slink'		=>	'Send Password Reset Link',
	'sdomain'	=>	'Select a Domain',
    'srole'		=>	'Select a Role',
    'action'	=>	'Actions',
    'upuser'	=>	'Update User',

    /*Reims*/
	'date'		=>	'Date',
    'emp'		=>	'Employee',
    'job'		=>	'Job',
    'ccode'		=>	'Cost Code',
    'cat'		=>	'Cat',
    'loc'		=>	'Location',
	'desc'		=>	'Description',
    'amt'		=>	'Amount',
    'attach'	=>	'Attachment',
    'user'		=>	'User',
    'location'	=>	'Loc',
    'category'	=>	'Cat',
    'total'		=>	'Total',
    
    /*Buttons*/
    'login'		=>	'Login',
    'register'	=>	'Register',
    'slink'		=>	'Send Password Reset Link',
    'rpwd'		=>	'Reset Password',
    'Adusr'		=>	'Add User',
    'edit'		=>	'Edit',
	'del'		=>	'Delete',
    'update'	=>	'Update',
    'subtket'	=>	'Submit Ticket',
    'subslu'	=>	'Submit Solution',
    'runscript'	=>	'Run Script',
    'submit'	=>	'Submit'


];
