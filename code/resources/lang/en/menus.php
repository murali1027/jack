<?php

return [

   /*Header Menus name*/

    'title'		=> 'MyVPAZ',
    'ds'		=> 'Company 1',
	'de'		=> 'Company 2',
	'login'		=> 'Login',
	'logout'	=> 'Logout',
	'home'		=> 'Home',
	'labor'		=> 'Labor',
	'enter'		=> 'Enter Time',
	'edit'		=> 'Edit Time',
	'Approve'	=> 'Approve Time',
	'hours'		=> 'Time Entry Status',
	'cstub'		=> 'Check Stub',
	'expense'	=> 'Expenses',
	'ccard'		=> 'Credit Cards',
	'encard'	=> 'Enter Credit Cards',
	'edcard'	=> 'Edit Credit Cards',
	'apcard'	=> 'Approve Credit Cards',
	'invoice'	=> 'Invoices',
	'reims'		=> 'Reimbursements',
	'enreims'	=> 'Enter Reimbursements',
	'edreims'	=> 'Edit Reimbursements',
	'apreims'	=> 'Approve Reimbursements',
	'fapreims'	=> 'Final Approve Reimbursements',
	'help'		=> 'Help',
	'ticket'	=> 'Tickets',
	'adticket'	=> 'Add Ticket',
	'admin'		=> 'Admin',
	'edticket'	=> 'Edit/Update Tickets',
	'user'		=> 'Add/Edit/Delete Users',
	'report'	=> 'Transfers & Backup',
	'weekly'	=> 'Weekly Report',


	/* Home Page Header */
    'cname'		=> 'MyVP Construction Software',

    /* Roles */
    'adm'	=>	'ADM',
    'app'	=>	'APP',
    'sup'	=>	'SUP',
    'usr'	=>	'USR',
    "fapp"  =>	'FAP',

    /* Domain */
    'd2s'	=>	'S',
    'd2e'	=>	'E',
    'dx'	=>	'X',


];
