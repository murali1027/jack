<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Ulocal extends Model {

	protected $table = 'ulocal';
	
 	protected $fillable = ['JobID','ULOCAL'];

}
