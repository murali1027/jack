<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Paye extends Model {

	protected $table = 'paye';
	
	protected $fillable = ['EmpeID','CHK_DATE','CAT','UNITS', 'AMOUNT'];

}
