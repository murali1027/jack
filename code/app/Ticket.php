<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model {

	protected $table = 'ticket';
 	
 	protected $primaryKey = 'TICKET_ID';	
 	
 	protected $fillable = ['PROBLEM', 'SOLUTION','CAT_ID', 'ATTACH','USER_ID', 'OPENED_BY','CLOSED_BY', 'DATE_OPENED',
 	'DATE_COMPLETED','TSTATUS','TIMESTAMP','CREATED_AT','UPDATED_AT','DELETED'];

}
