<?php namespace App\Classes;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Jobcat;
use App\Reime;
use App\Empe;
use App\Ccod;
use App\Job;
use Session;
use View;
use Mail;
use DB;

class ReimeDelectric{

	public function __construct()
	{
		$this->created_at=date('Y-m-d H:i:s');
		$this->created_by=Auth::User()->USER_ID;
		$this->empsid=Auth::User()->EmpsID;
		$this->role=Auth::User()->ROLE;
	}

    /* View Reimbursement*/
	public function getViewreime()
	{
		if ($this->role == 'USR') 
		{
		    $empe=DB::table('reime as r')
		        ->join('empe as e', 'r.EmpeID', '=', 'e.EmpeID')
				->join('job as j', 'r.JobID', '=', 'j.JobID')
				->where('e.EmpeID', $this->empsid)
				->where('r.DELETED', '=', 'N')
				->where('r.DOWNLOADED', '=', 'N')
				->whereIn('r.APPROVED',['N','R','I'])
				->select('e.ENAME as Ename','r.EmpeID as EmpeID')
				->orderBy('r.RDATE','ASC')
				->groupBy('r.EmpeID')
				->get();
		}
		else
		{
            $empe=DB::table('reime as r')
		        ->join('empe as e', 'r.EmpeID', '=', 'e.EmpeID')
				->join('job as j', 'r.JobID', '=', 'j.JobID')
				->where('r.DELETED', '=', 'N')
				->where('r.DOWNLOADED', '=', 'N')
				->whereIn('r.APPROVED',['N','R','I'])
				->select('e.ENAME as Ename','r.EmpeID as EmpeID')
				->orderBy('r.RDATE','ASC')
				->groupBy('r.EmpeID')
				->get();
		}
		return View::make('ReimbursementsD2E/enter-reimsD2E/index',compact('empe'));
	}
    
    /*Form Reimbursement*/
	public function formReime()
	{
        $job_cat=new Jobcat();
		$jobcat=$job_cat->job_cat();
	    $reime=DB::table('reime as r')
					->join('empe as e', 'r.EmpeID', '=', 'e.EmpeID')
					->join('job as j', 'r.JobID', '=', 'j.JobID')
					->where('r.DELETED', '=', 'N')
					->where('r.EmpeID',$this->empsid)
					->select('r.JobID as Job','r.CostCode as CostCode','r.LOC as Location',
						'r.DSCR as Description', 'r.CAT as Category','r.AMOUNT as Amount','r.RDATE as Date','r.EmpeID','e.ENAME')
					->orderBy('r.RDATE','ASC')
					->get();
		return View::make('ReimbursementsD2E/enter-reimsD2E/create',compact('jobcat','reime'));
	}

	/*Store Reimbursements*/
	public function postReime($request){
		
        $empid=($this->role == 'USR' ? $this->empsid : Input::get('ename'));
		$Rdate = date("mdy", strtotime(Input::get('RDATE')));
		$date = date("Y-m-d", strtotime(Input::get('RDATE')));
		if($request->hasFile('file'))
		{
			$file = $request->file('file');
			$destinationPath = env('UPLOAD_REIME'); // upload path
			$extension = Input::file('file')->getClientOriginalExtension(); // getting file extension
			$fileName= $empid.$Rdate. '.' . $extension; // renameing image
			$upload_success = Input::file('file')->move($destinationPath, $fileName); // uploading file to given path
		}
		else{
			$fileName=NULL;
		}
		$data=array('EmpeID'=>$empid,'JobID'=>Input::get('jobid'),'EXTRA'=>Input::get('extra'),
		            'CostCode'=>Input::get('ccode'),'CAT'=>Input::get('cat'),'LOC'=>Input::get('LOC'),
		            'DSCR'=>Input::get('DSCR'),'RDATE'=>$date,'AMOUNT'=>Input::get('AMOUNT'),'ATTACH'=>$fileName,
		            'CREATED_BY'=>$this->created_by);
		REIME::insert($data);
		session::flash('status', 'Reimbursement added successfully');
		if($this->role == 'USR'){
			return redirect('enter-reimsD2E/create'); 
		}
		else{
			return redirect('enterReime');
		}
		                             
	}

     /*Edit View Reimbursement*/
	public function EditReime($id)
	{
	 	$edit=REIME::find($id);
	    $jobname=$edit->JobID;
	    $CostCode=$edit->CostCode;
	    $extra=$edit->EXTRA;
		$job_cat=new Jobcat();
		$jobcat=$job_cat->job_cat();;
        $ccode=DB::table('jobcat as j')->leftJoin('ccod as c', 'j.CostCode', '=', 'c.CostCode')->where('j.JobID',$jobname)
               ->where('j.EXTRA',$extra)->select('j.CostCode as CostCode','c.DSCR as DSCR' )->groupBy('j.CostCode')->get();
		$extra=JOBCAT::select('EXTRA')->where('JobID','=', $jobname)->where('CostCode','=', $CostCode)->first();
		$ext_val=$extra->EXTRA;
		$cat=JOBCAT::select(DB::raw('CONCAT(CAT, "-", DSCR)  DSCR'), 'CAT' )->where('JobID','=', $jobname )
		             ->where('CostCode','=', $CostCode)->where('EXTRA',$ext_val)->get();
	    $jname=JOB::where('JobID',$jobname)->get();    
		return view('ReimbursementsD2E/enter-reimsD2E.edit',compact('edit','jobcat','ccode','cat','jname'));
	}

	/* Update Reimbursements*/
    public function UpdateReime($id, $request)
    {
        $empid=Input::get('EmpeID');
		$Rdate = date("mdy", strtotime(Input::get('RDATE')));
    	$date = date("Y-m-d", strtotime(Input::get('RDATE')));
		$extra=JOBCAT::select('EXTRA')->where('JobID','=',Input::get('jobid'))
		            ->where('CostCode','=',Input::get('ccode'))->first();
		$extval=trim($extra->EXTRA);

		if($request->hasFile('file'))
		{   
			$file = $request->file('file');
			$destinationPath = env('UPLOAD_REIME'); // upload path
			$extension = Input::file('file')->getClientOriginalExtension(); // getting file extension
			$fileName= $empid.$Rdate. '.' . $extension; // renameing image
			$upload_success = Input::file('file')->move($destinationPath, $fileName); // uploading file to given path
		}
		else
		{
			$fileName=Input::get('ATTACH');
		}

		$Updatereime=(array('JobID'=>Input::get('jobid'), 'EXTRA'=>$extval,'CostCode'=> Input::get('ccode'),
							'CAT'=>Input::get('cat'),'LOC'=>Input::get('LOC'),'DSCR'=>Input::get('DSCR'),
							'RDATE'=>$date,'AMOUNT'=>Input::get('AMOUNT'),'APPROVED'=>'N','ATTACH'=>$fileName,
							'MODIFIED'=>$this->created_at,'MODIFIED_BY'=>$this->created_by));
		$reime=REIME::find($id);
	    $reime->update($Updatereime);
	    return redirect('enter-reimsD2E');
    }
     
    /* Delete Reimbursements*/
    public function DeleteReime($id)
    {
		$Delete_reime=(array('DELETED'=>'Y'));
		$Delreime=REIME::find($id);
	    $Delreime->update($Delreime);
		return redirect('enter-reimsD2E');
	}

	public function enterReime()
	{
		$job_cat=new Jobcat();
		$jobcat=$job_cat->job_cat();
	    $auth = new Empe();
		$empid= $auth->showempe();
	    return View::make('ReimbursementsD2E/enter-reimsD2E/enterReime',compact('jobcat','empid'));
	}

	public function EmpReime()
	{
		$emp=Input::get('empid');
		$reime=DB::table('reime as r')
					->join('empe as e', 'r.EmpeID', '=', 'e.EmpeID')
					->join('job as j', 'r.JobID', '=', 'j.JobID')
					->where('r.DELETED', '=', 'N')
					->where('r.EmpeID',$emp)
					->select('r.JobID as Job','r.CostCode as CostCode','r.LOC as Location','r.DSCR as Description',
					         'r.CAT as Category','r.AMOUNT as Amount','r.RDATE as Date','r.EmpeID','e.ENAME')
					->orderBy('r.RDATE','ASC')
					->get();
	    if(count($reime)>0){
			$returnHTML = view('ReimbursementsD2E/enter-reimsD2E/viewReime',compact('reime'))->render();
			return response()->json(array('success' => true,'edit'=>$returnHTML));
	    }
	    else{
	    	$returnHTML = view('errors/norecordReims',compact('reime'))->render();
		    return response()->json(array('success' => true,'edit'=>$returnHTML));
	    }
	} 

	/* Approve Reims HomePage*/
	public function ApproveReimsPage()
	{
        $emps = DB::table('reime as r')
		        ->join('empe as e', 'r.EmpeID', '=', 'e.EmpeID')
				->join('job as j', 'r.JobID', '=', 'j.JobID')
				->where('r.DELETED', '=', 'N')
				->where('r.DOWNLOADED', '=', 'N')
				->whereIn('r.APPROVED',['N','R','I'])
				->select('e.ENAME as Ename','r.EmpeID as EmpeID',DB::raw('(IF(e.APVTYPE = "MANAGER", e.MANAGER, j.APPROVER)) as Approver'))
				->orderBy('e.ENAME','ASC')
				->groupBy('r.EmpeID')
				->get();
		return View::make('ReimbursementsD2E/approve-reimsD2E/index',compact('emps'));
	} 

    /* Approve all Reims*/
    public function ApproveallReims()
    {
    	$approveall=explode(",",Input::get('approveall'));
		$Approveallreims=(array('APPROVED'=>'I','APPROVED_BY'=>$this->created_by,'MODIFIED'=>$this->created_at,
			                    'MODIFIED_BY'=>$this->created_by));
		DB::table('reime')->whereIn('REIME_ID',$approveall)->where('APPROVED','=','N')->update($Approveallreims);
		return response()->json(array('success' => true,'msg'=>'sucesss'));
    }

   /* Approve Reims*/
    public function ApproveReims()
    {
        $reimsid=Input::get('reimsid');
		$Approvereims=(array('APPROVED'=>'I','APPROVED_BY'=>$this->created_by,'MODIFIED'=>$this->created_at,'MODIFIED_BY'=>$this->created_by));
		DB::table('reime')->where('REIME_ID',$reimsid)->where('APPROVED','=','N')->update($Approvereims);
		return response()->json(array('success' => true,'msg'=>'sucesss'));
    } 

    /* Reject Reims*/
    public function RejectReims()
    {
        $reimsid=Input::get('reimsid');
		$reims=REIME::where('REIME_ID',$reimsid)->first();
		$empsid=$reims->EmpeID;
		$dates=$reims->RDATE;
		$user=EMPE::where('EmpeID',$empsid)->first();
		$email=trim($user->EMAIL);
		$fname=trim($user->ENAME);
		$userid= Auth::User()->USER_ID;
		$modify_date = date("Y-m-d H:i:s");
		$Rejectreims=(array('APPROVED'=>'R','MODIFIED'=>$modify_date,'MODIFIED_BY'=>$userid));
		DB::table('reime')->where('REIME_ID',$reimsid)->update($Rejectreims);
		/*$data = array(
			'fname'=>$fname,
			'dates'=>$dates
		);
			Mail::send('ReimbursementsD2S/approve-reimsD2S.mailers', $data, function($message) use ($email)
			{
				$message->to([$email])->subject("Rejected Reimbursement");
			});*/
		return response()->json(array('success' => true,'msg'=>'sucesss'));
    }


}

?>