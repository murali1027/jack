<?php namespace App\Classes;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Labors;
use App\Labore;
use Validator;
use App\Certs;
use App\Emps;
use App\Ccod;
use App\Job;
use stdClass;
use View;
use Mail;
use DB;

class Dsquare{

	public function __construct()
	{
		$this->emps_id=Auth::User()->EmpsID;
		$this->role=Auth::User()->ROLE;
		$this->created_at=date('Y-m-d H:i:s');
		$this->created_by=Auth::User()->USER_ID;
	}

    /* Labors Form Timesheet*/
	public function labors()
	{
		$jobs = new Job();
		$job= $jobs->jobs();
		if ($this->role == 'USR') 
		{
			$empid=DB::table('emps')->leftJoin('job', 'emps.JOB_DEFAULT', '=', 'job.JobID')
			        ->where('emps.EmpsID',$this->emps_id)->get();
			foreach($empid as $value){
				$jobdefault = $value->JOB_DEFAULT;
			}
			$extra=CCOD::where('JobID', '=', $jobdefault)->where('LBREST', '>', 0)->groupBy('Extra')->get();
			$ccode=CCOD::where('JobID', '=', $jobdefault)->where('LBREST', '>', 0)->get();
			$certs=CERTS::where('JobID', '=', $jobdefault)->get();
		}
	    else 
		{
		    $auth = new Emps();
		    $empid= $auth->empshow();
		}
		return View::make('laborD2S/view_labord2s',compact('job','ccode','empid','certs','extra'));
	}

	/*Get Selected Employee Timesheet*/
	public function getEmployee()
	{
        $emp_id=Input::get('ename');
        $jobs = new Job();
		$job= $jobs->jobs();
		$empid=DB::table('emps')->leftJoin('job', 'emps.JOB_DEFAULT', '=', 'job.JobID')
				->leftJoin('users', 'emps.EmpsID', '=', 'users.EmpsID')->where('emps.EmpsID',$emp_id)->get();
		foreach($empid as $value){
			$jobdefault=$value->JOB_DEFAULT;
		}
		$extra=CCOD::where('JobID', '=', $jobdefault)->where('LBREST', '>', 0)->groupBy('Extra')->get();
		$ccode=CCOD::where('JobID', '=', $jobdefault)->where('LBREST', '>', 0)->get();
	    $certs=CERTS::where('JobID', '=', $jobdefault)->get();
		$returnHTML=view('/laborD2S/view_employee',compact('certs','job','ccode','empid','extra'))->render();
		return response()->json(array('success' => true,'html'=>$returnHTML));
	}
    
    /*Get All Hours*/
	public function getHours(){

	    if ($this->role == 'USR') 
		{
			$emp_id = $this->emps_id;
		}
		else
		{ 
	        $empseid=Input::get('ename');
	        $emp_id=(count($empseid)>0 ? Input::get('ename') : Input::get('ename'));
		}

		$sunday=Input::get("sunday");
		$monday=Input::get("monday");
		$tuesday=Input::get("tuesday");
		$wednesday=Input::get("wedday");
		$thursday=Input::get("thuday");
		$friday=Input::get("friday");
		$saturday=Input::get("satday");

		$empsid=EMPS::where('EmpsID',$emp_id)->first();

		if(count($empsid)>0)
		{
			$sunday_date=LABORS::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $sunday)
			            ->where('EmpsID', $emp_id)->where('DELETED','=','N')->first();
			$monday_date=LABORS::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $monday)
			            ->where('EmpsID', $emp_id)->where('DELETED','=','N')->first();
			$tuesday_date=LABORS::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $tuesday)
			            ->where('EmpsID', $emp_id)->where('DELETED','=','N')->first();
			$wedday_date=LABORS::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $wednesday)
			            ->where('EmpsID', $emp_id)->where('DELETED','=','N')->first();
			$thuday_date=LABORS::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $thursday)
			            ->where('EmpsID', $emp_id)->where('DELETED','=','N')->first();
			$friday_date=LABORS::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $friday)
			            ->where('EmpsID', $emp_id)->where('DELETED','=','N')->first();
			$satday_date=LABORS::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $saturday)
			            ->where('EmpsID', $emp_id)->where('DELETED','=','N')->first();
		}
		else
		{
			$sunday_date=LABORE::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $sunday)
			            ->where('EmpeID', $emp_id)->where('DELETED','=','N')->first();
			$monday_date=LABORE::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $monday)
			            ->where('EmpeID', $emp_id)->where('DELETED','=','N')->first();
			$tuesday_date=LABORE::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $tuesday)
			            ->where('EmpeID', $emp_id)->where('DELETED','=','N')->first();
			$wedday_date=LABORE::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $wednesday)
			            ->where('EmpeID', $emp_id)->where('DELETED','=','N')->first();
			$thuday_date=LABORE::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $thursday)
			            ->where('EmpeID', $emp_id)->where('DELETED','=','N')->first();
			$friday_date=LABORE::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $friday)
			            ->where('EmpeID', $emp_id)->where('DELETED','=','N')->first();
			$satday_date=LABORE::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $saturday)
			            ->where('EmpeID', $emp_id)->where('DELETED','=','N')->first();
		}
		$sunday_hours=$sunday_date->HOURS;
		$sunday_hour=(empty($sunday_hours) ? NULL: $sunday_hours);
		$monday_hours=$monday_date->HOURS;
		$monday_hour=(empty($monday_hours) ? NULL: $monday_hours);
		$tuesday_hours=$tuesday_date->HOURS;
		$tuesday_hour=(empty($tuesday_hours) ? NULL: $tuesday_hours);
		$wedday_hours=$wedday_date->HOURS;
		$wedday_hour=(empty($wedday_hours) ? NULL: $wedday_hours);
		$thuday_hours=$thuday_date->HOURS;
		$thuday_hour=(empty($thuday_hours) ? NULL: $thuday_hours);
		$friday_hours=$friday_date->HOURS;
		$friday_hour=(empty($friday_hours) ? NULL: $friday_hours);
		$satday_hours=$satday_date->HOURS;
		$satday_hour=(empty($satday_hours) ? NULL: $satday_hours);
		$total=$sunday_hour+$monday_hour+$tuesday_hour+$wedday_hour+$thuday_hour+$friday_hour+$satday_hour;
		return response()->json(array('success' => true,'sun'=>$sunday_hour, 'mon'=>$monday_hour,
		                              'tue'=>$tuesday_hour,'wed'=>$wedday_hour,'thu'=>$thuday_hour,
		                              'fri'=>$friday_hour, 'sat'=>$satday_hour,'total'=>$total));                              
	}

    /* Post Labors Timesheet*/
	public function postlabors()
    {
    	$valid=Validator::make(Input::all(),array('jobid'=>'required','ccode'=>'required'));
		if($valid->fails())
		{
			Input::flash();
			return Redirect::to('ViewLaborD2S')->withErrors($valid);
		}
		else
		{
	        $emp_id=($this->role == 'USR' ? $this->emps_id : Input::get('ename'));
			$jobid=Input::get('jobid');
			$date=Input::get('date');
			$sundate=Input::get('sundate');
			$mondate=Input::get('mondate');
			$tuedate=Input::get('tuedate');
			$weddate=Input::get('weddate');
			$thudate=Input::get('thudate');
			$fridate=Input::get('fridate');
			$satdate=Input::get('satdate');
			$ccode=Input::get('ccode');
			$extra=Input::get('extra');
			$certs=Input::get('certs');
			$shift=Input::get('shift');
			$sunhours=Input::get('sunhours');
			$monhours=Input::get('hours');
			$tuehours=Input::get('tuehours');
			$wedhours=Input::get('wedhours');
			$thuhours=Input::get('thuhours');
			$frihours=Input::get('frihours');
			$sathours=Input::get('sathours');
			if(!empty($sunhours))
			{
				$labors=LABORS::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $sundate)
				        ->where('EmpsID', $emp_id)->where('DELETED','=','N')->get();
				foreach($labors as $value)	
				{
					$getHours=$value->HOURS + $sunhours;
				}
				if($getHours <= 16){}
				else
				{
				  	return response()->json(array('success' => true, 'msg'=>"<span class='alert alert-danger'>
				  		You have enter more than 16 Hours Sunday date</span>"));
				}
			}
			if(!empty($monhours))
			{
				$labors=LABORS::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $mondate)
						->where('EmpsID', $emp_id)->where('DELETED','=','N')->get();
				foreach($labors as $value)	
				{
					$getHours=$value->HOURS + $monhours;
				}
				if($getHours <= 16){}
				else
				{
				  return response()->json(array('success' => true, 'msg'=>"<span class='alert alert-danger'>
				  	You have enter more than 16 Hours Monday date</span>"));
				}
			}
			if(!empty($tuehours))
			{
			    $labors=LABORS::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $tuedate)
			    		->where('EmpsID', $emp_id)->where('DELETED','=','N')->get();
				foreach($labors as $value)	
				{
					$getHours=$value->HOURS + $tuehours;
				}
				if($getHours <= 16){}
				else
				{
				   return response()->json(array('success' => true, 'msg'=>"<span class='alert alert-danger'>
				   	You have enter more than 16 Hours Tuesday date</span>"));
				}
			}
			if(!empty($wedhours))
			{
			   $labors=LABORS::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $weddate)
			   			->where('EmpsID', $emp_id)->where('DELETED','=','N')->get();
				foreach($labors as $value)	
				{
					$getHours=$value->HOURS + $wedhours;
				}
				if($getHours <= 16){}
				else
				{
				   return response()->json(array('success' => true, 'msg'=>"<span class='alert alert-danger'>
				   	You have enter more than 16 Hours Wednesday date</span>"));
				}
			}
			if(!empty($thuhours))
			{
				$labors=LABORS::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $thudate)
						->where('EmpsID', $emp_id)->where('DELETED','=','N')->get();
				foreach($labors as $value)	
				{
					$getHours=$value->HOURS + $thuhours;
				}
				if($getHours <= 16){}
				else
				{
				   return response()->json(array('success' => true, 'msg'=>"<span class='alert alert-danger'>
				   	You have enter more than 16 Hours Thursday date</span>"));
				}
			}
			if(!empty($frihours))
			{
				$labors=LABORS::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $fridate)
						->where('EmpsID', $emp_id)->where('DELETED','=','N')->get();
				foreach($labors as $value)	
				{
					$getHours=$value->HOURS + $frihours;
				}
				if($getHours <= 16){}
				else
				{
				  return response()->json(array('success' => true, 'msg'=>"<span class='alert alert-danger'>
				  	You have enter more than 16 Hours Friday date</span>"));
				}
			}
			if(!empty($sathours))
			{
				$labors=LABORS::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $satdate)
						->where('EmpsID', $emp_id)->where('DELETED','=','N')->get();
				foreach($labors as $value)	
				{
					$getHours=$value->HOURS + $sathours;
				}
				if($getHours <= 16){}
				else
				{
				   return response()->json(array('success' => true, 'msg'=>"<span class='alert alert-danger'>
				   	You have enter more than 16 Hours Satday date</span>"));
				}
			}
			if(!empty($sunhours))
			{
			    $labors_data=array('JobID'=>$jobid,'CostCode'=>Input::get('ccode'),'EmpsID'=>$emp_id,'EXTRA'=>$extra,
			    	               'CERTS'=>$certs,'DATE_WORK'=>$sundate,'SHIFT'=>$shift,'HOURS'=>$sunhours,
			    	               'CREATED'=>$this->created_at,'CREATED_BY'=>$this->created_by);
				LABORS::insert($labors_data);
			}
			if(!empty($monhours))
			{
				$labors_data=array('JobID'=>$jobid,'CostCode' =>Input::get('ccode'),'EmpsID'=>$emp_id,'EXTRA'=>$extra,
								   'CERTS'=>$certs,'DATE_WORK'=>$mondate,'SHIFT'=> $shift,'HOURS'=> $monhours,
								   'CREATED'=>$this->created_at,'CREATED_BY'=>$this->created_by);	
				LABORS::insert($labors_data);
			}
			if(!empty($tuehours))
			{
			   $labors_data=array('JobID'=>$jobid,'CostCode'=>Input::get('ccode'),'EmpsID'=>$emp_id,'EXTRA'=>$extra,
			   					  'CERTS'=>$certs,'DATE_WORK'=>$tuedate,'SHIFT'=>$shift,'HOURS'=>$tuehours,
			   					  'CREATED'=>$this->created_at,'CREATED_BY'=>$this->created_by);	
				LABORS::insert($labors_data);
			}
			if(!empty($wedhours))
			{
			    $labors_data=array('JobID'=>$jobid,'CostCode'=>Input::get('ccode'),'EmpsID'=>$emp_id,'EXTRA'=>$extra,
								   'CERTS'=>$certs,'DATE_WORK'=>$weddate,'SHIFT'=>$shift,'HOURS'=>$wedhours,
								   'CREATED'=>$this->created_at,'CREATED_BY'=>$this->created_by);	
				LABORS::insert($labors_data);
			}
			if(!empty($thuhours))
			{
			    $labors_data=array('JobID'=>$jobid,'CostCode'=>Input::get('ccode'),'EmpsID'=> $emp_id,'EXTRA'=>$extra,
				                   'CERTS'=>$certs,'DATE_WORK'=>$thudate,'SHIFT'=>$shift,'HOURS'=>$thuhours,
				                   'CREATED'=>$this->created_at,'CREATED_BY'=>$this->created_by);
				LABORS::insert($labors_data);
			}
			if(!empty($frihours))
			{
				$labors_data=array('JobID'=>$jobid,'CostCode'=>Input::get('ccode'),'EmpsID'=>$emp_id,'EXTRA'=> $extra,
				                   'CERTS'=>$certs,'DATE_WORK'=>$fridate,'SHIFT'=>$shift,'HOURS'=>$frihours,
				                   'CREATED'=>$this->created_at,'CREATED_BY'=>$this->created_by);
				LABORS::insert($labors_data);
			}
			if(!empty($sathours))
			{
				$labors_data=array('JobID'=>$jobid,'CostCode'=>Input::get('ccode'),'EmpsID'=>$emp_id,'EXTRA'=>$extra,
								   'CERTS'=>$certs,'DATE_WORK'=>$satdate,'SHIFT'=>$shift,'HOURS'=> $sathours,
								   'CREATED'=>$this->created_at,'CREATED_BY'=>$this->created_by);
				LABORS::insert($labors_data);
			}
			return response()->json(array('success' => true, 'msg'=>"<span class='alert alert-success'>
				Labor Timesheet Added Successfully</span>"));
		}
    }

    /*Edit Timesheet Page*/
    public function EditView()
    {
        $auth = new Emps();
		$empid= $auth->empshow();
		return view('laborD2S/editlaborsD2S.index',compact('empid'));
    }

   /*View Selected Edit Timesheet*/
    public function EditViewTimesheet()
    {
        $empid=($this->role == 'USR' ? $this->emps_id : Input::get('empid'));
        $start_date = date("Y-m-d", strtotime(Input::get('weeks_start')));
		$end_date = date("Y-m-d", strtotime(Input::get('weeks_end')));
	    $labors=DB::table('labors as l')
		        ->join('emps as e', 'l.EmpsID', '=', 'e.EmpsID')
				->join('job as j', 'l.JobID', '=', 'j.JobID')
				->where('e.EmpsID', '=', $empid)
				->whereBetween('l.DATE_WORK', array($start_date, $end_date))
				->where('l.DELETED', '=', 'N')
				->select(DB::raw('CONCAT(l.JobID, "-", j.JNAME)  Job'),'l.CostCode as CostCode','l.CERTS as Cert',
					    'l.DATE_WORK as Date','l.SHIFT as Shift', 'l.HOURS as Hours','l.APPROVED as Approved',
					    'l.LABORS_ID as Labors_ID',
				        DB::raw('(IF(e.APVTYPE = "MANAGER", e.MANAGER, j.APPROVER)) as Approver'))
				->orderBy('l.DATE_WORK','ASC')
				->get();
		if(count($labors)>0)
		{			
		$data=array('labors'=>$labors);
		$returnHTML = view('laborD2S/editlaborsD2S/editViewtimesheet')->with($data)->render();
		return response()->json(array('success' => true,'edit'=>$returnHTML));
		}
		else
		{
			$data=array('labors'=>$labors);
		    $returnHTML = view('laborD2S/editlaborsD2S/editViewtimesheet')->with($data)->render();
		    return response()->json(array('success' => true,'edit'=>$returnHTML,'norec'=>'Edit Timesheet Records Not Found'));
		}
    }
    
    /*Edit View Timesheet*/
    public function editTimesheet($id)
    {
    	$edit=LABORS::find($id);
	    $jobname=$edit->JobID;
	    $extra=$edit->EXTRA;
		$jobs = new Job();
		$job= $jobs->jobs();
		$ccod=CCOD::where('JobID', '=', $jobname)->where('EXTRA',$extra)->where('LBREST', '>', 0)->get();
		$certs=CERTS::where('JobID', '=', $jobname)->get();
		$jname=JOB::where('JobID',$jobname)->get();
		return view('laborD2S/editlaborsD2S.edit',compact('edit','job','jname','ccod','certs'));
    }
    
    /*Update Timesheet*/
    public function updateTimesheet($id)
    {
    	$hours=Input::get('hours');
		$date_work=Input::get('date');
		$Updatetimesheet=(array('JobID'=>Input::get('jobid'),'CostCode'=>Input::get('ccode'),'CERTS'=>Input::get('certs'),
			                    'EXTRA'=>Input::get('extra'),'SHIFT'=>Input::get('shift'),'DATE_WORK'=>$date_work,'HOURS'=>$hours,
			                    'APPROVED'=>'N','MODIFIED'=>$this->created_at,'MODIFIED_BY'=>$this->created_by));
		$empsid=Input::get('empsid');
		$labors_hours_edit=LABORS::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $date_work)
		                   ->where('EmpsID', $empsid)->where('DELETED','=','N')->where('LABORS_ID',$id)->get();
		$labors_hours=LABORS::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $date_work)
		              ->where('EmpsID', $empsid)->where('DELETED','=','N')->get();
		foreach($labors_hours_edit as $value)	
		{
			$getHours=$value->HOURS;
		}
		foreach($labors_hours as $value)	
		{
			$getHour=$value->HOURS-$getHours+$hours;
		}
		if($getHour <= 16)
		{
			DB::table('labors')->where('LABORS_ID','=',$id)->update($Updatetimesheet);
	        return redirect('editlaborsD2S');
		}
		else
		{
			return redirect()->back()->with('message', 'You have Enter More than 16 hours');
		}  
    }

    /*Delete Timesheet*/
    public function deleteTimesheet()
    {
    	$laborsid=Input::get('laborsid');
		$Deletetimesheet=(array('DELETED'=>'Y'));
		$delete=DB::table('labors')->where('LABORS_ID',$laborsid)->update($Deletetimesheet);
		return response()->json(array('success' => true,'msg'=>'sucesss'));
	}

    /* Approve Timesheet HomePage*/
	public function ApproveTimesheetPage()
	{
       $approve = new Emps();
	   $empid= $approve->empshow();
	   return view('laborD2S/approveD2S.index',compact('empid'));
    }

    /* Grouping Timesheeet */
	public function GroupTimesheet()
    {
        $start_date = date("Y-m-d", strtotime(Input::get('weeks_start')));
		$end_date = date("Y-m-d", strtotime(Input::get('weeks_end')));
	    $labors=DB::table('labors as l')
		        ->join('emps as e', 'l.EmpsID', '=', 'e.EmpsID')
				->join('job as j', 'l.JobID', '=', 'j.JobID')
				->where('l.DELETED', '=', 'N')
				->whereBetween('l.DATE_WORK', array($start_date, $end_date))
				->select(DB::raw('CONCAT(l.JobID, "-", j.JNAME)  Job'),'l.CostCode as CostCode','l.EmpsID as EmpsID',
					    'l.CERTS as Cert','l.DATE_WORK as Date','l.SHIFT as Shift', 'l.HOURS as Hours','l.APPROVED as Approved',
					    'l.LABORS_ID as Labors_ID','l.JobID as JobID',
				        DB::raw('(IF(e.APVTYPE = "MANAGER", e.MANAGER, j.APPROVER)) as Approver'))
				->orderBy('l.DATE_WORK','ASC')
				->get();
		foreach($labors as $value){
			$empsids[]=$value->EmpsID;
			$Approver[]=trim($value->Approver);
			$job_id[]=$value->JobID;
		}
		$empsid=(count($labors)>0 ? $empsids: NULL);
		$jobid=(count($labors)>0 ? $job_id: NULL);
	 	$mailid  = Auth::user()->EMAIL;  
		$approves = strstr($mailid, '@', true);  
		$job=JOB::whereIn('JobID',$jobid)->where('APPROVER',$approves)->get();
		foreach ($job as $value)
		{
			$jobs_ID[]=$value->JobID;
		}
		$jobsID=(count($job)>0 ? $jobs_ID: NULL);
		$labors_job=LABORS::whereIn('JobID',$jobsID)->whereBetween('DATE_WORK', array($start_date, $end_date))->get();
		foreach ($labors_job as $value)
		{
			$empss_id[]=$value->EmpsID;
		}
		$emps_id=(count($labors_job)>0 ? $empss_id: NULL);
		$labors_jobid=EMPS::where(function($q) { $q->where('APVTYPE', '!=','MANAGER' )->orWhereNull('APVTYPE');})
                     ->whereIn('EmpsID',$emps_id)->get();
		foreach ($labors_jobid as $value)
		{
			$labors_empsid[]=$value->EmpsID;
		}
		$labors_id=EMPS::whereIn('EmpsID',$empsid)->where('APVTYPE','=','MANAGER')->where('MANAGER',$approves)->get();
		foreach ($labors_id as $value)
		{
			$emp_id[]=$value->EmpsID;
		}
		
		if(count($labors_jobid)>0 && count($labors_id)>0)
		{
			$employee_approve=array_merge($labors_empsid,$emp_id);
			$emps=EMPS::whereIn('EmpsID',$employee_approve)->get();
		}
		elseif(count($labors_id)>0)
		{
			$emps=EMPS::whereIn('EmpsID',$emp_id)->get();
		}
		elseif(count($labors_jobid)>0)
		{
			$emps= EMPS::whereIn('EmpsID',$labors_empsid)->get();
		}
		else{
			$emps=null;
		}
		if(count($emps)>0)
		{
			$data=array('labors'=>$labors,'emps'=>$emps);
			$returnHTML = view('laborD2S/approveD2S/approvetimesheet')->with($data)->render();
			return response()->json(array('success' => true,'edit'=>$returnHTML));
		}
		else{
			$returnHTML = view('errors/notimesheet')->render();
			return response()->json(array('success' => true,'edit'=>$returnHTML));
		}
    }

    /*Emp Approve Timesheet*/
    public function EmpApproveTimesheet()
    {
        $empid=Input::get('empid');
		$start_date = date("Y-m-d", strtotime(Input::get('weeks_start')));
		$end_date = date("Y-m-d", strtotime(Input::get('weeks_end')));
	    $labors=DB::table('labors as l')
		        ->join('emps as e', 'l.EmpsID', '=', 'e.EmpsID')
				->join('job as j', 'l.JobID', '=', 'j.JobID')
				->where('e.EmpsID', '=', $empid)
				->where('l.DELETED', '=', 'N')
				->whereBetween('DATE_WORK', array($start_date, $end_date))
				->select(DB::raw('CONCAT(l.JobID, "-", j.JNAME)  Job'),'l.CostCode as CostCode','l.CERTS as Cert',
					    'l.DATE_WORK as Date','l.SHIFT as Shift', 'l.HOURS as Hours','l.APPROVED as Approved',
					    'l.LABORS_ID as Labors_ID',
				        DB::raw('(IF(e.APVTYPE = "MANAGER", e.MANAGER, j.APPROVER)) as Approver'))
				->orderBy('l.DATE_WORK','ASC')
				->get();
		$emps=EMPS::where('EmpsID',$empid)->get();
		if(count($labors)>0)
		{
			$data=array('labors'=>$labors,'emps'=>$emps);
			$returnHTML = view('laborD2S/approveD2S/approvetimesheetemp')->with($data)->render();
			return response()->json(array('success' => true,'edit'=>$returnHTML));
		}
		else
		{
			$data=array('labors'=>$labors,'emps'=>$emps);
			$returnHTML = view('laborD2S/approveD2S/approvetimesheetemp')->with($data)->render();
			return response()->json(array('success' => true,'edit'=>$returnHTML,'action'=>'Action','norec'=>'No Timesheet to Approve'));
		}
    }

    /* Approve all Timesheet*/
    public function ApproveallTimsheet()
    {
    	$approveall=explode(",",Input::get('approveall'));
		$Approvealltimesheet=(array('APPROVED'=>'A','APPROVED_BY'=>$this->created_by,'MODIFIED'=>$this->created_at,
			                  'MODIFIED_BY'=>$this->created_by));
		DB::table('labors')->whereIn('LABORS_ID',$approveall)->where('APPROVED','=','N')->update($Approvealltimesheet);
		return response()->json(array('success' => true,'msg'=>'sucesss'));
    }

   /* Approve Timesheet*/
    public function ApproveTimesheet()
    {
        $laborsid=Input::get('laborsid');
		$Approvetimesheet=(array('APPROVED'=>'A','APPROVED_BY'=>$this->created_by,'MODIFIED'=>$this->created_at,
			               'MODIFIED_BY'=>$this->created_by));
		DB::table('labors')->where('LABORS_ID',$laborsid)->where('APPROVED','=','N')->update($Approvetimesheet);
		return response()->json(array('success' => true,'msg'=>'sucesss'));
    } 

    /* Reject Timesheet*/
    public function RejectTimesheet()
    {
        $laborsid=Input::get('laborsid');
		$labors=LABORS::where('LABORS_ID',$laborsid)->first();
		$empsid=$labors->EmpsID;
		$dates=$labors->DATE_WORK;
		$user=EMPS::where('EmpsID',$empsid)->first();
	    $email=trim($user->EMAIL);
		$fname=trim($user->ENAME);
		$Rejecttimesheet=(array('APPROVED'=>'R','MODIFIED'=>$this->created_at,'MODIFIED_BY'=>$this->created_by));
		DB::table('labors')->where('LABORS_ID',$laborsid)->update($Rejecttimesheet);
		$data = array(
			'fname'=>$fname,
			'dates'=>$dates
		);
		Mail::send('laborD2S/approveD2S.mailers', $data, function($message) use ($email)
		{
			$message->to([$email])->subject("Rejected Timesheet");
		});
		return response()->json(array('success' => true,'msg'=>'sucesss'));
    }
}

?>