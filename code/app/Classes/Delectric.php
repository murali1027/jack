<?php namespace App\Classes;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Labore;
use App\Ulocal;
use App\Uclass;
use App\Certe;
use App\Empe;
use App\Ccod;
use App\Job;
use Validator;
use View;
use Mail;
use DB;

class Delectric{

	public function __construct()
	{
		$this->empe_id=Auth::User()->EmpsID;
		$this->role=Auth::User()->ROLE;
		$this->created_at=date('Y-m-d H:i:s');
		$this->created_by=Auth::User()->USER_ID;
		$this->uclass=UCLASS::get();	      
	}

    /* View D2E Labors Timesheet*/
	public function labore()
	{
		$jobs = new Job();
		$job= $jobs->jobs();
		$uclass=$this->uclass; 
	    if ($this->role == 'USR') 
		{
			$empid=DB::table('empe')->leftJoin('job', 'empe.JOB_DEFAULT', '=', 'job.JobID')
			        ->where('empe.EmpeID',$this->empe_id)->get();
			foreach($empid as $value){
				$jobdefault = $value->JOB_DEFAULT;
			}
			$extra=CCOD::where('JobID', '=', $jobdefault)->where('LBREST', '>', 0)->groupBy('Extra')->get();
			$ccode=CCOD::where('JobID', '=', $jobdefault)->where('LBREST', '>', 0)->get();
			$ulocal=ULOCAL::where('JobID', '=', $jobdefault)->get();
			$certe=CERTE::where('JobID', '=', $jobdefault)->get();
		}
	    else 
		{
		    $auth_empe = new Empe();
		    $empid= $auth_empe->showempe();
		    $ulocal=ULOCAL::get();
			$ccode=CCOD::where('LBREST', '>', 0)->get();       
		}
		return View::make('laborD2E/view_labord2e',compact('job','ccode','empid','ulocal','uclass','extra','certe'));
	}
    
    /*Get Employee Timesheet*/
	public function getEmployee()
	{
        $emp_id=Input::get('ename');
		$uclass=$this->uclass;
		$jobs = new Job();
		$job= $jobs->jobs();
		$empid=DB::table('empe')->leftJoin('job', 'empe.JOB_DEFAULT', '=', 'job.JobID')
		       ->leftJoin('users', 'empe.EmpeID', '=', 'users.EmpsID')->where('empe.EmpeID',$emp_id)->get();
		foreach($empid as $value){
			$jobdefault=$value->JOB_DEFAULT;
		}
		$extra=CCOD::where('JobID', '=', $jobdefault)->where('LBREST', '>', 0)->groupBy('Extra')->get();
		$ccode=CCOD::where('JobID', '=', $jobdefault)->where('LBREST', '>', 0)->get();
		$ulocal=ULOCAL::where('JobID', '=', $jobdefault)->get();
		$certe=CERTE::where('JobID', '=', $jobdefault)->get();
		$returnHTML=view('/laborD2E/view_employeeD2E',compact('certe','job','ccode','empid','uclass','ulocal','extra'))->render();
		return response()->json(array('success' => true,'html'=>$returnHTML));
	}

	
    /* Post Labors Timesheet*/
	public function postlabore()
    {
    	$valid=Validator::make(Input::all(),array('jobid'=>'required','ccode'=>'required'));
		if($valid->fails())
		{
			Input::flash();
			return Redirect::to('ViewLaborD2E')->withErrors($valid);
		}
		else
		{
	        $emp_id=($this->role == 'USR' ? $this->empe_id : Input::get('ename'));
			$jobid=Input::get('jobid');
			$date=Input::get('date');
			$sundate=Input::get('sundate');
			$mondate=Input::get('mondate');
			$tuedate=Input::get('tuedate');
			$weddate=Input::get('weddate');
			$thudate=Input::get('thudate');
			$fridate=Input::get('fridate');
			$satdate=Input::get('satdate');
			$ccode=Input::get('ccode');
			$extra=Input::get('extra');
			$ulocal=Input::get('ulocal');
			$uclass=Input::get('uclass');
			$certe=Input::get('certe');
			$shift=Input::get('shift');
			$sunhours=Input::get('sunhours');
			$monhours=Input::get('hours');
			$tuehours=Input::get('tuehours');
			$wedhours=Input::get('wedhours');
			$thuhours=Input::get('thuhours');
			$frihours=Input::get('frihours');
			$sathours=Input::get('sathours');
			if(!empty($sunhours))
			{
				$labore=LABORE::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $sundate)->where('EmpeID', $emp_id)
				        ->where('DELETED','=','N')->get();
				foreach($labore as $value)	
				{
					$getHours=$value->HOURS + $sunhours;
				}
				if($getHours <= 16){}
				else
				{
					return response()->json(array('success' => true, 'msg'=>"<span class='alert alert-danger'>
					You have enter more than 16 Hours Sunday date</span>"));
				}
			}
			if(!empty($monhours))
			{
				$labore=LABORE::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $mondate)->where('EmpeID', $emp_id)
				        ->where('DELETED','=','N')->get();
				foreach($labore as $value)	
				{
					$getHours=$value->HOURS + $monhours;
				}
				if($getHours <= 16){}
				else
				{
					return response()->json(array('success' => true, 'msg'=>"<span class='alert alert-danger'>
					You have enter more than 16 Hours Monday date</span>"));
				}
			}
			if(!empty($tuehours))
			{
			    $labore=LABORE::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $tuedate)->where('EmpeID', $emp_id)
			           ->where('DELETED','=','N')->get();
				foreach($labore as $value)	
				{
					$getHours=$value->HOURS + $tuehours;
				}
				if($getHours <= 16){}
				else
				{
					return response()->json(array('success' => true, 'msg'=>"<span class='alert alert-danger'>
					You have enter more than 16 Hours Tuesday date</span>"));
				}
			}
			if(!empty($wedhours))
			{
			   $labore=LABORE::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $weddate)->where('EmpeID', $emp_id)
			          ->where('DELETED','=','N')->get();
				foreach($labore as $value)	
				{
					$getHours=$value->HOURS + $wedhours;
				}
				if($getHours <= 16){}
				else
				{
					return response()->json(array('success' => true, 'msg'=>"<span class='alert alert-danger'>
					You have enter more than 16 Hours Wednesday date</span>"));
				}
			}
			if(!empty($thuhours))
			{
				$labore=LABORE::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $thudate)->where('EmpeID', $emp_id)
				        ->where('DELETED','=','N')->get();
				foreach($labore as $value)	
				{
					$getHours=$value->HOURS + $thuhours;
				}
				if($getHours <= 16){}
				else
				{
					return response()->json(array('success' => true, 'msg'=>"<span class='alert alert-danger'>
				    You have enter more than 16 Hours Thursday date</span>"));
				}
			}
			if(!empty($frihours))
			{
				$labore=LABORE::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $fridate)->where('EmpeID', $emp_id)
						->where('DELETED','=','N')->get();
				foreach($labore as $value)	
				{
					$getHours=$value->HOURS + $frihours;
				}
				if($getHours <= 16){}
				else
				{
					return response()->json(array('success' => true, 'msg'=>"<span class='alert alert-danger'>
					You have enter more than 16 Hours Friday date</span>"));
				}
			}
			if(!empty($sathours))
			{
				$labore=LABORE::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $satdate)->where('EmpeID', $emp_id)
				        ->where('DELETED','=','N')->get();
				foreach($labore as $value)	
				{
					$getHours=$value->HOURS + $sathours;
				}
				if($getHours <= 16){}
				else
				{
					return response()->json(array('success' => true, 'msg'=>"<span class='alert alert-danger'>
				    You have enter more than 16 Hours Satday date</span>"));
				}
			}
			if(!empty($sunhours))
			{
			    $labore_data=array('JobID'=>$jobid,'CostCode'=>Input::get('ccode'),'EmpeID'=>$emp_id,'EXTRA'=>$extra,
			    	        'ULOCAL'=>$ulocal,'UCLASS'=>$uclass,'CERTE'=>$certe,'DATE_WORK'=>$sundate,'SHIFT'=>$shift,
							'HOURS'=>$sunhours,'CREATED'=>$this->created_at,'CREATED_BY' =>$this->created_by);
				LABORE::insert($labore_data);
			}
			if(!empty($monhours))
			{
				$labore_data=array('JobID'=>$jobid,'CostCode' =>Input::get('ccode'),'EmpeID'=>$emp_id,'EXTRA'=>$extra,
					        'ULOCAL'=>$ulocal,'UCLASS'=>$uclass,'CERTE'=>$certe,'DATE_WORK'=>$mondate,'SHIFT'=> $shift,
							'HOURS'=> $monhours,'CREATED'=>$this->created_at,'CREATED_BY' =>$this->created_by);	
				LABORE::insert($labore_data);
			}
			if(!empty($tuehours))
			{
			   $labore_data=array('JobID'=>$jobid,'CostCode'=>Input::get('ccode'),'EmpeID'=>$emp_id,'EXTRA'=>$extra,
			   				'ULOCAL'=>$ulocal,'UCLASS'=>$uclass,'CERTE'=>$certe,'DATE_WORK'=>$tuedate,'SHIFT'=>$shift,
			   				'HOURS'=>$tuehours,'CREATED'=>$this->created_at,'CREATED_BY'=>$this->created_by);	
				LABORE::insert($labore_data);
			}
			if(!empty($wedhours))
			{
			    $labore_data=array('JobID'=>$jobid,'CostCode'=>Input::get('ccode'),'EmpeID'=>$emp_id,'EXTRA'=>$extra,
			    	        'ULOCAL'=>$ulocal,'UCLASS'=>$uclass,'CERTE'=>$certe,'DATE_WORK'=>$weddate,'SHIFT'=>$shift,
			    	        'HOURS'=>$wedhours,'CREATED'=>$this->created_at,'CREATED_BY'=>$this->created_by);	
				LABORE::insert($labore_data);
			}
			if(!empty($thuhours))
			{
			    $labore_data=array('JobID'=>$jobid,'CostCode'=>Input::get('ccode'),'EmpeID'=> $emp_id,'EXTRA'=>$extra,
			    	        'ULOCAL'=>$ulocal,'UCLASS'=>$uclass,'CERTE'=>$certe,'DATE_WORK'=>$thudate,'SHIFT'=>$shift,
			    	        'HOURS'=>$thuhours,'CREATED'=>$this->created_at,'CREATED_BY'=>$this->created_by);
				LABORE::insert($labore_data);
			}
			if(!empty($frihours))
			{
				$labore_data=array('JobID'=>$jobid,'CostCode'=>Input::get('ccode'),'EmpeID'=>$emp_id,'EXTRA'=> $extra,
					        'ULOCAL'=>$ulocal,'UCLASS'=>$uclass,'CERTE'=>$certe,'DATE_WORK'=>$fridate,'SHIFT'=>$shift,
							'HOURS'=>$frihours,'CREATED'=>$this->created_at,'CREATED_BY' =>$this->created_by);
				LABORE::insert($labore_data);
			}
			if(!empty($sathours))
			{
				$labore_data=array('JobID'=>$jobid,'CostCode'=>Input::get('ccode'),'EmpeID'=>$emp_id,'EXTRA'=>$extra,
							'ULOCAL'=>$ulocal,'UCLASS'=>$uclass,'CERTE'=>$certe,'DATE_WORK'=>$satdate,'SHIFT'=>$shift,
							'HOURS'=> $sathours,'CREATED'=>$this->created_at,'CREATED_BY'=>$this->created_by);
				LABORE::insert($labore_data);
			}
			return response()->json(array('success' => true, 'msg'=>"<span class='alert alert-success'>
				Labor Timesheet Added Successfully</span>"));
		}
    }

    /*Edit Timesheet Page*/
    public function EditView()
    {
        $auth_empe = new Empe();
		$empid= $auth_empe->showempe();
		return view('laborD2E/editlaborsD2E.index',compact('empid'));
    }

    /*View Selected Edit Timesheet*/
    public function EditViewTimesheet()
    {
        $empid=($this->role == 'USR' ? $this->empe_id : Input::get('empid'));
        $weeks_start=Input::get('weeks_start');
		$weeks_end=Input::get('weeks_end');
		$start_date = date("Y-m-d", strtotime($weeks_start));
		$end_date = date("Y-m-d", strtotime($weeks_end));
	    $labore=DB::table('labore as l')
		        ->join('empe as e', 'l.EmpeID', '=', 'e.EmpeID')
				->join('job as j', 'l.JobID', '=', 'j.JobID')
				->where('e.EmpeID', '=', $empid)
				->whereBetween('l.DATE_WORK', array($start_date, $end_date))
				->where('l.DELETED', '=', 'N')
				->select(DB::raw('CONCAT(l.JobID, "-", j.JNAME)  Job'),'l.CostCode as CostCode','l.CERTE as Cert','l.DATE_WORK as Date','l.ULOCAL as Local',
				'l.UCLASS as Class','l.SHIFT as Shift', 'l.HOURS as Hours','l.APPROVED as Approved','l.LABORE_ID as Labore_ID',
				DB::raw('(IF(e.APVTYPE = "MANAGER", e.MANAGER, j.APPROVER)) as Approver'))->orderBy('l.DATE_WORK','ASC')
				->get();
		if(count($labore)>0)
		{			
			$data=array('labore'=>$labore);
			$returnHTML = view('laborD2E/editlaborsD2E/editViewtimesheet')->with($data)->render();
			return response()->json(array('success' => true,'edit'=>$returnHTML));
		}
		else
		{
			$data=array('labore'=>$labore);
		    $returnHTML = view('laborD2E/editlaborsD2E/editViewtimesheet')->with($data)->render();
			return response()->json(array('success' => true,'edit'=>$returnHTML,'norec'=>'Edit Timesheet Records Not Found'));
		}
    }

    /*Edit View Timesheet*/
    public function editTimesheet($id)
    {
    	$edit=LABORE::find($id);
	    $jobname=$edit->JobID;
	    $extra=$edit->EXTRA;
		$jobs = new Job();
		$job= $jobs->jobs();
		$uclass=UCLASS::get();
		$ccod=CCOD::where('JobID', '=', $jobname)->where('EXTRA',$extra)->where('LBREST', '>', 0)->get();
		$certe=CERTE::where('JobID', '=', $jobname)->get();
		$jname=JOB::where('JobID',$jobname)->get();
		
		return view('laborD2E/editlaborsD2E.edit',compact('edit','job','jname','ccod','uclass','certe'));
    }

    /*Update Timesheet*/
    public function updateTimesheet($id)
    {
        $hours=Input::get('hours');
        $date_work=Input::get('date');
		$Updatetimesheet=(array('JobID'=>Input::get('jobid'),'CostCode'=> Input::get('ccode'),'ULOCAL'=>Input::get('ulocal'),
								'UCLASS'=>Input::get('uclass'),'CERTE'=>Input::get('certe'),'SHIFT'	 =>Input::get('shift'),
								'DATE_WORK'=>$date_work,'HOURS'=>$hours,'APPROVED'=>'N','EXTRA'=>Input::get('extra'),
								'MODIFIED'=>$this->created_at,'MODIFIED_BY'=>$this->created_by));
		$empeid=Input::get('empeid');
		$labors_hours_edit=LABORE::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $date_work)
		                  ->where('EmpeID', $empeid)->where('DELETED','=','N')->where('LABORE_ID',$id)->get();
		$labore_hours=LABORE::select(DB::raw('sum(HOURS) AS HOURS'))->where('DATE_WORK', $date_work)
		             ->where('EmpeID', $empeid)->where('DELETED','=','N')->get();
		foreach($labors_hours_edit as $value)	
		{
			$getHours=$value->HOURS;
		}
		foreach($labore_hours as $value)	
		{
			$getHour=$value->HOURS-$getHours+$hours;
		}
		if($getHour <= 16)
		{
			DB::table('labore')->where('LABORE_ID','=',$id)->update($Updatetimesheet);
	        return redirect('editlaborsD2E');
		}
		else
		{
			return redirect()->back()->with('message', 'You have Enter More than 16 hours');
		}  
    }

    /*Delete Timesheet*/
    public function deleteTimesheet()
    {
    	$laboreid=Input::get('laboreid');
		$Deletetimesheet=(array('DELETED'=>'Y'));
		$delete=DB::table('labore')->where('LABORE_ID',$laboreid)->update($Deletetimesheet);
		return response()->json(array('success' => true,'msg'=>'sucesss'));
	}

	/* Approve Timesheet HomePage*/
	public function ApproveTimesheetPage()
	{
       $approve = new Empe();
	   $empid= $approve->showempe();
	   return view('laborD2E/approveD2E.index',compact('empid'));
    }

    /* Grouping Timesheet*/
   public function GroupTimesheet()
   {
        $start_date = date("Y-m-d", strtotime(Input::get('weeks_start')));
		$end_date = date("Y-m-d", strtotime(Input::get('weeks_end')));
	    $labore=DB::table('labore as l')
		        ->join('empe as e', 'l.EmpeID', '=', 'e.EmpeID')
				->join('job as j', 'l.JobID', '=', 'j.JobID')
				->where('l.DELETED', '=', 'N')
				->whereBetween('l.DATE_WORK', array($start_date, $end_date))
				->select(DB::raw('CONCAT(l.JobID, "-", j.JNAME)  Job'),'l.CostCode as CostCode','l.EmpeID as EmpeID',
					    'l.CERTE as Cert','l.DATE_WORK as Date','l.SHIFT as Shift', 'l.HOURS as Hours','l.APPROVED as Approved',
					    'l.LABORE_ID as Labore_ID','l.JobID as JobID',
				        DB::raw('(IF(e.APVTYPE = "MANAGER", e.MANAGER, j.APPROVER)) as Approver'))
				->orderBy('l.DATE_WORK','ASC')
				->get();
		foreach($labore as $value)
		{
			$empeids[]=$value->EmpeID;
			$Approver[]=trim($value->Approver);
			$job_id[]=$value->JobID;
		}
		$empeid=(count($labore)>0 ? $empeids: NULL);
		$jobid=(count($labore)>0 ? $job_id: NULL);
		$mailid  = Auth::user()->EMAIL;  
		$approves = strstr($mailid, '@', true);  
		$job=JOB::whereIn('JobID',$jobid)->where('APPROVER',$approves)->get();
		foreach ($job as $value)
		{
			$jobs_ID[]=$value->JobID;
		}
		$jobsID=(count($job)>0 ? $jobs_ID: NULL);
		$labore_job=LABORE::whereIn('JobID',$jobsID)->whereBetween('DATE_WORK', array($start_date, $end_date))->get();
		foreach ($labore_job as $value)
		{
			$empes_id[]=$value->EmpeID;
		}
		$empe_id=(count($labore_job)>0 ? $empes_id: NULL);
		$labore_jobid=EMPE::where(function($q) { $q->where('APVTYPE', '!=','MANAGER' ) ->orWhereNull('APVTYPE');})
                      ->whereIn('EmpeID',$empe_id)->get();
		foreach ($labore_jobid as $value)
		{
			$labore_empeid[]=$value->EmpeID;
		}
	    $labore_id=EMPE::whereIn('EmpeID',$empeid)->where('APVTYPE','=','MANAGER')->where('MANAGER',$approves)->get();
		foreach ($labore_id as $value)
		{
			$emp_id[]=$value->EmpeID;
		}
		if(count($labore_jobid)>0 && count($labore_id)>0)
		{
			$employee_approve=array_merge($labore_empeid,$emp_id);
			$empe=EMPE::whereIn('EmpeID',$employee_approve)->get();
		}
		elseif(count($labore_id)>0)
		{
			$empe=EMPE::whereIn('EmpeID',$emp_id)->get();
		}
		elseif(count($labore_jobid)>0)
		{
			$empe= EMPE::whereIn('EmpeID',$labore_empeid)->get();
		}
		else{
			$empe=null;
		}
		if(count($empe)>0)
		{
			$data=array('labore'=>$labore,'empe'=>$empe);
			$returnHTML = view('laborD2E/approveD2E/approvetimesheet')->with($data)->render();
			return response()->json(array('success' => true,'edit'=>$returnHTML));
		}
		else{
			$returnHTML = view('errors/notimesheet')->render();
			return response()->json(array('success' => true,'edit'=>$returnHTML));
		}
   }

   /*Emp Approve Timesheet*/
    public function EmpApproveTimesheet()
    {
    	$empid=Input::get('empid');
		$start_date = date("Y-m-d", strtotime(Input::get('weeks_start')));
		$end_date = date("Y-m-d", strtotime(Input::get('weeks_end')));
	    $labore=DB::table('labore as l')
		        ->join('empe as e', 'l.EmpeID', '=', 'e.EmpeID')
				->join('job as j', 'l.JobID', '=', 'j.JobID')
				->where('e.EmpeID', '=', $empid)
				->where('l.DELETED', '=', 'N')
				->whereBetween('DATE_WORK', array($start_date, $end_date))
				->select(DB::raw('CONCAT(l.JobID, "-", j.JNAME)  Job'),'l.CostCode as CostCode','l.CERTE as Cert','l.DATE_WORK as Date','l.ULOCAL as Local',
				'l.SHIFT as Shift', 'l.HOURS as Hours','l.APPROVED as Approved','l.LABORE_ID as Labore_ID','l.UCLASS as Class',
				DB::raw('(IF(e.APVTYPE = "MANAGER", e.MANAGER, j.APPROVER)) as Approver'))->orderBy('l.DATE_WORK','ASC')
				->get();
		$empe=EMPE::where('EmpeID',$empid)->get();
		if(count($labore)>0)
		{
			$data=array('labore'=>$labore,'empe'=>$empe);
			$returnHTML = view('laborD2E/approveD2E/approvetimesheetemp')->with($data)->render();
			return response()->json(array('success' => true,'edit'=>$returnHTML));
		}
		else
		{
			$data=array('labore'=>$labore,'empe'=>$empe);
			$returnHTML = view('laborD2E/approveD2E/approvetimesheetemp')->with($data)->render();
			return response()->json(array('success' => true,'edit'=>$returnHTML,'action'=>'Action','norec'=>'No Timesheet to Approve'));
		}
    }

    /* Approve all Timesheet*/
    public function ApproveallTimsheet()
    {
    	$approveall=explode(",",Input::get('approveall'));
		$Approvealltimesheet=(array('APPROVED'=>'A','APPROVED_BY'=>$this->created_by,'MODIFIED'=>$this->created_at,
			                  'MODIFIED_BY'=>$this->created_by));
		DB::table('labore')->whereIn('LABORE_ID',$approveall)->where('APPROVED','=','N')->update($Approvealltimesheet);
		return response()->json(array('success' => true,'msg'=>'sucesss'));
    }

    /* Approve Timesheet*/
    public function ApproveTimesheet()
    {
        $laboreid=Input::get('laboreid');
		$Approvetimesheet=(array('APPROVED'=>'A','APPROVED_BY'=>$this->created_by,'MODIFIED'=>$this->created_at,
			              'MODIFIED_BY'=>$this->created_by));
		$delete=DB::table('labore')->where('LABORE_ID',$laboreid)->where('APPROVED','=','N')->update($Approvetimesheet);
		return response()->json(array('success' => true,'msg'=>'sucesss'));
    }
    
    /* Reject Timesheet*/
    public function RejectTimesheet()
    {
        $laboreid=Input::get('laboreid');
		$labore=LABORE::where('LABORE_ID',$laboreid)->first();
		$empeid=$labore->EmpeID;
		$dates=$labore->DATE_WORK;
		$user=EMPE::where('EmpeID',$empeid)->first();
		$email=trim($user->EMAIL); 
		$fname=trim($user->ENAME);
		$Rejecttimesheet=(array('APPROVED'=>'R','MODIFIED'=>$this->created_at,'MODIFIED_BY'=>$this->created_by));
		DB::table('labore')->where('LABORE_ID',$laboreid)->update($Rejecttimesheet);
		$data = array(
			'fname'=>$fname,
			'dates'=>$dates
		);
		Mail::send('laborD2S/approveD2S.mailers', $data, function($message) use ($email)
		{
			$message->to($email)->subject("Rejected Timesheet");
		});
		return response()->json(array('success' => true,'msg'=>'sucesss',));
    }

    
}

?>