<?php namespace App\Classes;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Reims;
use App\Emps;
use App\Reime;
use App\Empe;
use View;
use DB;

class Financial{

	public function __construct()
	{
		$this->created_at=date('Y-m-d H:i:s');
		$this->created_by=Auth::User()->USER_ID;
	}
    
    /* Final Approve D-square*/
	public function GetFinalApprove()
	{
		 $emps= DB::table('reims as r')
		        ->join('emps as e', 'r.EmpsID', '=', 'e.EmpsID')
				->join('job as j', 'r.JobID', '=', 'j.JobID')
				->where('r.DELETED', '=', 'N')
				->where('r.DOWNLOADED', '=', 'N')
				->where('r.APPROVED', '=', 'I')
				->select('e.ENAME as Ename','r.EmpsID as EmpsID',DB::raw('(IF(e.APVTYPE = "MANAGER", e.MANAGER, j.APPROVER)) as Approver'))
				->groupBy('r.EmpsID')
				->get();
		
		return View::make('FinancialD2S.index',compact('emps'));
	}
	/* Approve all Reims*/
    public function FinalApproveallReims()
    {
    	$approveall=explode(",",Input::get('approveall'));
		$Approveallreims=(array('APPROVED'=>'A','APPROVED_BY'=>$this->created_by,'MODIFIED'=>$this->created_at,
			             'MODIFIED_BY'=>$this->created_by));
		DB::table('reims')->whereIn('REIMS_ID',$approveall)->where('APPROVED','=','I')->update($Approveallreims);
		return response()->json(array('success' => true,'msg'=>'sucesss'));
    }

   /* Approve Reims*/
    public function FinalApproveReims()
    {
        $reimsid=Input::get('reimsid');
		$Approvereims=(array('APPROVED'=>'A','APPROVED_BY'=>$this->created_by,'MODIFIED'=>$this->created_at,
			                 'MODIFIED_BY'=>$this->created_by));
		DB::table('reims')->where('REIMS_ID',$reimsid)->where('APPROVED','=','I')->update($Approvereims);
		return response()->json(array('success' => true,'msg'=>'sucesss'));
    } 

    /* Reject Reims*/
    public function FinalRejectReims()
    {
        $reimsid=Input::get('reimsid');
		$reims=REIMS::where('REIMS_ID',$reimsid)->first();
		$empsid=$reims->EmpsID;
		$dates=$reims->RDATE;
		$user=EMPS::where('EmpsID',$empsid)->first();
		$email=trim($user->EMAIL);
		$fname=trim($user->ENAME);
		$Rejectreims=(array('APPROVED'=>'R','MODIFIED'=>$this->created_at,'MODIFIED_BY'=>$this->created_by));
		DB::table('reims')->where('REIMS_ID',$reimsid)->update($Rejectreims);
		/*$data = array(
			'fname'=>$fname,
			'dates'=>$dates
		);
		    Mail::send('ReimbursementsD2S/approve-reimsD2S.mailers', $data, function($message) use ($email)
			{
				$message->to([$email])->subject("Rejected Reimbursement");
			});*/
		return response()->json(array('success' => true,'msg'=>'sucesss'));
    }
    
    /* Final Approval D-Electric */
    public function GetFinalApprovereime()
	{
		 $emps= DB::table('reime as r')
		        ->join('empe as e', 'r.EmpeID', '=', 'e.EmpeID')
				->join('job as j', 'r.JobID', '=', 'j.JobID')
				->where('r.DELETED', '=', 'N')
				->where('r.DOWNLOADED', '=', 'N')
				->where('r.APPROVED', '=', 'I')
				->select('e.ENAME as Ename','r.EmpeID as EmpeID',DB::raw('(IF(e.APVTYPE = "MANAGER", e.MANAGER, j.APPROVER)) as Approver'))
				->groupBy('r.EmpeID')
				->get();
		
		return View::make('FinancialD2E.index',compact('emps'));
	}
	/* Approve all Reims*/
    public function FinalApproveallReime()
    {
    	$approveall=explode(",",Input::get('approveall'));
		$Approveallreims=(array('APPROVED'=>'A','APPROVED_BY'=>$this->created_by,'MODIFIED'=>$this->created_at,
			             'MODIFIED_BY'=>$this->created_by));
		DB::table('reime')->whereIn('REIME_ID',$approveall)->where('APPROVED','=','I')->update($Approveallreims);
		return response()->json(array('success' => true,'msg'=>'sucesss'));
    }

   /* Approve Reims*/
    public function FinalApproveReime()
    {
        $reimsid=Input::get('reimsid');
		$Approvereims=(array('APPROVED'=>'A','APPROVED_BY'=>$this->created_by,'MODIFIED'=>$this->created_at,
			                 'MODIFIED_BY'=>$this->created_by));
		DB::table('reime')->where('REIME_ID',$reimsid)->where('APPROVED','=','I')->update($Approvereims);
		return response()->json(array('success' => true,'msg'=>'sucesss'));
    } 

    /* Reject Reims*/
    public function FinalRejectReime()
    {
        $reimsid=Input::get('reimsid');
		$reims=REIME::where('REIME_ID',$reimsid)->first();
		$empsid=$reims->EmpeID;
		$dates=$reims->RDATE;
		$user=EMPE::where('EmpeID',$empsid)->first();
		$email=trim($user->EMAIL);
		$fname=trim($user->ENAME);
		$Rejectreims=(array('APPROVED'=>'R','MODIFIED'=>$this->created_at,'MODIFIED_BY'=>$this->created_by));
		DB::table('reime')->where('REIME_ID',$reimsid)->update($Rejectreims);
		/*$data = array(
			'fname'=>$fname,
			'dates'=>$dates
		);
		    Mail::send('ReimbursementsD2S/approve-reimsD2S.mailers', $data, function($message) use ($email)
			{
				$message->to([$email])->subject("Rejected Reimbursement");
			});*/
		return response()->json(array('success' => true,'msg'=>'sucesss'));
    }
	
}

?>