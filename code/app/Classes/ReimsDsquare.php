<?php namespace App\Classes;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Jobcat;
use App\Reims;
use App\Emps;
use App\Ccod;
use App\Job;
use Session;
use View;
use Mail;
use DB;

class ReimsDsquare{

	public function __construct()
	{
		$this->created_at=date('Y-m-d H:i:s');
		$this->created_by=Auth::User()->USER_ID;
		$this->empsid=Auth::User()->EmpsID;
		$this->role=Auth::User()->ROLE;
	}
    /* Edit View Reimbursement*/
	public function getViewreims()
	{
		if ($this->role == 'USR') 
		{
		    $emps=DB::table('reims as r')
		        ->join('emps as e', 'r.EmpsID', '=', 'e.EmpsID')
				->join('job as j', 'r.JobID', '=', 'j.JobID')
				->where('e.EmpsID', $this->empsid)
				->where('r.DELETED', '=', 'N')
				->where('r.DOWNLOADED', '=', 'N')
				->whereIn('r.APPROVED',['N','R','I'])
				->select('e.ENAME as Ename','r.EmpsID as EmpsID')
				->orderBy('r.RDATE','ASC')
				->groupBy('r.EmpsID')
				->get();
		}
		else
		{
            $emps=DB::table('reims as r')
		        ->join('emps as e', 'r.EmpsID', '=', 'e.EmpsID')
				->join('job as j', 'r.JobID', '=', 'j.JobID')
				->where('r.DELETED', '=', 'N')
				->where('r.DOWNLOADED', '=', 'N')
				->whereIn('r.APPROVED',['N','R','I'])
				->select('e.ENAME as Ename','r.EmpsID as EmpsID')
				->orderBy('r.RDATE','ASC')
				->groupBy('r.EmpsID')
				->get();
		}
		return View::make('ReimbursementsD2S/enter-reimsD2S/index',compact('emps'));
	}
    
    /*Form Reimbursement*/
	public function formReims()
	{
		$job_cat=new Jobcat();
	    $jobcat=$job_cat->job_cat();
		$reims=DB::table('reims as r')
					->join('emps as e', 'r.EmpsID', '=', 'e.EmpsID')
					->join('job as j', 'r.JobID', '=', 'j.JobID')
					->where('r.DELETED', '=', 'N')
					->where('r.EmpsID',$this->empsid)
					->select('r.JobID as Job','r.CostCode as CostCode','r.LOC as Location',
						'r.DSCR as Description', 'r.CAT as Category','r.AMOUNT as Amount','r.RDATE as Date','r.EmpsID','e.ENAME')
					->orderBy('r.RDATE','ASC')
					->get();	
		return View::make('ReimbursementsD2S/enter-reimsD2S/create',compact('jobcat','reims'));
	}

	/*Post Reimbursements*/
	public function postReims($request)
	{
		$empid=($this->role == 'USR' ? $this->empsid : Input::get('ename'));
		$Rdate = date("mdy", strtotime(Input::get('RDATE')));
		$date = date("Y-m-d", strtotime(Input::get('RDATE')));
		if($request->hasFile('file'))
		{   
			$file = $request->file('file');
			$destinationPath = env('UPLOAD_REIMS'); // upload path
			$extension = Input::file('file')->getClientOriginalExtension(); // getting file extension
			$fileName= $empid.$Rdate. '.' . $extension; // renameing image
			$upload_success = Input::file('file')->move($destinationPath, $fileName); // uploading file to given path
		}
		else{
			$fileName=NULL;
		}
		$data=array('EmpsID'=>$empid,'JobID'=>Input::get('jobid'),'EXTRA'=>Input::get('extra'),
		            'CostCode'=>Input::get('ccode'),'CAT'=>Input::get('cat'),'LOC'=>Input::get('LOC'),
		            'DSCR'=>Input::get('DSCR'),'RDATE'=>$date,'AMOUNT'=>Input::get('AMOUNT'),'ATTACH'=>$fileName,
		            'CREATED_BY'=>$this->created_by);
		REIMS::insert($data);
		session::flash('status', 'Reimbursement added successfully');
		if($this->role == 'USR'){
			return redirect('enter-reimsD2S/create');
		}
		else{
			return redirect('enterReims');
		}
		
	}

    /*Edit View Reimbursement*/
	public function EditReims($id)
	{
	 	$edit=REIMS::find($id);
	    $jobname=$edit->JobID;
	    $CostCode=$edit->CostCode;
	    $extra=$edit->EXTRA;
		$job_cat = new Jobcat();
		$jobcat=$job_cat->job_cat();
        $ccode=DB::table('jobcat as j')->leftJoin('ccod as c', 'j.CostCode', '=', 'c.CostCode')->where('j.JobID',$jobname)->where('j.EXTRA',$extra)
		       ->select('j.CostCode as CostCode','c.DSCR as DSCR' )->groupBy('j.CostCode')->get();
		$extra=JOBCAT::select('EXTRA')->where('JobID','=', $jobname)->where('CostCode','=', $CostCode)->first();
		$ext_val=$extra->EXTRA;
		$cat=JOBCAT::select(DB::raw('CONCAT(CAT, "-", DSCR)  DSCR'), 'CAT' )->where('JobID','=', $jobname )
		             ->where('CostCode','=', $CostCode)->where('EXTRA',$ext_val)->get();
		$jname=JOB::where('JobID',$jobname)->get();             
		return view('ReimbursementsD2S/enter-reimsD2S.edit',compact('edit','jobcat','ccode','cat','jname'));
	 }

	/* Update Reimbursements*/
    public function UpdateReims($id, $request)
    {
		$empid=Input::get('EmpsID');
		$Rdate = date("mdy", strtotime(Input::get('RDATE')));
    	$date = date("Y-m-d", strtotime(Input::get('RDATE')));
		$extra=JOBCAT::select('EXTRA')->where('JobID','=',Input::get('jobid'))
		            ->where('CostCode','=',Input::get('ccode'))->first();
		$extval=trim($extra->EXTRA);

		if($request->hasFile('file'))
		{   
			$file = $request->file('file');
			$destinationPath = env('UPLOAD_REIMS'); // upload path
			$extension = Input::file('file')->getClientOriginalExtension(); // getting file extension
			$fileName= $empid.$Rdate. '.' . $extension; // renameing image
			$upload_success = Input::file('file')->move($destinationPath, $fileName); // uploading file to given path
		}
		else
		{
			$fileName=Input::get('ATTACH');
		}

        $Updatereims=(array('JobID'=>Input::get('jobid'), 'EXTRA'=>$extval,'CostCode'=> Input::get('ccode'),
							'CAT'=>Input::get('cat'),'LOC'=>Input::get('LOC'),'DSCR'=>Input::get('DSCR'),
							'RDATE'=>$date,'AMOUNT'=>Input::get('AMOUNT'),'APPROVED'=>'N','ATTACH'=>$fileName,
							'MODIFIED'=>$this->created_at,'MODIFIED_BY'=>$this->created_by));

		$reims=REIMS::find($id);
	    $reims->update($Updatereims);
		return redirect('enter-reimsD2S');
    }

    /* Delete Reimbursements*/
    public function DeleteReims($id)
    {
		$Delete_reims=(array('DELETED'=>'Y'));
		$Delreims=REIMS::find($id);
	    $Delreims->update($Delete_reims);
		return redirect('enter-reimsD2S');
	}

	/* Approve Reims HomePage*/
	public function ApproveReimsPage()
	{
		 $emps= DB::table('reims as r')
		        ->join('emps as e', 'r.EmpsID', '=', 'e.EmpsID')
				->join('job as j', 'r.JobID', '=', 'j.JobID')
				->where('r.DELETED', '=', 'N')
				->where('r.DOWNLOADED', '=', 'N')
				->whereIn('r.APPROVED',['N','R','I'])
				->select('e.ENAME as Ename','r.EmpsID as EmpsID',DB::raw('(IF(e.APVTYPE = "MANAGER", e.MANAGER, j.APPROVER)) as Approver'))
				->groupBy('r.EmpsID')
				->get();
		return View::make('ReimbursementsD2S/approve-reimsD2S/index',compact('emps'));
	}

    /* Approve all Reims*/
    public function ApproveallReims()
    {
    	$approveall=explode(",",Input::get('approveall'));
		$Approveallreims=(array('APPROVED'=>'I','APPROVED_BY'=>$this->created_by,'MODIFIED'=>$this->created_at,
			             'MODIFIED_BY'=>$this->created_by));
		DB::table('reims')->whereIn('REIMS_ID',$approveall)->where('APPROVED','=','N')->update($Approveallreims);
		return response()->json(array('success' => true,'msg'=>'sucesss'));
    }

   /* Approve Reims*/
    public function ApproveReims()
    {
        $reimsid=Input::get('reimsid');
		$Approvereims=(array('APPROVED'=>'I','APPROVED_BY'=>$this->created_by,'MODIFIED'=>$this->created_at,
			                 'MODIFIED_BY'=>$this->created_by));
		DB::table('reims')->where('REIMS_ID',$reimsid)->where('APPROVED','=','N')->update($Approvereims);
		return response()->json(array('success' => true,'msg'=>'sucesss'));
    } 

    /* Reject Reims*/
    public function RejectReims()
    {
        $reimsid=Input::get('reimsid');
		$reims=REIMS::where('REIMS_ID',$reimsid)->first();
		$empsid=$reims->EmpsID;
		$dates=$reims->RDATE;
		$user=EMPS::where('EmpsID',$empsid)->first();
		$email=trim($user->EMAIL);
		$fname=trim($user->ENAME);
		$Rejectreims=(array('APPROVED'=>'R','MODIFIED'=>$this->created_at,'MODIFIED_BY'=>$this->created_by));
		DB::table('reims')->where('REIMS_ID',$reimsid)->update($Rejectreims);
		/*$data = array(
			'fname'=>$fname,
			'dates'=>$dates
		);
		    Mail::send('ReimbursementsD2S/approve-reimsD2S.mailers', $data, function($message) use ($email)
			{
				$message->to([$email])->subject("Rejected Reimbursement");
			});*/
		return response()->json(array('success' => true,'msg'=>'sucesss'));
    }

    public function enterReims()
	{
		$job_cat=new Jobcat();
		$jobcat=$job_cat->job_cat();
	    $auth = new Emps();
		$empid= $auth->empshow();
	    return View::make('ReimbursementsD2S/enter-reimsD2S/enterReims',compact('jobcat','empid'));
	}

	public function EmpReims()
	{
		$emp=Input::get('empid');
		$reims=DB::table('reims as r')
					->join('emps as e', 'r.EmpsID', '=', 'e.EmpsID')
					->join('job as j', 'r.JobID', '=', 'j.JobID')
					->where('r.DELETED', '=', 'N')
					->where('r.EmpsID',$emp)
					->select('r.JobID as Job','r.CostCode as CostCode','r.LOC as Location', 'r.DSCR as Description', 
						     'r.CAT as Category','r.AMOUNT as Amount','r.RDATE as Date','r.EmpsID','e.ENAME')
					->orderBy('r.RDATE','ASC')
					->get();
	    if(count($reims)>0){
		$returnHTML = view('ReimbursementsD2S/enter-reimsD2S/viewReims',compact('reims'))->render();
		return response()->json(array('success' => true,'edit'=>$returnHTML));
	    }
	    else{
	    	$returnHTML = view('errors/norecordReims',compact('reims'))->render();
		    return response()->json(array('success' => true,'edit'=>$returnHTML));
	    }
	}  

}

?>