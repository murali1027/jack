<?php namespace App\Classes;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Credit;
use App\Jobcat;
use App\Ccard;
use App\Ccod;
use App\Job;
use Session;
use View;
use DB;

class Creditcard{

	public function __construct()
	{
		$this->created_at=date('Y-m-d H:i:s');
		$this->created_by=Auth::User()->USER_ID;
		//$this->empsid=Auth::User()->EmpsID;
		//$this->role=Auth::User()->ROLE;
	}
    
	public function getCcard()
	{
		$ccard=CCARD::where('DELETED','=','N')->get();
		return View::make('creditcard/index',compact('ccard'));
	}
    
  
	public function formCcard()
	{
        $job_cat=new Jobcat();
		$jobcat=$job_cat->job_cat();
	    $ccard=CREDIT::where('DELETED', '=', 'N')->get();
		return View::make('creditcard/create',compact('jobcat','ccard'));
	}

	
	public function postCcard($request){
       
        $location= substr(Input::get('LOC'), 0,5);
		$Rdate = date("mdy", strtotime(Input::get('RDATE')));
		$date = date("Y-m-d", strtotime(Input::get('RDATE')));
		$data=array('CCardID'=>Input::get('ccard'),'JobID'=>Input::get('jobid'),'EXTRA'=>Input::get('extra'),
		            'CostCode'=>Input::get('ccode'),'CAT'=>Input::get('cat'),'LOC'=>Input::get('LOC'),
		            'DSCR'=>Input::get('DSCR'),'RDATE'=>$date,'AMOUNT'=>Input::get('AMOUNT'),
		            'CREATED_BY'=>$this->created_by);
		$ccard=CCARD::insertGetId($data);
		if($request->hasFile('file'))
		{
			$file = $request->file('file');
			$destinationPath = env('UPLOAD_CC'); // upload path
			$extension = Input::file('file')->getClientOriginalExtension(); // getting file extension
			$fileName= $location.$Rdate.$ccard . '.' . $extension; // renameing image
			$upload_success = Input::file('file')->move($destinationPath, $fileName); // uploading file to given path
		}
		else{
			$fileName=NULL;
		}
		$update_data=array('ATTACH'=>$fileName);
		CCARD::where('CCARD_ID', '=', $ccard)->update($update_data);
		session::flash('status', 'Credit Card added successfully');
		return redirect('creditcard/create');                              
	}

	public function editCcard($id)
	{
        $ccard=CCARD::find($id);
        $jobname=$ccard->JobID;
	    $CostCode=$ccard->CostCode;
	    $extra=$ccard->EXTRA;
        $credit=CREDIT::where('DELETED', '=', 'N')->get();
        $job_cat=new Jobcat();
		$jobcat=$job_cat->job_cat();
		$ccode=DB::table('jobcat as j')->leftJoin('ccod as c', 'j.CostCode', '=', 'c.CostCode')->where('j.JobID',$jobname)->where('j.EXTRA',$extra)
		       ->select('j.CostCode as CostCode','c.DSCR as DSCR' )->groupBy('j.CostCode')->get();
		$extra=JOBCAT::select('EXTRA')->where('JobID','=', $jobname)->where('CostCode','=', $CostCode)->first();
		$ext_val=$extra->EXTRA;
		$cat=JOBCAT::select(DB::raw('CONCAT(CAT, "-", DSCR)  DSCR'), 'CAT' )->where('JobID','=', $jobname )
		             ->where('CostCode','=', $CostCode)->where('EXTRA',$ext_val)->get();
	    $jname=JOB::where('JobID',$jobname)->get(); 
        return view('creditcard.edit',compact('ccard','credit','jobcat','ccode','cat','jname'));
	}


    public function UpdateCcard($id, $request)
    {
        
        $location = substr(Input::get('LOC'), 0,5);
		$Rdate = date("mdy", strtotime(Input::get('RDATE')));
    	$date = date("Y-m-d", strtotime(Input::get('RDATE')));
		
		if($request->hasFile('file'))
		{
			$file = $request->file('file');
			$destinationPath = env('UPLOAD_CC'); // upload path
			$extension = Input::file('file')->getClientOriginalExtension(); // getting file extension
			$fileName= $location.$Rdate.$id . '.' . $extension; // renameing image
			$upload_success = Input::file('file')->move($destinationPath, $fileName); // uploading file to given path
		}
		else{
			$fileName=Input::get('ATTACH');
		}

		$Updatecard=(array('CCardID'=>Input::get('ccard'),'JobID'=>Input::get('jobid'), 'EXTRA'=>Input::get('extra'),'CAT'=>Input::get('cat'),
			               'CostCode'=> Input::get('ccode'),'LOC'=>Input::get('LOC'),'RDATE'=>$date, 'APPROVED'=>'N',
			               'DSCR'=>Input::get('DSCR'),'AMOUNT'=>Input::get('AMOUNT'),'ATTACH'=>$fileName,
						   'MODIFIED'=>$this->created_at,'MODIFIED_BY'=>$this->created_by));
		$Ccard=CCARD::find($id);
	    $Ccard->update($Updatecard);
		return redirect('creditcard');
    }
    public function DeleteCcard($id)
    {
		$Delete_Card=(array('DELETED'=>'Y'));
		$Delcard=CCARD::find($id);
	    $Delcard->update($Delete_Card);
		return redirect('creditcard');
	}


}

?>