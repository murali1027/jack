<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Empe extends Model {

	protected $table = 'empe';
	
	protected $primaryKey = 'EmpeID';	
	
	protected $fillable = ['ENAME','EMAIL','ULOCAL','UCLASS','CERT','HIRE_DATE','REHIRE_DATE','TERM_DATE', 'EPAY','JOB_DEFAULT',
	                       'PAYGRP','APVTYPE','MANAGER','PTO','PEMAIL'];

	public function showempe()
	{
		return EMPE::whereRaw('TERM_DATE<REHIRE_DATE')->orWhere('TERM_DATE','=',NULL)->orderBy('ENAME', 'ASC')->get();
	}
}
