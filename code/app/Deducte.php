<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Deducte extends Model {

	protected $table = 'deducte';
		
		protected $fillable = ['EmpeID', 'CHK_DATE','CAT', 'BASIS','AMOUNT'];

}
