<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Credit extends Model {

	protected $table = 'credit';
	
	protected $fillable = ['CCardID','DSCR','DELETED'];

}
