<?php namespace App\Repositories;

use App\Emps;
use DB;

class WeeklyReportD2S {

	public function TimesReport($emp_id,$startdate,$enddate)
	{

        return $labors=DB::table('labors as l')
		        ->join('emps as e', 'l.EmpsID', '=', 'e.EmpsID')
				->join('job as j', 'l.JobID', '=', 'j.JobID')
				->whereIn('e.EmpsID',$emp_id)
				->whereBetween('l.DATE_WORK', array($startdate, $enddate))
				->where('l.DELETED', '=', 'N')
				->select('l.JobID as Job','l.CostCode as CostCode','l.ULOCAL as Local','l.UCLASS as Class','l.DATE_WORK as Date',
				         'l.HOURS as Hours','l.EmpsID as EmpsID')
				->orderBy('l.DATE_WORK','ASC')
				->get();
				
	}

	public function ReimsReport($emp_id,$startdate,$enddate)
	{
        return   $reims=DB::table('reims as r')
		        ->join('emps as e', 'r.EmpsID', '=', 'e.EmpsID')
				->join('job as j', 'r.JobID', '=', 'j.JobID')
				->whereIn('e.EmpsID',$emp_id)
				->whereBetween('r.RDATE', array($startdate, $enddate))
				->where('r.DELETED', '=', 'N')
				->select('r.RDATE as Date','r.JobID as Job','r.CostCode as CostCode',
					    'r.LOC as Loc','r.CAT as Cat','r.AMOUNT as Amount','r.EmpsID as EmpsID')
				->orderBy('r.RDATE','ASC')
				->get();		
	}
}