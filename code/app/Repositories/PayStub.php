<?php namespace App\Repositories;

use stdClass;
use DB;
use View;
use Mail;
use App\Emps;
use App\Pays;
use App\Taxs;
use App\Deducts;
use Carbon\Carbon;

class PayStub {

		/**
		* @function ytdPay
		* @function weekpay
		* @function generate view
		this is old code	
		*/

    public function empPays($emp,$checkdate){
		$cat_not=['PTO Hours Accrual','PTO Hours Used','Floating Holiday Accrl','Floating Holiday Taken'];
		$pays=PAYS::where('EmpsID',$emp)->where('CHK_DATE','<=',$checkdate)->whereNotIn('CAT',$cat_not)
					->select('EmpsID','CAT',DB::raw('SUM(CASE WHEN CHK_DATE = "'.$checkdate.'" THEN UNITS ELSE 0 END) as CurrentUnits'),
							 DB::raw('SUM(CASE WHEN CHK_DATE = "'.$checkdate.'" THEN AMOUNT ELSE 0 END) as CurrentAmount'),
							 DB::raw('SUM(UNITS) as YTDUnits'),
							 DB::raw('SUM(AMOUNT) as YTDAmount'))
					 ->groupBy('CAT');
		$data=$pays->get(); 
		return $data;
	} 
	public function empTaxs($emp,$checkdate){
		$categories=['FUTA_E','MED_E','SOC_E','CASUI_E','AZSUI_E','HISUI_E','HITDI_E','ORSDI_E','ORSUI_E','TXSUI_E','NMSUI_E','WAWC_E'];
		$taxs=TAXS::where('EmpsID',$emp)->where('CHK_DATE','<=',$checkdate)->whereNotIn('CAT',$categories)
					->select('EmpsID','CAT',DB::raw('SUM(CASE WHEN CHK_DATE = "'.$checkdate.'" THEN WAGE ELSE 0 END) as CurrentWage'),
							 DB::raw('SUM(CASE WHEN CHK_DATE = "'.$checkdate.'" THEN TAX ELSE 0 END) as CurrentTax'),
							 DB::raw('SUM(WAGE) as YTDWage'),
							 DB::raw('SUM(TAX) as YTDTax'))
					 ->groupBy('CAT');
		$data=$taxs->get(); 
		return $data;
	}
	public function empDeducts($emp,$checkdate){
		$dedcutcat_not=['Direct Deposit Net'];
		$deducts=DEDUCTS::where('EmpsID',$emp)->where('CHK_DATE','<=',$checkdate)->whereNotIn('CAT',$dedcutcat_not)
					->select('EmpsID','CAT',DB::raw('SUM(CASE WHEN CHK_DATE = "'.$checkdate.'" THEN BASIS ELSE 0 END) as CurrentBasis'),
							 DB::raw('SUM(CASE WHEN CHK_DATE = "'.$checkdate.'" THEN AMOUNT ELSE 0 END) as CurrentAmount'),
							 DB::raw('SUM(BASIS) as YTDBasis'),
							 DB::raw('SUM(AMOUNT) as YTDAmount'))
					 ->groupBy('CAT');
		$data=$deducts->get(); 
		return $data;
	}
	public function empDeposit($emp,$checkdate){
		$categories=['Direct Deposit Net'];
		$deposit=DEDUCTS::where('EmpsID',$emp)->where('CHK_DATE','<=',$checkdate)->where('CAT',$categories)
				->select('EmpsID','CAT',DB::raw('SUM(CASE WHEN CHK_DATE = "'.$checkdate.'" THEN BASIS ELSE 0 END) as CurrentBasis'),
						DB::raw('SUM(CASE WHEN CHK_DATE = "'.$checkdate.'" THEN AMOUNT ELSE 0 END) as CurrentAmount'),
						DB::raw('SUM(BASIS) as YTDBasis'),
						DB::raw('SUM(AMOUNT) as YTDAmount'))
					    ->groupBy('CAT');
		$data=$deposit->get(); 
		return $data;
	}
	public function empPTO($emp,$checkdate){
		$PTO_cat='PTO Hours Accrual'; $PTO_cats='PTO Hours Used';
		$PTO=DB::table('pays as p')
		    ->join('emps as e','p.EmpsID', '=', 'e.EmpsID')
			->where('p.EmpsID',$emp)
			->select('e.EmpsID','e.PTO as PTOAvaliable',
		DB::raw('SUM(CASE WHEN p.CHK_DATE <= "'.$checkdate.'" AND p.CAT="'.$PTO_cat.'" AND p.EmpsID="'.$emp.'" THEN p.UNITS ELSE 0 END)
		as PTOAccural'),
		DB::raw('SUM(CASE WHEN p.CHK_DATE <= "'.$checkdate.'" AND p.CAT="'.$PTO_cats.'" AND p.EmpsID="'.$emp.'" THEN p.UNITS ELSE 0 END)
		as PTOAccuralUsed'));
		$data=$PTO->get(); 
		return $data;
	}
	public function empFloat($emp,$checkdate){
		$Float_cat='Floating Holiday Accrl'; $Float_cats='Floating Holiday Taken';
		$Holiday=PAYS::where('EmpsID',$emp)
			->select('EmpsID',
		DB::raw('SUM(CASE WHEN CHK_DATE <= "'.$checkdate.'" AND CAT="'.$Float_cat.'" AND EmpsID="'.$emp.'" THEN UNITS ELSE 0 END)
		as Floating_holiday'),
		DB::raw('SUM(CASE WHEN CHK_DATE <= "'.$checkdate.'" AND CAT="'.$Float_cats.'" AND EmpsID="'.$emp.'" THEN UNITS ELSE 0 END) as Floating_holiday_Taken'));
		$data=$Holiday->get(); 
		return $data;
	}

}