<?php namespace App\Repositories;

use stdClass;
use DB;
use View;
use Mail;
use App\Empe;
use App\Paye;
use App\Taxe;
use App\Deducte;
use Carbon\Carbon;

class PayStubD2E {

		/**
		* @function ytdPay
		* @function weekpay
		* @function generate view
		this is old code	
		*/

    public function empPaye($emp,$checkdate){
		$cat_not=['PTO Hours Accrual','PTO Hours Used','Floating Holiday Accrl','Floating Holiday Taken'];
		$paye=PAYE::where('EmpeID',$emp)->where('CHK_DATE','<=',$checkdate)->whereNotIn('CAT',$cat_not)
					->select('EmpeID','CAT',DB::raw('SUM(CASE WHEN CHK_DATE = "'.$checkdate.'" THEN UNITS ELSE 0 END) as CurrentUnits'),
							 DB::raw('SUM(CASE WHEN CHK_DATE = "'.$checkdate.'" THEN AMOUNT ELSE 0 END) as CurrentAmount'),
							 DB::raw('SUM(UNITS) as YTDUnits'),
							 DB::raw('SUM(AMOUNT) as YTDAmount'))
					 ->groupBy('CAT');
		$data=$paye->get(); 
		return $data;
	} 
	public function empTaxe($emp,$checkdate){
		$categories=['FUTA_E','MED_E','SOC_E','CAETT_E','CASUI_E','AZSUI_E'];
		$taxe=TAXE::where('EmpeID',$emp)->where('CHK_DATE','<=',$checkdate)->whereNotIn('CAT',$categories)
					->select('EmpeID','CAT',DB::raw('SUM(CASE WHEN CHK_DATE = "'.$checkdate.'" THEN WAGE ELSE 0 END) as CurrentWage'),
							 DB::raw('SUM(CASE WHEN CHK_DATE = "'.$checkdate.'" THEN TAX ELSE 0 END) as CurrentTax'),
							 DB::raw('SUM(WAGE) as YTDWage'),
							 DB::raw('SUM(TAX) as YTDTax'))
					 ->groupBy('CAT');
		$data=$taxe->get(); 
		return $data;
	}
	public function empDeducte($emp,$checkdate){
		$dedcutcat_not=['Direct Deposit'];
		$deducte=DEDUCTE::where('EmpeID',$emp)->where('CHK_DATE','<=',$checkdate)->whereNotIn('CAT',$dedcutcat_not)
					->select('EmpeID','CAT',DB::raw('SUM(CASE WHEN CHK_DATE = "'.$checkdate.'" THEN BASIS ELSE 0 END) as CurrentBasis'),
							 DB::raw('SUM(CASE WHEN CHK_DATE = "'.$checkdate.'" THEN AMOUNT ELSE 0 END) as CurrentAmount'),
							 DB::raw('SUM(BASIS) as YTDBasis'),
							 DB::raw('SUM(AMOUNT) as YTDAmount'))
					 ->groupBy('CAT');
		$data=$deducte->get(); 
		return $data;
	}
	public function empDeposit($emp,$checkdate){
		$categories=['Direct Deposit'];
		$deposit=DEDUCTE::where('EmpeID',$emp)->where('CHK_DATE','<=',$checkdate)->where('CAT',$categories)
				->select('EmpeID','CAT',DB::raw('SUM(CASE WHEN CHK_DATE = "'.$checkdate.'" THEN BASIS ELSE 0 END) as CurrentBasis'),
						DB::raw('SUM(CASE WHEN CHK_DATE = "'.$checkdate.'" THEN AMOUNT ELSE 0 END) as CurrentAmount'),
						DB::raw('SUM(BASIS) as YTDBasis'),
						DB::raw('SUM(AMOUNT) as YTDAmount'))
					    ->groupBy('CAT');
		$data=$deposit->get(); 
		return $data;
	}
	public function empPTO($emp,$checkdate){
		$PTO_cat='PTO Hours Accrual'; $PTO_cats='PTO Hours Used';
		$PTO=DB::table('paye as p')
		    ->join('empe as e','p.EmpeID', '=', 'e.EmpeID')
			->where('p.EmpeID',$emp)
			->select('e.EmpeID','e.PTO as PTOAvaliable',
		DB::raw('SUM(CASE WHEN p.CHK_DATE <= "'.$checkdate.'" AND p.CAT="'.$PTO_cat.'" AND p.EmpeID="'.$emp.'" THEN p.UNITS ELSE 0 END)
		as PTOAccural'),
		DB::raw('SUM(CASE WHEN p.CHK_DATE <= "'.$checkdate.'" AND p.CAT="'.$PTO_cats.'" AND p.EmpeID="'.$emp.'" THEN p.UNITS ELSE 0 END)
		as PTOAccuralUsed'));
		$data=$PTO->get(); 
		return $data;
	}
	public function empFloat($emp,$checkdate){
		$Float_cat='Floating Holiday Accrl'; $Float_cats='Floating Holiday Taken';
		$Holiday=PAYE::where('EmpeID',$emp)
			->select('EmpeID',
		DB::raw('SUM(CASE WHEN CHK_DATE <= "'.$checkdate.'" AND CAT="'.$Float_cat.'" AND EmpeID="'.$emp.'" THEN UNITS ELSE 0 END)
		as Floating_holiday'),
		DB::raw('SUM(CASE WHEN CHK_DATE <= "'.$checkdate.'" AND CAT="'.$Float_cats.'" AND EmpeID="'.$emp.'" THEN UNITS ELSE 0 END) as Floating_holiday_Taken'));
		$data=$Holiday->get(); 
		return $data;
	}

}