<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Reims extends Model {

	protected $table = 'reims';
	
	protected $primaryKey = 'REIMS_ID';	

	public $timestamps = false;
	
	protected $fillable = ['EmpsID','JobID','EXTRA','CostCode','CAT','LOC','DSCR','	REIMB', 'RDATE','AMOUNT','ATTACH','CREATED','CREATED_BY','MODIFIED','MODIFIED_BY','DELETED','APPROVED','APPROVED_BY','DOWNLOADED','DOWNLOADED_DATE'];

}
