<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Jobcat extends Model {

	protected $table = 'jobcat';
	
	protected $fillable = ['JobID','EXTRA','CostCode','CAT','DSCR'];

	public function job_cat()
	{
		return DB::table('jobcat as jc')->leftJoin('job as j', 'jc.JobID', '=', 'j.JobID')
		       ->select('jc.JobID as JobID',DB::raw('CONCAT(jc.JobID, "  ",jc.EXTRA)  JOB'),DB::raw('CONCAT(jc.EXTRA)  Extra'),'j.JNAME as JNAME' )
		       ->distinct()->get();
    }

}
