<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Ccard extends Model {

	protected $table = 'ccard';
	
	protected $primaryKey = 'CCARD_ID';	

	public $timestamps = false;
	
	protected $fillable = ['CCardID','JobID','EXTRA','CostCode','CAT','LOC','DSCR','REIMB', 'RDATE','AMOUNT','ATTACH','CREATED','CREATED_BY','MODIFIED','MODIFIED_BY','DELETED','APPROVED','APPROVED_BY','DOWNLOADED','DOWNLOADED_DATE'];

}
