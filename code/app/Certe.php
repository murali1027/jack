<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Certe extends Model {

	protected $table = 'certe';
	
	protected $fillable = ['JobID','CERTE'];
}
