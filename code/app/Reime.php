<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Reime extends Model {

	protected $table = 'reime';
	
	protected $primaryKey = 'REIME_ID';	
	
	public $timestamps = false;
	
	protected $fillable = ['EmpeID','JobID','EXTRA','CostCode','CAT','LOC','DSCR','	REIMB', 'RDATE','AMOUNT','ATTACH','CREATED','CREATED_BY','MODIFIED','MODIFIED_BY','DELETED','APPROVED','APPROVED_BY','DOWNLOADED','DOWNLOADED_DATE'];

}
