<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'IndexController@index');
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

$router->group(['middleware' => 'auth'], function()
{
   /* CRUD */
	Route::resource('users','UsersController');
	Route::resource('tickets','TicketsController');
	Route::resource('approveD2S','ApproveControllerD2S');
	Route::resource('approveD2E','ApproveControllerD2E');
	Route::resource('editlaborsD2S','EdittimeControllerD2S');
	Route::resource('editlaborsD2E','EdittimeControllerD2E');
	Route::resource('checkstubD2S','CheckstubControllerD2S');
	Route::resource('checkstubD2E','CheckstubControllerD2E');
	//Route::resource('weeklyreportD2S','WeeklyReportControllerD2S');
	Route::resource('enter-reimsD2S','ReimsControllerD2S');
	Route::resource('enter-reimsD2E','ReimsControllerD2E');
	Route::resource('approve-reimsD2S','ApprovereimsControllerD2S');
	Route::resource('approve-reimsD2E','ApprovereimsControllerD2E');
	Route::resource('creditcard','CcardController');
	Route::resource('FinancialD2S','FinancialControllerD2S');
	Route::resource('FinancialD2E','FinancialControllerD2E');

	/* EdittimeControllerD2S */
	Route::post('EditTimesheetD2S','EdittimeControllerD2S@EditTimesheetD2S');
	Route::post('DeleteD2S','EdittimeControllerD2S@DeleteD2S');

	/* EdittimeControllerD2E */
	Route::post('EditTimesheetD2E','EdittimeControllerD2E@EditTimesheetD2E');
	Route::post('DeleteD2E','EdittimeControllerD2E@DeleteD2E');

	/* ApproveControllerD2S */
	Route::post('ApproveTimesheetD2S','ApproveControllerD2S@ApproveTimesheetD2S');
	Route::post('postApproveTimesheetD2S','ApproveControllerD2S@postApproveTimesheetD2S');
	Route::post('ApproveallD2S','ApproveControllerD2S@ApproveallD2S');
	Route::post('Approve','ApproveControllerD2S@Approve');
	Route::post('Reject','ApproveControllerD2S@Reject');

	/* ApproveControllerD2E */
	Route::post('ApproveTimesheetD2E','ApproveControllerD2E@ApproveTimesheetD2E');
	Route::post('postApproveTimesheetD2E','ApproveControllerD2E@postApproveTimesheetD2E');
	Route::post('ApproveallD2E','ApproveControllerD2E@ApproveallD2E');
	Route::post('ApproveD2E','ApproveControllerD2E@ApproveD2E');
	Route::post('RejectD2E','ApproveControllerD2E@RejectD2E');

	/* HoursControllerD2S */
	Route::get('HoursD2S','TotalhoursControllerD2S@index');
	Route::post('HoursEmpDetailD2S','TotalhoursControllerD2S@HoursEmpDetailD2S');

	/* HoursControllerD2E */
	Route::get('HoursD2E','TotalhoursControllerD2E@index');
	Route::post('HoursEmpDetailD2E','TotalhoursControllerD2E@HoursEmpDetailD2E');

	/* LaborControllerD2S */
	Route::get('ViewLaborD2S','LaborController@ViewLaborD2S');
	Route::post('EmpDetails','LaborController@EmpDetails');
	Route::post('getalldates','LaborController@getalldates');
	Route::post('PostLaborD2S','LaborController@PostLaborD2S');

	/* LaborControllerD2E */
	Route::post('EmpDetailsD2E','LaborControllerD2E@EmpDetailsD2E');
	Route::get('ViewLaborD2E','LaborControllerD2E@ViewLaborD2E');
	Route::post('PostLaborD2E','LaborControllerD2E@PostLaborD2E');

	/* Import Expot */
	Route::get('ImportExport', 'ImportExportController@ImportExport');
	Route::get('runMysqlBackup', 'ImportExportController@runMysqlBackup');
	Route::get('runImportProcess', 'ImportExportController@runImportProcess');
	Route::get('runExportProcessD2S', 'ImportExportController@runExportProcessD2S');
	Route::get('runExportProcessD2E', 'ImportExportController@runExportProcessD2E');
	Route::get('runReimsProcess', 'ImportExportController@runReimsProcess');
	Route::get('runReimeProcess', 'ImportExportController@runReimeProcess');

	/*Reims D2S*/
	Route::post('viewReims', 'ReimsControllerD2S@viewReims');
	Route::get('enterReims', 'ReimsControllerD2S@enterReims');
	Route::post('EmpReims', 'ReimsControllerD2S@EmpReims');

	/*Reims D2S*/
	Route::get('enterReime', 'ReimsControllerD2E@enterReime');
	Route::post('EmpReime', 'ReimsControllerD2E@EmpReime');
	Route::post('viewReime', 'ReimsControllerD2E@viewReime');

	/* ApproveReimsControllerD2S */
	Route::post('ApproveReimsallD2S','ApprovereimsControllerD2S@ApproveReimsallD2S');
	Route::post('ApproveReims','ApprovereimsControllerD2S@ApproveReims');
	Route::post('RejectReims','ApprovereimsControllerD2S@RejectReims');
	Route::get('textattachement', 'ApprovereimsControllerD2S@textattachement');

	/* FinancialControllerD2S */
	Route::post('FinalApproveD2S','FinancialControllerD2S@FinalApproveD2S');
	Route::post('FinalApproveallD2S','FinancialControllerD2S@FinalApproveallD2S');
	Route::post('FinalRejectD2S','FinancialControllerD2S@FinalRejectD2S');

	/* FinancialControllerD2E */
	Route::post('FinalApproveD2E','FinancialControllerD2E@FinalApproveD2E');
	Route::post('FinalApproveallD2E','FinancialControllerD2E@FinalApproveallD2E');
	Route::post('FinalRejectD2E','FinancialControllerD2E@FinalRejectD2E');

	/* ApproveReimsControllerD2E */
	Route::post('ApproveReimsallD2E','ApprovereimsControllerD2E@ApproveReimsallD2E');
	Route::post('ApproveReime','ApprovereimsControllerD2E@ApproveReime');
	Route::post('RejectReime','ApprovereimsControllerD2E@RejectReime');
	Route::get('attachement', 'ApprovereimsControllerD2E@attachement');

	Route::controller('weeklyreportD2S', 'WeeklyReportControllerD2S', [
	    'getData'  => 'weeklyreportD2S.data',
	    'getIndex' => 'weeklyreportD2S'
	]);
	Route::post('ReportD2S','WeeklyReportControllerD2S@ReportD2S');
});
/* Dropdown Changes */
Route::get('PostCcod', 'DropdownController@PostCcod');
Route::get('Postcerts', 'DropdownController@Postcerts');
Route::get('Postcerte', 'DropdownController@Postcerte');
Route::get('Postulocal', 'DropdownController@Postulocal');
Route::get('PostJobcat', 'DropdownController@PostJobcat');
Route::get('Postcat', 'DropdownController@Postcat');




