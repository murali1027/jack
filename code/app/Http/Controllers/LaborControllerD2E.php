<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Classes\Delectric;

class LaborControllerD2E extends Controller {

	public function __construct(Delectric $delectric)
	{
		$this->Delectric=$delectric;
	}

	/* View TimesheetD2E */
	public function ViewLaborD2E()
	{
        return $this->Delectric->labore();		
	}

	/* Add TimesheetD2E */
	public function PostLaborD2E()
	{
		return  $this->Delectric->postlabore();
    }

   /* Timesheet EmpDetails D2E */
	public function EmpDetailsD2E()
	{
	    return $this->Delectric->getEmployee();
	}
	
}