<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Classes\Dsquare;

class EdittimeControllerD2S extends Controller {

	public function __construct(Dsquare $dsquare)
	{
		$this->Labors=$dsquare;
	}
	
	public function index()
	{
		return $this->Labors->EditView();
	}
	
    //Edit Timesheet//
	public function edit($id)
	{
		return $this->Labors->editTimesheet($id);
	}

	//Update Timesheet//
	public function update($id)
	{
		return $this->Labors->updateTimesheet($id);
	}

	//Delete Timesheet//
	public function DeleteD2S()
	{
		return $this->Labors->deleteTimesheet();
	}
	
   // DropDown select Emps Timesheet Details//
   public function EditTimesheetD2S()
   {
		return $this->Labors->EditViewTimesheet();
   }
   
}
