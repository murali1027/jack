<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Classes\Delectric;

class ApproveControllerD2E extends Controller {

 	public function __construct(Delectric $delectric)
	{
		$this->middleware('approver');
		$this->Delectric=$delectric;
	}
	
	public function index()
	{  
	   return $this->Delectric->ApproveTimesheetPage();
	}

	// Approve Timesheet D2E//
	public function ApproveD2E()
	{
		return $this->Delectric->ApproveTimesheet();
	}

	//Reject Timesheeet D2E//
	public function RejectD2E()
	{
		return $this->Delectric->RejectTimesheet();
	}
	
	//Dropdown Selected Approve and Reject Timesheeet D2E//
	public function ApproveTimesheetD2E()
	{
		return $this->Delectric->EmpApproveTimesheet();
	}
	// Grouping Timesheeet D2E//
	public function postApproveTimesheetD2E()
	{
		return $this->Delectric->GroupTimesheet();
	}
	// Approve All Timesheeet D2E//
	public function ApproveallD2E()
	{
		return $this->Delectric->ApproveallTimsheet();
	}
	

}
