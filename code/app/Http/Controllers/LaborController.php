<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Classes\Dsquare;

class LaborController extends Controller {

	public function __construct(Dsquare $dsquare)
	{
		$this->middleware('auth');
		$this->Labors=$dsquare;
	}
	
	/* Timesheet ViewLaborD2S  */
	public function ViewLaborD2S()
	{  
	   return $this->Labors->labors();
	}

	/* Enter Labors Timesheet*/
	public function PostLaborD2S()
	{
		return  $this->Labors->postlabors();
	}

	/*Get Labor Hours*/
	public function getalldates()
	{
		return  $gethours = $this->Labors->getHours();
	}
    
    /* Labor Timesheet for Admin  */
	public function EmpDetails()
	{
	   return $this->Labors->getEmployee();
	}

}