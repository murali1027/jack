<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class PasswordController extends Controller {

	use ResetsPasswords;

	protected $redirectPath = 'auth/login';
	
	public function __construct(Guard $auth, PasswordBroker $passwords)
	{
		$this->auth = $auth;
		$this->passwords = $passwords;
		$this->middleware('guest');
	}

	public function postReset(Request $request)
	{
		$this->validate($request, [
			'token' => 'required',
			'email' => 'required|email',
			'password' => 'required|confirmed',
		]);
		$credentials = $request->only('email', 'password', 'password_confirmation', 'token');
		$response = $this->passwords->reset($credentials, function($user, $password)
		{
			$user->password = bcrypt($password);
			$user->save();
            return redirect('auth/login');
			//$this->auth->login($user);
		});
		switch ($response)
		{
			case PasswordBroker::PASSWORD_RESET:
			return redirect($this->redirectPath());
			default:
				    return redirect()->back()
						->withInput($request->only('email'))
						->withErrors(['email' => trans($response)]);
		}
	}
}
