<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades;
use Validator;
use App\User;
use Illuminate\Support\Facades\Auth;
use View;
use Redirect;
use Session;

class AuthController extends Controller {

	use AuthenticatesAndRegistersUsers;

	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;
		//$this->middleware('guest', ['except' => 'getLogout']);
	}
	public function redirectPath()
	{
		if (property_exists($this, 'redirectPath'))
		{
			return $this->redirectPath;
		}
		return property_exists($this, 'redirectTo') ? $this->redirectTo : '/';
	}
	public function postLogin(Request $request)
	{
		$this->validate($request, ['username' => 'required', 'password' => 'required', ]);
		if (Auth::attempt(['email' => $request->username, 'password' => $request->password]))
		{
			return redirect()->intended($this->redirectPath());
		} 
		elseif(Auth::attempt(['username'=> $request->username, 'password' => $request->password]))
		{
			return redirect()->intended($this->redirectPath());
		} 
		return redirect($this->loginPath())
        ->withInput($request->only('username', 'remember'))
        ->withErrors([
            'username' => $this->getFailedLoginMessage(),
        ]);
	}
	
	protected function getFailedLoginMessage()
	{
		return 'Invalid Username/Password.';
	}
	
	
	protected function validator(array $data)
	{
		return Validator::make($data, [
				'email' => 'required|email|max:255|unique:users',
				'name'  => 'required',
				'fname' => 'required',
				'lname' => 'required',
				'domain'=>'required',
				'role'  => 'required',
			]);
	}	
	
	public function postRegister(Request $request)
	{
		$validator = $this->validator($request->all());
		if ($validator->fails())
		{
			$this->throwValidationException($request, $validator);
		}
		
		$user = new User;
		$user->USERNAME = $request->input('name');
		$user->EmpsID = $request->input('empid');
	    $user->EMAIL = $request->input('email');
		$user->DOMAIN = $request->input('domain');
	    $user->FNAME = $request->input('fname');
		$user->LNAME = $request->input('lname');
		$user->ROLE = $request->input('role');
		$user->save();
		session::flash('status', 'User Added successfully');
		return Redirect::to('users');
	}
	public function getLogout()
	{
		Auth::logout();
		return Redirect::to('/');
	}
	

}
