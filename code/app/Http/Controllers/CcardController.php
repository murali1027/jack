<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Classes\Creditcard;

class CcardController extends Controller {

	public function __construct(Creditcard $ccard) {

		$this->Creditcard=$ccard;
	}

	public function index()
	{
		return $this->Creditcard->getCcard();
	}

	public function create()
	{
		return $this->Creditcard->formCcard();
	}

	public function store(Request $request)
	{
		return $this->Creditcard->postCcard($request);
	}

    public function edit($id)
	{
		return $this->Creditcard->editCcard($id);
	}

    public function update($id, Request $request)
	{
		return $this->Creditcard->UpdateCcard($id,$request);
	}

    public function destroy($id)
	{
		return $this->Creditcard->DeleteCcard($id);
	}

}
