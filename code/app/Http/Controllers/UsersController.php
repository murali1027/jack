<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\User;
use Session;

class UsersController extends Controller {

	public function __construct()
	{
		$this->middleware('admin');
	}

	//View user//
	public function index()
	{
		$users=USER::where('DELETED','=','N')->paginate(15);
        return view('users.index',compact('users'));
	}

	// Edit User//
	public function edit($id)
	{
		$user=USER::find($id);
        return view('users.edit',compact('user'));
	}

	//Update User//
	public function update($id)
	{
		$userUpdate=(array(
		'USERNAME'=>Input::get('USERNAME'),
		'EmpsID'=>Input::get('EmpsID'),
		'FNAME'=>Input::get('FNAME'),
		'LNAME'=>Input::get('LNAME'),
		'EMAIL'=>Input::get('EMAIL'),
		'DOMAIN'=>Input::get('domain'),
		'ROLE'=>Input::get('role')
		));
	   $user=USER::find($id);
	   $user->update($userUpdate);
	   session::flash('status', 'User Profile Updated Successfully');
	   return redirect('users');
	}

	//Delete User//
	public function destroy($id)
	{
		$Deleteuser=(array('DELETED'=>'Y'));
		USER::find($id)->update($Deleteuser);
		session::flash('status', 'User Profile Deleted Successfully');
	    return redirect('users');
	}
	
 
}
