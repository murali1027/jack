<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Classes\ReimeDelectric;
use Illuminate\Support\Facades\Input;

class ApprovereimsControllerD2E extends Controller {

	public function __construct(ReimeDelectric $reime)
	{
		$this->middleware('approver');
		$this->ReimeDelectric=$reime;                
	}

	public function index()
	{
		return $this->ReimeDelectric->ApproveReimsPage();
	}

	// Approve Reims D2E//
    public function ApproveReime()
	{
		return $this->ReimeDelectric->ApproveReims();
	}

	//Reject Reims D2E//
	public function RejectReime()
	{
		return $this->ReimeDelectric->RejectReims();
	}

	//Dropdown Selected Approve and Reject Timesheeet D2E//
	public function ApproveReimsD2E()
	{ 
		return $this->ReimeDelectric->EmpApproveReims();
	}

    public function ApproveReimsallD2E()
	{
		return $this->ReimeDelectric->ApproveallReims();
	}

	// Grouping Timesheeet D2E//
	public function postApproveReimsD2E()
	{
		return $this->ReimeDelectric->GroupReims();
	}
    
    // Attachment //
    public function attachement()
    {  
        header('Content-Type: image/png');
        $imagepath = env('UPLOAD_REIME');
		$file=Input::get('file');
		echo file_get_contents("../../../../..".$imagepath.'/' . $file);
    }

}