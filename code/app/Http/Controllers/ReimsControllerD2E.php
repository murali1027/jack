<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Classes\ReimeDelectric;

class ReimsControllerD2E extends Controller {

	public function __construct(ReimeDelectric $reime)
	{
		$this->middleware('user', ['only' => ['create']]);
		$this->ReimeDelectric=$reime;                
	}

	public function index()
	{
		return $this->ReimeDelectric->getViewreime();
	}

	public function create()
	{
		return $this->ReimeDelectric->formReime();
	}

    public function store(Request $request)
    {
    	return $this->ReimeDelectric->postReime($request);
	}
	
	public function edit($id)
	{
		return $this->ReimeDelectric->EditReime($id);
	}

	public function update($id, Request $request)
	{
       return $this->ReimeDelectric->UpdateReime($id,$request);
    }

	public function destroy($id)
	{
		return $this->ReimeDelectric->DeleteReime($id);
	}

	public function enterReime()
	{
		return $this->ReimeDelectric->enterReime();
	}
	
    public function EmpReime()
	{
		return $this->ReimeDelectric->EmpReime();
	}


}
