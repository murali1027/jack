<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Empe;
use App\Paye;
use Carbon\Carbon;
use PDF;
use Mail;
use Session;
use View;
use App\Repositories\PayStubD2E;

class CheckstubControllerD2E extends Controller {

	public function __construct(PayStubD2E $PayStubRepos)
    {
        $this->PayStubRepos = $PayStubRepos;
	}
	
	public function index()
	{
		$auth_empe = new Empe();
		$empid= $auth_empe->showempe();
		return view('checkstubD2E.index',compact('empid'));
	}

	
	public function store(Request $request)
	{
		$PayStubRepos=$this->PayStubRepos;
	    $user_role=Auth::User()->ROLE;
	   if ($user_role === 'ADM') 
		{
			$emp_id=Input::get('employee');
			$this->validate($request, ['employee'  =>'required','checkstubDate' => 'required' ]);
		}else{
			$emp_id=Auth::User()->EmpsID;
			$this->validate($request, ['checkstubDate' => 'required' ]);
        } 
        
        $weeks=date("Y-m-d", strtotime(Input::get('checkstubDate')));
        $paye=PAYE::where('EmpeID',$emp_id)->where('CHK_DATE',$weeks)->first();
        if(count($paye) > 0){
        $emps_paye = $PayStubRepos->empPaye($emp_id,$weeks);
		$emps_taxe= $PayStubRepos->empTaxe($emp_id,$weeks);
		$emps_deducte= $PayStubRepos->empDeducte($emp_id,$weeks);
		$emps_deposit= $PayStubRepos->empDeposit($emp_id,$weeks);
		$emp_pto= $PayStubRepos->empPTO($emp_id,$weeks);
		$emp_holiday= $PayStubRepos->empFloat($emp_id,$weeks);
		$emp=EMPE::where('EmpeID',$emp_id)->first();
		$data = [
			'EmpsID'=>$emp->EmpeID,
			'email'=>trim($emp->PEMAIL),
			'ename'=>$emp->ENAME,
			'weekending'=> date_format(Carbon::createFromFormat('Y-m-d',$weeks), 'm-d-Y'),
			'checkdate'=>date_format(Carbon::createFromFormat('Y-m-d',$weeks)->AddDay(6), 'm-d-Y'),
			'pays'=>$emps_paye,
			'taxs'=>$emps_taxe,
			'deducts'=>$emps_deducte,
			'deposit'=>$emps_deposit,
			'pto'=>$emp_pto,
			'float'=>$emp_holiday
		];
		$ename=trim($emp->ENAME);
		$email=trim($emp->PEMAIL);
	    $pdf=PDF::loadView('checkstubD2E/pdf' ,$data);
		return $pdf->download("$ename-Check stub-($weeks).pdf");
	    }
	    else{
	    	return redirect()->back()->with('checkstub', 'No Checkstub available for this week.');
	    }

	}

}
