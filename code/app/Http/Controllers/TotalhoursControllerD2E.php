<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Empe;
use App\Labore;
use DB;

class TotalhoursControllerD2E extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('laborD2E/hoursD2E.index');
	}

		// DropDown select Emps Hours Details//
    public function HoursEmpDetailD2E()
    {
	    $start_date = date("Y-m-d", strtotime(Input::get('weeks_start')));
		$end_date = date("Y-m-d", strtotime(Input::get('weeks_end')));
		$auth_empe = new Empe();
		$labore_hours= $auth_empe->showempe();
		$labore_date=LABORE::whereBetween('DATE_WORK', array($start_date, $end_date))->get();
		$data=array('labore_hours'=>$labore_hours,'labore_date'=>$labore_date);
		$returnHTML = view('laborD2E/hoursD2E/view')->with($data)->render();
		return response()->json(array('success' => true,'edit'=>$returnHTML));
	}

}
