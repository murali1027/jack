<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Classes\Financial;
use Illuminate\Http\Request;

class FinancialControllerD2E extends Controller {

	public function __construct(Financial $financial)
	{
		$this->Financial=$financial;
	}

	public function index()
	{
		return $this->Financial->GetFinalApprovereime();
		
	}
    
    public function FinalApproveallD2E()
	{
		return $this->Financial->FinalApproveallReime();
	}

	public function FinalApproveD2E()
	{
		return $this->Financial->FinalApproveReime();
	}
	
    public function FinalRejectD2E()
	{
		return $this->Financial->FinalRejectReime();
	}

}
