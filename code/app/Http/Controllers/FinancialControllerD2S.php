<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Classes\Financial;
use Illuminate\Http\Request;

class FinancialControllerD2S extends Controller {

	public function __construct(Financial $financial)
	{
		$this->Financial=$financial;
	}

	public function index()
	{
		return $this->Financial->GetFinalApprove();
		
	}
    
    public function FinalApproveallD2S()
	{
		return $this->Financial->FinalApproveallReims();
	}

	public function FinalApproveD2S()
	{
		return $this->Financial->FinalApproveReims();
	}
	
    public function FinalRejectD2S()
	{
		return $this->Financial->FinalRejectReims();
	}
     
 }
