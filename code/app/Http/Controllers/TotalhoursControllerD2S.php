<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Labors;
use App\Emps;
use DB;

class TotalhoursControllerD2S extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('laborD2S/hoursD2S.index');
	}

	// DropDown select Emps Hours Details//
   public function HoursEmpDetailD2S()
   {
	    $start_date = date("Y-m-d", strtotime(Input::get('weeks_start')));
		$end_date = date("Y-m-d", strtotime(Input::get('weeks_end')));
		$auth = new Emps();
		$labors_hours= $auth->empshow();
		$labors_date=LABORS::whereBetween('DATE_WORK', array($start_date, $end_date))->get();
		$data=array('labors_hours'=>$labors_hours,'labors_date'=>$labors_date);
		$returnHTML = view('laborD2S/hoursD2S/view')->with($data)->render();
		return response()->json(array('success' => true,'edit'=>$returnHTML));
	}

}
