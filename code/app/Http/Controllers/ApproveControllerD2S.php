<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Classes\Dsquare;

class ApproveControllerD2S extends Controller {
   
    public function __construct(Dsquare $dsquare)
	{
		$this->middleware('approver');
		$this->Labors=$dsquare;
	}
	
	public function index()
	{  
		return $this->Labors->ApproveTimesheetPage();
	}
	
	// Approve Timesheet D2S//
    public function Approve()
	{
		return $this->Labors->ApproveTimesheet();
	}

	//Reject Timesheeet D2S//
	public function Reject()
	{
		return $this->Labors->RejectTimesheet();
	}

	//Dropdown Selected Approve and Reject Timesheeet D2S//
	public function ApproveTimesheetD2S()
	{
		return $this->Labors->EmpApproveTimesheet();
	}

	// Grouping Timesheeet D2S//
	public function postApproveTimesheetD2S()
	{
		return $this->Labors->GroupTimesheet();
	}

	// Approve All Timesheeet D2S//
	public function ApproveallD2S()
	{
		return $this->Labors->ApproveallTimsheet();
	}
}
