<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Ulocal;
use App\Certe;
use App\Ccod;
use App\Certs;
use App\Jobcat;
use Response;
use DB;

class DropdownController extends Controller {

	public function PostCcod()
	{
		$job_id=Input::get('job_id');
		$extra=Input::get('extra');
	    $ccode=CCOD::where('JobID','=', $job_id )->where('Extra',$extra)->where('LBREST', '>', 0)->get();
		return Response::json($ccode);
	}

	public function Postcerts()
	{
		$job_id=Input::get('job_id');
	    $certs=CERTS::where('JobID','=', $job_id )->get();
	    return Response::json($certs);
	}

	public function Postcerte()
	{
		$job_id=Input::get('job_id');
	    $certe=CERTE::where('JobID','=', $job_id )->get();
	    return Response::json($certe);
	}

	public function Postulocal()
	{
		$job_id=Input::get('job_id');
	    $ulocal=ULOCAL::where('JobID','=', $job_id )->get();
	    return Response::json($ulocal);
	}
    
    public function PostJobcat()
	{
		$job_id=Input::get('job_id');
		$extra=Input::get('extra');
		$ccode=DB::table('jobcat as j')->leftJoin('ccod as c', 'j.CostCode', '=', 'c.CostCode')->where('j.JobID',$job_id)->where('j.EXTRA',$extra)
		       ->select('j.CostCode as CostCode','c.DSCR as DSCR' )->groupBy('j.CostCode')->get();
		return Response::json($ccode);
	}
    public function Postcat()
	{
		$job_id=Input::get('job_id');
		$ccode=Input::get('ccode');
		$extra=JOBCAT::select('EXTRA')->where('JobID','=', $job_id )->where('CostCode','=', $ccode)->first();
		$ext_val=$extra->EXTRA;
		$cat=JOBCAT::select(DB::raw('CONCAT(CAT, " - ", DSCR)  CATEGORY'), 'CAT' )->where('JobID','=', $job_id )
		             ->where('CostCode','=', $ccode)->where('EXTRA',$ext_val)->get();
	    return Response::json($cat);
	}
}
