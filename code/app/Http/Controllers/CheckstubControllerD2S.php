<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Emps;
use App\Pays;
use Carbon\Carbon;
use PDF;
use Mail;
use Session;
use View;
use App\Repositories\PayStub;

class CheckstubControllerD2S extends Controller {

	public function __construct(PayStub $PayStubRepo)
    {
        $this->PayStubRepo = $PayStubRepo;
	}
	
	public function index()
	{
		$auth = new Emps();
		$empid= $auth->empshow();
		return view('checkstubD2S.index',compact('empid'));
	}

	
	public function store(Request $request)
	{
		$PayStubRepo=$this->PayStubRepo;
	    $user_role=Auth::User()->ROLE;
	   if ($user_role === 'ADM') 
		{
			$emp_id=Input::get('employee');
			$this->validate($request, ['employee'  =>'required','checkstubDate' => 'required' ]);
		}else{
			$emp_id=Auth::User()->EmpsID;
			$this->validate($request, ['checkstubDate' => 'required' ]);
        } 
        
        $weeks=date("Y-m-d", strtotime(Input::get('checkstubDate')));
        $pays=PAYS::where('EmpsID',$emp_id)->where('CHK_DATE',$weeks)->first();
        if(count($pays) > 0){
        $emps_pays = $PayStubRepo->empPays($emp_id,$weeks);
		$emps_taxs= $PayStubRepo->empTaxs($emp_id,$weeks);
		$emps_deducts= $PayStubRepo->empDeducts($emp_id,$weeks);
		$emps_deposit= $PayStubRepo->empDeposit($emp_id,$weeks);
		$emp_pto= $PayStubRepo->empPTO($emp_id,$weeks);
		$emp_holiday= $PayStubRepo->empFloat($emp_id,$weeks);
		$emp=EMPS::where('EmpsID',$emp_id)->first();
		$data = [
			'EmpsID'=>$emp->EmpsID,
			'email'=>trim($emp->PEMAIL),
			'ename'=>$emp->ENAME,
			'weekending'=> date_format(Carbon::createFromFormat('Y-m-d',$weeks), 'm-d-Y'),
			'checkdate'=>date_format(Carbon::createFromFormat('Y-m-d',$weeks)->AddDay(6), 'm-d-Y'),
			'pays'=>$emps_pays,
			'taxs'=>$emps_taxs,
			'deducts'=>$emps_deducts,
			'deposit'=>$emps_deposit,
			'pto'=>$emp_pto,
			'float'=>$emp_holiday
		];
		$ename=trim($emp->ENAME);
		$pdf=PDF::loadView('checkstubD2S/pdf' ,$data);
        return $pdf->download("$ename-Check stub-($weeks).pdf");
        }
	    else{
	    	return redirect()->back()->with('checkstub', 'No Checkstub available for this week.');
	    }
	    
	}
}
