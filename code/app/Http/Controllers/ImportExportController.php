<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
Use DB;
use Excel;
use Session;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class ImportExportController extends Controller {
    
    public function __construct()
	{
		$this->middleware('admin');
	}
	
	public function ImportExport()
	{
		return view('admin.ImportExport');
	}
	
	public function runExportProcessD2S()
	{
		$labors=DB::table('labors as l')
		->join('emps as e', 'l.EmpsID', '=', 'e.EmpsID')
		->where('l.DELETED', '=', 'N')
		->where('l.APPROVED', '=', 'A')
		->where('l.DOWNLOADED', '=', 'N')
		->select('l.DATE_WORK as Dates',DB::raw(TRIM('DATE_FORMAT(l.DATE_WORK, "%m/%d/%y") as Date')),TRIM('l.EmpsID as EmpID'), TRIM('l.JobID as JobID'),
				DB::raw(TRIM('IF(l.EXTRA IS NULL, "", l.EXTRA) as Extra')),TRIM('l.CostCode as CostCode'),
				DB::raw(TRIM('IF(l.ULOCAL IS NULL, "", l.ULOCAL) as UnionLocal')),DB::raw(TRIM('IF(l.UCLASS IS NULL, "", l.UCLASS) as UnionClass')),
				DB::raw(TRIM('(REPLACE(REPLACE(REPLACE(IF(l.CERTS IS NULL, "", l.CERTS),"\t",""),"\n",""),"\r","")) as CertClass')),TRIM('l.SHIFT as Shift'), 
				DB::raw(TRIM('(CASE WHEN l.JobID = "90-0001" AND e.EPAY = 1 THEN 5 WHEN l.JobID = "90-0002" AND e.EPAY = 1 THEN 6 ELSE e.EPAY END) EPay')),
				TRIM('l.HOURS as Hours'))->get();
		$data = array();
		foreach($labors as $result) {
			$labors_date=$result->Dates;
			$temp = (array)$result;  
			$temp['L'] = 'L';
			$temp['IBEW'] = '';
			$temp['X'] = 'x';
			$data[] = $temp;
			$download_update=array('DOWNLOADED'=> 'Y','DOWNLOADED_DATE'=>date('Y-m-d'));
			DB::table('labors')->where('DATE_WORK',$labors_date)->where('APPROVED','=','A')->update($download_update);  
		}
		if(count($data) >0 ){
			$total=count($data)-1;
			$x=0;
			while($x <= $total)
			{
			  $data_id=$data[$x];
			  $x++;
			  $arrayemps[]=["Date" => $data_id["Date"],"EmpID" => $data_id["EmpID"],"JobID" => $data_id["JobID"],"Extra" => $data_id["Extra"],
			  "CostCode" => $data_id["CostCode"],"L" => $data_id["L"],"IBEW" => $data_id["IBEW"],"UnionLocal" => $data_id["UnionLocal"],
			  "UnionClass" => $data_id["UnionClass"],"X" => $data_id["X"],"CertClass" => $data_id["CertClass"],"Shift" => $data_id["Shift"],
			  "EPay" => $data_id["EPay"],"Hours" => $data_id["Hours"]];
			}
			$filelabors='labors_'.date('m-d-y_h_i_s');
			Excel::create($filelabors, function($excel) use($arrayemps) {
				$excel->sheet('mySheet', function($sheet) use($arrayemps)
				{
					$sheet->fromArray($arrayemps,null,'A1',true,false);
				});
			})->store('txt', storage_path('/var/labor'));
		
			$process = new Process('/usr/src/myvpaz/upload_files.sh'.' '.$filelabors.'.txt  >>/var/mysqldump/export_labor.log');
			try {
				$process->run();
				if ($process->isSuccessful()) {
					session::flash('export', 'Export Labor Timesheet downloaded successfully with '.count($data).' records');
					return redirect('ImportExport')->with('lastrun', date('m/d/y H:i:s'));
				}
				else { 
					session::flash('errexport', 'Export Labor Timesheet not downloaded');
					return redirect('ImportExport')->with('lastrun', date('m/d/y H:i:s'));
				}
			} 
			catch(ProcessFailedException $e) {
				echo $e->getMessage();
			}
		}
		else {
			session::flash('errexport', 'There were 0 records to download');
			return redirect('ImportExport');
		}
	}

	public function runExportProcessD2E()
	{
		$labore=DB::table('labore as l')
			->join('empe as e', 'l.EmpeID', '=', 'e.EmpeID')
			->where('l.DELETED', '=', 'N')
			->where('l.APPROVED', '=', 'A')
			->where('l.DOWNLOADED', '=', 'N')
			->select('l.DATE_WORK as Dates',DB::raw(TRIM('DATE_FORMAT(l.DATE_WORK, "%m/%d/%y") as Date')),TRIM('l.EmpeID as EmpID'), TRIM('l.JobID as JobID'),
				DB::raw(TRIM('IF(l.EXTRA IS NULL, "", l.EXTRA) as Extra')),TRIM('l.CostCode as CostCode'),
				DB::raw(TRIM('IF(l.ULOCAL IS NULL, "", l.ULOCAL) as UnionLocal')),DB::raw(TRIM('IF(l.UCLASS IS NULL, "", l.UCLASS) as UnionClass')),
				DB::raw(TRIM('(REPLACE(REPLACE(REPLACE(IF(l.CERTE IS NULL, "", l.CERTE),"\t",""),"\n",""),"\r","")) as CertClass')),TRIM('l.SHIFT as Shift'), 
				DB::raw(TRIM('(CASE WHEN l.JobID = "99-0001" AND e.EPAY = 1 THEN 5 WHEN l.JobID = "99-0002" AND e.EPAY = 1 THEN 6 ELSE e.EPAY END) EPay')),
				TRIM('l.HOURS as Hours'))->get();		
		$datae = array();
		foreach($labore as $result) {
			$labore_date=$result->Dates;
			$temp = (array)$result;  
			$temp['L'] = 'L';
			$temp['IBEW'] = 'IBEW';
			$temp['Blank'] = 'x';
			$datae []= $temp;
			$download_update=array('DOWNLOADED'=> 'Y','DOWNLOADED_DATE'=>date('Y-m-d'));
			DB::table('labore')->where('DATE_WORK',$labore_date)->where('APPROVED','=','A')->update($download_update);
		}
		if(count($datae) >0 ){
			$total=count($datae)-1;
			$x=0;
			while($x <= $total)
			{
			  $data_id=$datae[$x];
			  $x++;
			  $arrayempe[]=["Date" => $data_id["Date"],"EmpID" => $data_id["EmpID"],"JobID" => $data_id["JobID"],"Extra" => $data_id["Extra"],
			  "CostCode" => $data_id["CostCode"],"L" => $data_id["L"],"IBEW" => $data_id["IBEW"],"UnionLocal" => $data_id["UnionLocal"],
			  "UnionClass" => $data_id["UnionClass"],"Blank" => $data_id["Blank"],"CertClass" => $data_id["CertClass"],"Shift" => $data_id["Shift"],
			  "EPay" => $data_id["EPay"],"Hours" => $data_id["Hours"]];
			}
			$filelabore = 'labore_'.date('m-d-y_h_i_s');
			Excel::create($filelabore, function($excel) use ($arrayempe) {
				$excel->sheet('mySheet', function($sheet) use ($arrayempe)
				{
					 $sheet->fromArray($arrayempe,null,'A1',true,false);
				});
			})->store('txt', storage_path('/var/labor'));
			$process = new Process('/usr/src/myvpaz/upload_files.sh'.' '.$filelabore.'.txt >>/var/mysqldump/export_labor.log');
			try {
				$process->run();
				if ($process->isSuccessful()) {
					session::flash('exportD2E', 'Export D2E Labor Timesheet downloaded successfully with '.count($datae).' records');
					return redirect('ImportExport')->with('lastrun', date('m/d/y H:i:s'));
				}
				else { 
					session::flash('errexport', 'Export Labor Timesheet not downloaded');
					return redirect('ImportExport')->with('lastrun', date('m/d/y H:i:s'));
				}
			}
			catch (ProcessFailedException $e) {
				echo $e->getMessage();
			}
		}
		else
		{
			session::flash('errexport', 'There were 0 records to download');
			return redirect('ImportExport');
		}
	}
	
	public function runMysqlBackup()
	{
		$process = new Process('/usr/src/myvpaz/mysql_backup.sh -b >>/var/mysqldump/mysqldump.log');
		try {
			$process->run();
			if ($process->isSuccessful()) {
				session::flash('mysql', 'Backup Mysql Executed successfully');
				return redirect('ImportExport')->with('param', date('m/d/y H:i:s'));
			}
			else
			{ 
			    session::flash('errmysql', 'Backup Mysql Not Executed');
			    return redirect('ImportExport')->with('param', date('m/d/y H:i:s'));
			}
		}
		catch (ProcessFailedException $e) {
			echo $e->getMessage();
		}
	}
		
	public function runImportProcess()
	{
		$process = new Process('/usr/src/myvpaz/load_files.sh >>/var/mysqldump/import_files.log');
		try {
			$process->run();
			if ($process->isSuccessful()) {
				session::flash('import', 'Import Tables Executed successfully');
				return redirect('ImportExport')->with('params', date('m/d/y H:i:s'));
			}
			else{
				session::flash('errimport', 'Import Tables Not Executed');
				return redirect('ImportExport')->with('params', date('m/d/y H:i:s'));
			}
		}
		catch (ProcessFailedException $e) {
			echo $e->getMessage();
		}
	}
    /* D-Square Reimbursement*/
	public function runReimsProcess()
	{
        $reims=DB::table('reims')->where('DELETED', '=', 'N')->where('DOWNLOADED', '=', 'N')
			    ->select('EmpsID','JobID','EXTRA','CostCode','CAT','LOC', 'LOC', 'DSCR',
			     DB::raw(TRIM('DATE_FORMAT(RDATE, "%m/%d/%y") as RDATE')),'AMOUNT')->get();		
		foreach($reims as $result)
		{
		    $Date=$result->RDATE;
			$temp = (array)$result;  
			$temp[''] = 'REIMB';
			$dataReims []= $temp;
			$download_update=array('DOWNLOADED'=> 'Y','DOWNLOADED_DATE'=>date('Y-m-d'));
			DB::table('reims')->where('RDATE',$Date)->update($download_update);
		}
		
		if(count($reims) >0 ){
			foreach ($dataReims as &$outputItem) {
			$arrayOfElementsToMove = ["" => $outputItem[""]];
			unset($outputItem[""]);
				$insertionPosition = array_search("DSCR", array_keys($outputItem));
			$insertionPosition++;
			$outputItem = array_slice($outputItem, 0, $insertionPosition, true) +
	        $arrayOfElementsToMove +
	        array_slice($outputItem, $insertionPosition, NULL, true);
        }
		$filelreims = 'reims_'.date('m-d-y_h_i_s');
		Excel::create($filelreims, function($excel) use ($dataReims) {
			$excel->sheet('mySheet', function($sheet) use ($dataReims)
			{
				 $sheet->fromArray($dataReims,null,'A1',true,false);
			});
		})->store('txt', storage_path('/var/labor'));
		$process = new Process('/usr/src/myvpaz/upload_files.sh'.' '.$filelreims.'.txt >>/var/mysqldump/export_reims.log');
			try {
				$process->run();
				if ($process->isSuccessful()) {
					session::flash('exportReims', 'Export D-Square reimbursement downloaded successfully with '.count($reims).' records');
					return redirect('ImportExport')->with('lastrunReims', date('m/d/y H:i:s'));
				}
				else { 
					session::flash('errexportReims', 'Export reimbursement not downloaded');
					return redirect('ImportExport')->with('lastrunReims', date('m/d/y H:i:s'));
				}
			}
			catch (ProcessFailedException $e) {
				echo $e->getMessage();
			}
		}
		else
		{
			session::flash('errexportReims', 'There were 0 records to download');
			return redirect('ImportExport');
		}
	}
    /* D2Electric Reimbursement*/
	public function runReimeProcess()
	{
        $reime=DB::table('reime')->where('DELETED', '=', 'N')->where('DOWNLOADED', '=', 'N')
			    ->select('EmpeID','JobID','EXTRA','CostCode','CAT','LOC', 'LOC', 'DSCR',  DB::raw(TRIM('DATE_FORMAT(RDATE, "%m/%d/%y") as RDATE')),'AMOUNT')->get();		
		foreach($reime as $result)
		{
		    $Date=$result->RDATE;
			$temp = (array)$result;  
			$temp[''] = 'REIMB';
			$dataReime[]= $temp;  
			$download_update=array('DOWNLOADED'=> 'Y','DOWNLOADED_DATE'=>date('Y-m-d'));
			DB::table('reime')->where('RDATE',$Date)->update($download_update);
		}
		 if(count($reime) >0 ){
        foreach($dataReime as &$outputItem) {
			$arrayOfElementsToMove = ["" => $outputItem[""]];
			unset($outputItem[""]);
   			$insertionPosition = array_search("DSCR", array_keys($outputItem));
			$insertionPosition++;
			$outputItem = array_slice($outputItem, 0, $insertionPosition, true) +
            $arrayOfElementsToMove +
            array_slice($outputItem, $insertionPosition, NULL, true);
        }
       $filelreime = 'reime_'.date('m-d-y_h_i_s');
		Excel::create($filelreime, function($excel) use ($dataReime) {
			$excel->sheet('mySheet', function($sheet) use ($dataReime)
			{
				 $sheet->fromArray($dataReime,null,'A1',true,false);
			});
		})->store('txt', storage_path('/var/labor'));
		$process = new Process('/usr/src/myvpaz/upload_files.sh'.' '.$filelreime.'.txt >>/var/mysqldump/export_reime.log');
			try {
				$process->run();
				if ($process->isSuccessful()) {
					session::flash('exportReime', 'Export D2E reimbursement downloaded successfully with '.count($reims).' records');
					return redirect('ImportExport')->with('lastrunReims', date('m/d/y H:i:s'));
				}
				else { 
					session::flash('errexportReims', 'Export reimbursement not downloaded');
					return redirect('ImportExport')->with('lastrunReims', date('m/d/y H:i:s'));
				}
			}
			catch (ProcessFailedException $e) {
				echo $e->getMessage();
			}
		}
		else
		{
			session::flash('errexportReims', 'There were 0 records to download');
			return redirect('ImportExport');
		}
	}
}
