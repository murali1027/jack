<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\WeeklyReportD2S;
use Illuminate\Support\Facades\Input;
use View;
use PDF;
use App\Emps;
use yajra\Datatables\Datatables;

class WeeklyReportControllerD2S extends Controller {


    public function __construct(WeeklyReportD2S $WeeklyReportD2S)
    {
        $this->WeeklyReportD2S = $WeeklyReportD2S;
	}

	public function getIndex()
    {
        return view('weeklyreportD2S.index');
    }

    public function getData()
    {
    	$auth = new Emps();
		$empid= $auth->empshow();
        return Datatables::of($empid)->addColumn('check', '<input type="checkbox" class="chkbox" name="selected_users[]" value="{{$EmpsID}}">')->make(true);
    }


    public function ReportD2S(Request $request)
    {
    	$WeeklyReportD2S=$this->WeeklyReportD2S;
    	$this->validate($request, ['startdate' => 'required','enddate' => 'required' ]);
        $empsid=explode(",",Input::get('selectedval'));
        $emp_id=array_filter($empsid);//dd(count($emp_id));
        if(count($emp_id) == 0){
        	return redirect()->back()->with('message', 'Please select the employee');
        }
        $startdate=date("Y-m-d", strtotime(Input::get('startdate')));
	    $enddate=date("Y-m-d", strtotime(Input::get('enddate')));

        $ename=EMPS::whereIn('EmpsID',$emp_id)->select('EmpsID')->get();
        $TimesReport = $WeeklyReportD2S->TimesReport($emp_id,$startdate,$enddate);
	    $ReimsReport = $WeeklyReportD2S->ReimsReport($emp_id,$startdate,$enddate);
       
        $data = [
		    'ename' => $ename,
		    'startdate'=>Input::get('startdate'),
		    'enddate'=>Input::get('enddate'),
		    'timesheet'=>$TimesReport,
			'reims'=>$ReimsReport,
		];
		//return View::make('weeklyreportD2S/pdf',$data);
		return PDF::loadView('weeklyreportD2S/pdf' ,$data)->stream();
		//->download("report.pdf");
    }
}
