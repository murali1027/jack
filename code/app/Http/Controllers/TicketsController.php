<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Ticket;
use Session;


class TicketsController extends Controller {

	public function __construct()
	{
		$this->middleware('admin', ['only' => ['index','edit']]);
		$this->user_id=Auth::User()->USER_ID;
	}

	public function index()
	{
		$ticket=TICKET::orderBy('TICKET_ID', 'desc')->get();
        return view('tickets.index',compact('ticket'));
	}

    public function create()
	{   
		return view('tickets.create');
	}

	public function store(Request $request)
	{
		$this->validate($request, [
					   'description'  =>'required',
					   'category'         =>'required'
			          ]);
		if($request->hasFile('file'))
		{
			$file = $request->file('file');
			$destinationPath = 'uploads'; // upload path
			$extension = Input::file('file')->getClientOriginalExtension(); // getting file extension
			$fileName= rand(11111, 99999) . '.' . $extension; // renameing image
			$upload_success = Input::file('file')->move($destinationPath, $fileName); // uploading file to given path
		}
		else{
			$fileName=NULL;
		}
      	TICKET::create(array(
		'CAT_ID'=>Input::get('category'),
		'PROBLEM'=>Input::get('description'),
		'ATTACH'=>$fileName,
	    'USER_ID'=> $this->user_id,
		'OPENED_BY' =>$this->user_id,
		'TSTATUS' =>1
		));
		session::flash('status', 'Ticket Added successfully');
	   return redirect('tickets/create');
	}

	public function show($id)
	{
		$ticket=TICKET::where('OPENED_BY',$this->user_id)->orderBy('TICKET_ID', 'desc')->get();
        return view('tickets.show',compact('ticket'));
	}

	
	public function edit($id)
	{
		$tickets=TICKET::find($id);
        return view('tickets.edit',compact('tickets'));
	}

	
	public function update($id)
	{
		$open_date = date('Y-m-d');
		$ticketUpdate=(array(
		'SOLUTION'=>Input::get('SOLUTION'),
		'TSTATUS'    => 2,
		'DATE_OPENED'=>$open_date,
		'CLOSED_BY'	 =>$this->user_id
		));
	   $ticket=TICKET::find($id);
	   $ticket->update($ticketUpdate);
	   session::flash('status', 'Ticket solution updated successfully');
	   return redirect('tickets');
	}

	
	public function destroy($id)
	{
		//
	}

}
