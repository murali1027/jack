<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Classes\ReimsDsquare;
use Illuminate\Support\Facades\Input;

class ApprovereimsControllerD2S extends Controller {

	public function __construct(ReimsDsquare $reims)
	{
		$this->middleware('approver');
		$this->ReimsDsquare=$reims;
	}

	public function index()
	{
		return $this->ReimsDsquare->ApproveReimsPage();
	}

	// Approve Reims D2S//
    public function ApproveReims()
	{
		return $this->ReimsDsquare->ApproveReims();
	}

	//Reject Reims D2S//
	public function RejectReims()
	{
		return $this->ReimsDsquare->RejectReims();
	}
     // Approve All//
    public function ApproveReimsallD2S()
	{
		return $this->ReimsDsquare->ApproveallReims();
	}
	
    // Attachment //
    public function textattachement()
    {
    	header('Content-Type: image/png');
    	$imagepath = env('UPLOAD_REIMS');
		$file=Input::get('file');
		echo file_get_contents("../../../../..".$imagepath.'/' . $file);
    }
}
