<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Classes\ReimsDsquare;


class ReimsControllerD2S extends Controller {

	public function __construct(ReimsDsquare $reims)
	{
		$this->middleware('user', ['only' => ['create']]);
		$this->ReimsDsquare=$reims;                
	}

	public function index()
	{
		return $this->ReimsDsquare->getViewreims();
	}

    /* Enter View Reimbursement*/
	public function create()
	{
		return $this->ReimsDsquare->formReims();
	}

    /* Store Reimbursement*/
	public function store(Request $request)
    {
        return $this->ReimsDsquare->postReims($request);
	}
	
	public function edit($id)
	{
		return $this->ReimsDsquare->EditReims($id);
	}

	public function update($id, Request $request)
	{
       return $this->ReimsDsquare->UpdateReims($id,$request);
    }

	public function destroy($id)
	{
		return $this->ReimsDsquare->DeleteReims($id);
	}
	
    public function enterReims()
	{
		return $this->ReimsDsquare->enterReims();
	}
	
    public function EmpReims()
	{
		return $this->ReimsDsquare->EmpReims();
	}

}
