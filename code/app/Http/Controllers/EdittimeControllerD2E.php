<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Classes\Delectric;

class EdittimeControllerD2E extends Controller {

	public function __construct(Delectric $delectric)
	{
		$this->Delectric=$delectric;
	}
	
	public function index()
	{
		return $this->Delectric->EditView();
	}

	//Edit Timesheet//
	public function edit($id)
	{
		return $this->Delectric->editTimesheet($id);
	}

	//Update Timesheet//
	public function update($id)
	{
		return $this->Delectric->updateTimesheet($id);
	}

	//Delete Timesheet//
	public function DeleteD2E()
	{
		 return $this->Delectric->deleteTimesheet();
	}
	
   // DropDown select Empe Timesheet Details//
   public function EditTimesheetD2E()
   {
	   return $this->Delectric->EditViewTimesheet();
   }
}
