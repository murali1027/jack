<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Uclass extends Model {

	protected $table = 'uclass';
 	
 	protected $primaryKey = 'UCLASS';	
 	
 	protected $fillable = ['UCLASS'];

}
