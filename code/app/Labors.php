<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Labors extends Model {

	protected $table = 'labors';
	
	protected $primaryKey = 'LABORS_ID';	
	
	protected $fillable = ['JobID','CostCode','EmpsID','EXTRA','ULOCAL','UCLASS','CERTS','DATE_WORK', 'SHIFT','HOURS','CREATED','CREATED_BY','MODIFIED','MODIFIED_BY','DELETED','APPROVED','APPROVED_BY','DOWNLOADED','DOWNLOADED_DATE'];

	
}
