<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Taxe extends Model {

	protected $table = 'taxe';
		
		protected $fillable = ['EmpeID', 'CHK_DATE','CAT', 'WAGE','TAX'];
}
