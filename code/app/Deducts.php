<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Deducts extends Model {

	    protected $table = 'deducts';
		
		protected $fillable = ['EmpsID', 'CHK_DATE','CAT', 'BASIS','AMOUNT'];

}
