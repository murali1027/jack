<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Taxs extends Model {

		protected $table = 'taxs';
		
		protected $fillable = ['EmpsID', 'CHK_DATE','CAT', 'WAGE','TAX'];

}
