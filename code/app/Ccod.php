<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Ccod extends Model {

	protected $table = 'ccod';
	
	protected $fillable = ['JobID','Extra', 'CostCode','DSCR', 'LBREST','MATEST', 'EQPEST','OVHEST', 'OTHEST','SUBEST'];

}
