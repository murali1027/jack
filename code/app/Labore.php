<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Labore extends Model {

	protected $table = 'labore';
	
	protected $primaryKey = 'LABORE_ID';	
	
	protected $fillable = ['JobID','CostCode','EmpeID','EXTRA','ULOCAL','UCLASS','CERTE','DATE_WORK','SHIFT','HOURS','CREATED','CREATED_BY','MODIFIED','MODIFIED_BY','DELETED','APPROVED','APPROVED_BY','DOWNLOADED','DOWNLOADED_DATE'];

}
