<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Emps extends Model {

	protected $table = 'emps';
	
	protected $primaryKey = 'EmpsID';	
	
	protected $fillable = ['ENAME','EMAIL', 'ULOCAL','UCLASS', 'CERT','HIRE_DATE', 'REHIRE_DATE','TERM_DATE', 'EPAY','JOB_DEFAULT', 'PAYGRP','APVTYPE', 'MANAGER','PTO','PEMAIL'];


	public function empshow()
	{
        return EMPS::whereRaw('TERM_DATE < REHIRE_DATE')->orWhere('TERM_DATE','=',NULL)
	         ->orderBy('ENAME', 'ASC')->get();
    }
	
}
