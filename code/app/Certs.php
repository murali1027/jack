<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Certs extends Model {

	protected $table = 'certs';
	
	protected $fillable = ['JobID','CERTS'];

}
