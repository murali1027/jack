<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Job extends Model {

	protected $table = 'job';
	
	protected $primaryKey = 'JobID';	
	
	protected $fillable = ['JNAME','JSTATUS', 'MANAGER','PAYROLL','APPROVER'];

	public function jobs(){
		return DB::table('job')->leftJoin('ccod', 'job.JobID', '=', 'ccod.JobID')->where('ccod.LBREST', '>', 0)
			      ->where('job.JSTATUS','!=', 'Closed')->select('job.JobID as JobID', 'job.JNAME as JNAME',
			      	DB::raw('CONCAT(job.JobID, " ",ccod.EXTRA)  JOB'), DB::raw('CONCAT(ccod.EXTRA)  EXTRA') )->distinct()->get();
	}

}
