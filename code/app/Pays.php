<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Pays extends Model {

	protected $table = 'pays';
	
	protected $fillable = ['EmpsID','CHK_DATE','CAT','UNITS', 'AMOUNT'];
}
