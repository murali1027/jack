<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('user_id');
			$table->string('username');
			$table->string('empid')->default('NULL');
			$table->string('fname');
			$table->string('lname');
			$table->string('domain')->default('NULL');
			$table->string('role')->default('NULL');
			$table->string('confirm')->default('Y');
			$table->string('active')->default('Y');
			$table->string('password', 60)->default('NULL');
			$table->string('deleted')->default('N');
			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
