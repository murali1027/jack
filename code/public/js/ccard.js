$(function(){
	$("#ccard").validate({
		rules: {
			RDATE: "required",
			ccard:"required",
			jobid:"required",
			ccode:"required",
		    cat:"required",
			LOC:"required",
			DSCR:"required",
			AMOUNT:"required"
		},
		messages: {
				RDATE: "Please select the date",
				ccard: "Please select the credit card",
				jobid: "Please select the jobID",
				ccode: "Please select the costCode",
				cat: "Please select the category",
				LOC: "Please enter the location",
				DSCR: "Please enter the description",
		    	AMOUNT: "Please enter the amount"
		    },
		submitHandler: function(form)
		{
			form.submit();
		}
	});

});
function validateFloatKeyPress(element, evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    var number = element.value.split('.');
   
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    //just one dot
    if(number.length>1 && charCode == 46){
         return false;
    }
    //get the carat position
    var caratPos = getSelectionStart(element);
    var dotPos = element.value.indexOf(".");
    if( caratPos > dotPos && dotPos>-1 && (number[1].length > 1)){
        return false;
    }
    return true;
}


function getSelectionStart(o) {
	if (o.createTextRange) {
		var r = document.selection.createRange().duplicate()
		r.moveEnd('character', o.value.length)
		if (r.text == '') return o.value.length
		return o.value.lastIndexOf(r.text)
	} else return o.selectionStart
}
