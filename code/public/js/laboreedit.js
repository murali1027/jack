   //Default Edit Page ViewD2E//
	var role,
	elemente = document.getElementById('role');
	role = (elemente != null ? elemente.value : null);
	if(role == 'USR'){
		var domain= document.getElementById("domain").value;
		if(domain == 'E'){
			var pageD2E=document.getElementById("page").value;
			if(pageD2E == 'editlaborsD2E' )
			{
				$(function() {
					var dateValue = localStorage.getItem("dateValue");
					if(dateValue != null) {
						$("select[name=weeks]").val(dateValue);
						var weeks_start = $('#weeks option:selected').attr('data-id');
						var weeks_end= document.getElementById("weeks").value;
						jQuery.ajax({
							type: "POST",
							dataType: "json",
							url: "EditTimesheetD2E",
							data: {weeks_start:weeks_start,weeks_end:weeks_end},
							success:function(data){
								$('#clear').empty();
								$('.clear').html(data.edit);
								$('#norec').html(data.norec);
							},
							error:function (){ 
							}
						});
					}
					else{
						var weeks_start = $('#weeks option:selected').attr('data-id');
						var weeks_end= document.getElementById("weeks").value;
						jQuery.ajax({
							type: "POST",
							dataType: "json",
							url: "EditTimesheetD2E",
							data: {weeks_start:weeks_start,weeks_end:weeks_end},
							success:function(data){
								$('#clear').empty();
								$('.clear').html(data.edit);
								$('#norec').html(data.norec);
							},
							error:function (){ 
							}
						});
					}
					$("select[name=weeks]").on("change", function() {
						var local= localStorage.setItem("dateValue", $(this).val());
						var weeks_start = $('#weeks option:selected').attr('data-id');
						var weeks_end= document.getElementById("weeks").value;
						jQuery.ajax({
							type: "POST",
							dataType: "json",
							url: "EditTimesheetD2E",
							data: {weeks_start:weeks_start,weeks_end:weeks_end},
							success:function(data){
								$('#clear').empty();
								$('.clear').html(data.edit);
								$('#norec').html(data.norec);
							},
							error:function (){ 
							}
						});
					});
				})
			}
		}
	}
	
	