 $(document).on('click', '.strclr', function () {
        localStorage.clear();
});
    var role,
	element = document.getElementById('role');
	role = (element != null ? element.value : null);
	if(role != 'USR')
	{    
		var approver,
		page = document.getElementById('page');
		approver = (page != null ? page.value : null);
		if(approver == 'editlaborsD2S')
		{
			$(function() {
				var enameValue = localStorage.getItem("enameValue");
				if(enameValue != null){
					$("select[name=ename]").val(enameValue);
					var empid= document.getElementById("ename").value; 
					var weeks_start = $('#weeks option:selected').attr('data-id');
					var weeks_end= document.getElementById("weeks").value;
					jQuery.ajax({
						type: "POST",
						dataType: "json",
						url: "EditTimesheetD2S",
						data: {empid:empid,weeks_start:weeks_start,weeks_end:weeks_end},
						success:function(data){
							$('#clear').empty();
							$('.clear').html(data.edit);
							$('#norec').html(data.norec);
						},
						error:function (){ }
					});
				}
				var dateValue = localStorage.getItem("dateValue");
				if(dateValue != null) {
					$("select[name=weeks]").val(dateValue);
					var empid= document.getElementById("ename").value; 
					var weeks_start = $('#weeks option:selected').attr('data-id');
					var weeks_end= document.getElementById("weeks").value;
					jQuery.ajax({
						type: "POST",
						dataType: "json",
						url: "EditTimesheetD2S",
						data: {empid:empid,weeks_start:weeks_start,weeks_end:weeks_end},
						success:function(data){
							$('#clear').empty();
							$('.clear').html(data.edit);
							$('#norec').html(data.norec);
						},
						error:function (){ 
						}
						});
				}

				$("select[name=ename]").on("change", function() {
					localStorage.setItem("enameValue", $(this).val());
					var empid= document.getElementById("ename").value; 
					var weeks_start = $('#weeks option:selected').attr('data-id');
					var weeks_end= document.getElementById("weeks").value;
					jQuery.ajax({
						type: "POST",
						dataType: "json",
						url: "EditTimesheetD2S",
						data: {empid:empid,weeks_start:weeks_start,weeks_end:weeks_end},
						success:function(data){
							$('#clear').empty();
							$('.clear').html(data.edit);
							$('#norec').html(data.norec);
						},
						error:function (){ }
					});
				});
				$("select[name=weeks]").on("change", function() {
					localStorage.setItem("dateValue", $(this).val());
					var empid= document.getElementById("ename").value; 
					var weeks_start = $('#weeks option:selected').attr('data-id');
					var weeks_end= document.getElementById("weeks").value;
					jQuery.ajax({
						type: "POST",
						dataType: "json",
						url: "EditTimesheetD2S",
						data: {empid:empid,weeks_start:weeks_start,weeks_end:weeks_end},
						success:function(data){
							$('#clear').empty();
							$('.clear').html(data.edit);
							$('#norec').html(data.norec);
						},
						error:function (){ }
					});
				});
			})
		}
	}
	if(role != 'USR')
	{
		var approver,
		page = document.getElementById('page');
		approver = (page != null ? page.value : null);
		if(approver == 'editlaborsD2E')
		{ 
			$(function() {
				var enameValue = localStorage.getItem("enameValue");
				if(enameValue != null){
					$("select[name=enames]").val(enameValue);
					var empid= document.getElementById("ename").value; 
					var weeks_start = $('#weeks option:selected').attr('data-id');
					var weeks_end= document.getElementById("weeks").value;
					jQuery.ajax({
						type: "POST",
						dataType: "json",
						url: "EditTimesheetD2E",
						data: {empid:empid,weeks_start:weeks_start,weeks_end:weeks_end},
						success:function(data){
							$('#clear').empty();
							$('.clear').html(data.edit);
							$('#norec').html(data.norec);
						},
						error:function (){ 
						}
					});
				}
				var dateValue = localStorage.getItem("dateValue");
				if(dateValue != null)
				{
					$("select[name=weeks]").val(dateValue);
					var empid= document.getElementById("ename").value; 
					var weeks_start = $('#weeks option:selected').attr('data-id');
					var weeks_end= document.getElementById("weeks").value;
					jQuery.ajax({
						type: "POST",
						dataType: "json",
						url: "EditTimesheetD2E",
						data: {empid:empid,weeks_start:weeks_start,weeks_end:weeks_end},
						success:function(data){
							$('#clear').empty();
							$('.clear').html(data.edit);
							$('#norec').html(data.norec);
						},
						error:function (){ 
						}
					});
				}
				$("select[name=enames]").on("change", function() {
					localStorage.setItem("enameValue", $(this).val());
					var empid= document.getElementById("ename").value; 
					var weeks_start = $('#weeks option:selected').attr('data-id');
					var weeks_end= document.getElementById("weeks").value;
					jQuery.ajax({
						type: "POST",
						dataType: "json",
						url: "EditTimesheetD2E",
						data: {empid:empid,weeks_start:weeks_start,weeks_end:weeks_end},
						success:function(data){
							$('#clear').empty();
							$('.clear').html(data.edit);
							$('#norec').html(data.norec);
						},
						error:function (){ }
					});
				});
				$("select[name=weeks]").on("change", function() {
					localStorage.setItem("dateValue", $(this).val());
					var empid= document.getElementById("ename").value; 
					var weeks_start = $('#weeks option:selected').attr('data-id');
					var weeks_end= document.getElementById("weeks").value;
					jQuery.ajax({
						type: "POST",
						dataType: "json",
						url: "EditTimesheetD2E",
						data: {empid:empid,weeks_start:weeks_start,weeks_end:weeks_end},
						success:function(data){
							$('#clear').empty();
							$('.clear').html(data.edit);
							$('#norec').html(data.norec);
						},
						error:function (){ }
					});
				});
			})
	    }
	}