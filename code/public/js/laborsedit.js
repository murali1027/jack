//Default Edit Page ViewD2S//
	var role,
	elements = document.getElementById('role');
	role = (elements != null ? elements.value : null);
	if(role == 'USR'){
		var domain= document.getElementById("domain").value;
		if(domain == 'S'){
			var pageD2S=document.getElementById("page").value;
			if(pageD2S == 'editlaborsD2S' )
			{	
				$(function() {
					var dateValue = localStorage.getItem("dateValue");
					if(dateValue != null) {
						$("select[name=weeks]").val(dateValue);
						var weeks_start = $('#weeks option:selected').attr('data-id');
						var weeks_end= document.getElementById("weeks").value;
						jQuery.ajax({
							type: "POST",
							dataType: "json",
							url: "EditTimesheetD2S",
							data: {weeks_start:weeks_start,weeks_end:weeks_end},
							success:function(data){
								$('#clear').empty();
								$('.clear').html(data.edit);
								$('#norec').html(data.norec);
							},
							error:function (){ 
							}
						});
					}
					else{
						var weeks_start = $('#weeks option:selected').attr('data-id');
						var weeks_end= document.getElementById("weeks").value;
						jQuery.ajax({
							type: "POST",
							dataType: "json",
							url: "EditTimesheetD2S",
							data: {weeks_start:weeks_start,weeks_end:weeks_end},
							success:function(data){
								$('#clear').empty();
								$('.clear').html(data.edit);
								$('#norec').html(data.norec);
							},
							error:function (){ 
							}
						});
					}
					$("select[name=weeks]").on("change", function() {
						var local= localStorage.setItem("dateValue", $(this).val());
						var weeks_start = $('#weeks option:selected').attr('data-id');
						var weeks_end= document.getElementById("weeks").value;
						jQuery.ajax({
							type: "POST",
							dataType: "json",
							url: "EditTimesheetD2S",
							data: {weeks_start:weeks_start,weeks_end:weeks_end},
							success:function(data){
								$('#clear').empty();
								$('.clear').html(data.edit);
								$('#norec').html(data.norec);
							},
							error:function (){ 
							}
						});
					});
				})
			}
		}
	}
	
	