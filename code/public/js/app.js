function getMonday(date) {
		var endDate;
		var date = new Date(date);
		
		//Current Monday Start//
		var dates = new Date();
		var weekstart = dates.getDate() - dates.getDay() +1;    
		var weekend = weekstart + -1;       // end day is the first day + 6 
		var monday_dates = new Date(dates.setDate(weekstart));
		var sunday_dates = new Date(dates.setDate(weekend));
		var sun_date = [sunday_dates.getDate()];
		var sun_month = [sunday_dates.getMonth() + 1] ;
		var sun_year = [sunday_dates.getFullYear()];	
        var curr_sunday= sun_year + "-" + sun_month + "-" + sun_date; 
        var mon_date = [monday_dates.getDate()];
		var mon_month = [monday_dates.getMonth() + 1] ;
		var mon_year = [monday_dates.getFullYear()];	
        var curr_monday= mon_year + "-" + mon_month + "-" + mon_date; 
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		var today = yyyy+'-'+mm+'-'+dd;
		//Current Monday End//
		if (curr_sunday == today || curr_monday == today ) {
		}
		else{
			//this Sunday//
			var sunday_date = date.getDay(),
			diff = date.getDate() - sunday_date + (sunday_date == 0 ? 0:0); 
			sunday=new Date(date.setDate(diff)); 
			var csun_date = sunday.getDate();
			var csun_month = sunday.getMonth()+1;
			var csun_year = sunday.getFullYear();
			var this_sunday= csun_month + "/" + csun_date + "/" + csun_year;
			//this Saturdaty//
			var satday = date.getDay(),
			diff = date.getDate() - satday + (satday == 0 ? 6:6); 
			saturday=new Date(date.setDate(diff)); 
			var csat_date = saturday.getDate();
			var csat_month = saturday.getMonth()+1;
			var csat_year = saturday.getFullYear();
			var this_satday= csat_month + "/" + csat_date + "/" + csat_year;
			$("#weeks").append('<option data-id="'+this_sunday+'" value="'+this_satday+'">'+this_sunday+'&nbsp;'+'to'+'&nbsp;'+this_satday+ '</option>');
		}
		
        //Previous Sunday//		
		var previous_sunday = date.getDay(),
		diff = date.getDate() - previous_sunday + (previous_sunday == 0 ? -7:-7); 
		sunday=new Date(date.setDate(diff)); 
		var psun_date = sunday.getDate();
		var psun_month = sunday.getMonth()+1;
		var psun_year = sunday.getFullYear();
		var previous_sunday_date= psun_month + "/" + psun_date + "/" + psun_year;
		
		//Previous Saturdaty//
		var pre_satday = date.getDay(),
		diff = date.getDate() - pre_satday + (pre_satday == 0 ? 6:6); 
		saturday=new Date(date.setDate(diff)); 
		var psat_date = saturday.getDate();
		var psat_month = saturday.getMonth()+1;
		var psat_year = saturday.getFullYear();
		var previous_satday= psat_month + "/" + psat_date + "/" + psat_year;
		$("#weeks").append('<option data-id="'+previous_sunday_date+'" value="'+previous_satday+'">'+previous_sunday_date+'&nbsp;'+'to'+'&nbsp;'+previous_satday+ '</option>');
		//2nd Previous Sunday//
		var prebefore_sunday = date.getDay(),
		diff = date.getDate() - prebefore_sunday + (prebefore_sunday == 0 ? -7:-7); 
		sunday=new Date(date.setDate(diff)); 
		var bsun_date = sunday.getDate();
		var bsun_month = sunday.getMonth()+1;
		var bsun_year = sunday.getFullYear();
		var previous_date= bsun_month + "/" + bsun_date + "/" + bsun_year;
		//2nd Previous Saturdaty//
		var pre_satday = date.getDay(),
		diff = date.getDate() - pre_satday + (pre_satday == 0 ? 6:6); 
		saturday=new Date(date.setDate(diff)); 
		var bsat_date = saturday.getDate();
		var bsat_month = saturday.getMonth()+1;
		var bsat_year = saturday.getFullYear();
		var bsatday= bsat_month + "/" + bsat_date + "/" + bsat_year;
		$("#weeks").append('<option data-id="'+previous_date+'" value="'+bsatday+'">'+previous_date+'&nbsp;'+'to'+'&nbsp;'+bsatday+ '</option>');
		//2nd Previous Sunday//
		var prethird_sunday = date.getDay(),
		diff = date.getDate() - prethird_sunday + (prethird_sunday == 0 ? -7:-7); 
		sunday=new Date(date.setDate(diff)); 
		var pbsun_date = sunday.getDate();
		var pbsun_month = sunday.getMonth()+1;
		var pbsun_year = sunday.getFullYear();
		var previous_thirddate= pbsun_month + "/" + pbsun_date + "/" + pbsun_year;
		//2nd Previous Saturdaty//
		var prethird_satday = date.getDay(),
		diff = date.getDate() - prethird_satday + (prethird_satday == 0 ? 6:6); 
		saturday=new Date(date.setDate(diff)); 
		var pbsat_date = saturday.getDate();
		var pbsat_month = saturday.getMonth()+1;
		var pbsat_year = saturday.getFullYear();
		var pbsatday= pbsat_month + "/" + pbsat_date + "/" + pbsat_year;
		$("#weeks").append('<option data-id="'+previous_thirddate+'" value="'+pbsatday+'">'+previous_thirddate+'&nbsp;'+'to'+'&nbsp;'+pbsatday+ '</option>');
	}
	var previous=getMonday(new Date());


$(function()
	{
		$("#edittimesheet-form").validate({
		rules: {
            jobid: "required",
			ccode: "required",
			shift: "required",
			hours:{
					required:true,
					number:true,
					range:[0.25, 16]                                                                                           
				},
		},
        messages:
		{
            jobid: "Please select the JobID",
		    ccode: "Please select the CostCode",
		    shift: "Please select the Shifts"
		},
        submitHandler: function(form)
		{
			var hours=document.getElementById("hours").value;
			var time= hours % 0.25;
			if(time != 0 )
			{
			   document.getElementById("msg").innerHTML ="<span class='error'>You must enter an amount and between 0.25 and 16 hours in 0.25 increments</span>";
			}
			else
			{
				form.submit();
			}
		}
		});
	});	
	$(window).one("focus", function() {
    localStorage.clear();
});
	
    //Approve Timesheet D2S//
	function ApproveEmpDetailD2S()
	{
		var empid= document.getElementById("ename").value; 
		if(empid == ''){
			var emp_value="postApproveTimesheetD2S";
		}
		else {
			var emp_value="ApproveTimesheetD2S";
		}
		var empname = $('#ename option:selected').attr('data-id');
		var weeks_start = $('#weeks option:selected').attr('data-id');
		var weeks_end= document.getElementById("weeks").value;
		jQuery.ajax({
			type: "POST",
			dataType: "json",
			url: emp_value,
			data: {empid:empid,weeks_start:weeks_start,weeks_end:weeks_end},
			success:function(data){
				$('#clear').empty();
				$('.clear').html(data.edit);
				$('#action').html(data.action);
				$('#norec').html(data.norec);
				$('[data-toggle="tooltip"]').tooltip();
			},
			error:function (){ 
			}
			});
			
	}
	
	//Approve Timesheet D2E//
	function ApproveEmpDetailD2E()
	{
		var empid= document.getElementById("ename").value;
		if(empid == ''){
			var emp_value="postApproveTimesheetD2E";
		}
		else {
			var emp_value="ApproveTimesheetD2E";
		}
		var empname = $('#ename option:selected').attr('data-id');
		var weeks_start = $('#weeks option:selected').attr('data-id');
		var weeks_end= document.getElementById("weeks").value;
		jQuery.ajax({
			type: "POST",
			dataType: "json",
			url: emp_value,
			data: {empid:empid,weeks_start:weeks_start,weeks_end:weeks_end},
			success:function(data){
				$('#clear').empty();
				$('.clear').html(data.edit);
				$('#action').html(data.action);
				$('#norec').html(data.norec);
				$('[data-toggle="tooltip"]').tooltip();
			},
			error:function (){ 
			}
			});
	}
	
	

		
	