
$(function() {
	var endDate;
 $("#date").datepicker({
        dateFormat: " mm/dd/yy", 
		minDate:  -28,
        maxDate:7,
        onSelect: function(date,year,month){            
            var firstdate = $('#date').datepicker('getDate'); 
			var date = new Date( Date.parse( firstdate ) ); 
			/*Sunday*/
			startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
			var sunday = [startDate.getDate()];
			var smonth = [startDate.getMonth() + 1] ;
			var syear = [startDate.getFullYear()];
			var sunday_date= syear + '-' + smonth + '-' + sunday;
			document.getElementById("sunDate").value = sunday_date;
			document.getElementById("sunday").innerHTML = sunday;
			/*Monday*/
			monDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 1 );
			var monday = [monDate.getDate()];
			var mmonth = [monDate.getMonth() + 1] ;
			var myear = [monDate.getFullYear()];
			var monday_date= myear + '-' + mmonth + '-' + monday;
			document.getElementById("monDate").value = monday_date;
			document.getElementById("monday").innerHTML = monday;
			/*Tuesday*/
			tueDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 2);
			var tuesday = [tueDate.getDate()];
			var tmonth = [tueDate.getMonth() + 1] ;
			var tyear = [tueDate.getFullYear()];
			var tuesday_date= tyear + '-' + tmonth + '-' + tuesday;
			document.getElementById("tueDate").value = tuesday_date;
			document.getElementById("tuesday").innerHTML = tuesday;
			/*Wednesday*/
			wedDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 3);
			var wednesday = [wedDate.getDate()];
			var wmonth = [wedDate.getMonth() + 1] ;
			var wyear = [wedDate.getFullYear()];
			var wednesday_date= wyear + '-' + wmonth + '-' + wednesday;
			document.getElementById("wedDate").value = wednesday_date;
			document.getElementById("wednesday").innerHTML = wednesday;
			/*Thursday*/
			thuDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 4);
			var thursday = [thuDate.getDate()];
			var thmonth = [thuDate.getMonth() + 1] ;
			var thyear = [thuDate.getFullYear()];
			var thursday_date= thyear + '-' + thmonth + '-' + thursday;
			document.getElementById("thuDate").value = thursday_date;
			document.getElementById("thursday").innerHTML = thursday;
			/*friday*/
			friDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 5);
			var friday = [friDate.getDate()];
			var fmonth = [friDate.getMonth() + 1] ;
			var fyear = [friDate.getFullYear()];
			var friday_date= fyear + '-' + fmonth + '-' + friday;
			document.getElementById("friDate").value = friday_date;
			document.getElementById("friday").innerHTML = friday;
			/*saturday*/
			endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
		    var saturday = [endDate.getDate()];
			var samonth = [endDate.getMonth() + 1] ;
			var sayear = [endDate.getFullYear()];
			var saturday_date= sayear + '-' + samonth + '-' + saturday;
			document.getElementById("satDate").value = saturday_date;
			document.getElementById("saturday").innerHTML =saturday;
			
			var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
		    var week_month = months[endDate.getMonth()] ;
		    var week_year = [endDate.getFullYear()];
		    document.getElementById("endDate").innerHTML =week_month +' '+ saturday + ',' + ' ' + week_year;
			var date_no = [date.getDate()];
			var month = [date.getMonth() + 1] ;
			var year = [date.getFullYear()];
			var result= year + '-' + month + '-' + date_no;
			
			date.setDate( date.getDate() + 1 );        
			var newDate = date.toDateString(); 
			newDate = new Date( Date.parse( newDate ) );                      
		
			$.ajax({
				   type: "POST",
				   url: "getalldates",
				   dataType: "json",
				   data: {sunday:sunday_date,monday:monday_date,tuesday:tuesday_date,wedday:wednesday_date,thuday:thursday_date,
				   friday:friday_date,satday:saturday_date},
				   success:function(data)
					{   
						$('#sun').html(data.sun);
						$('#mon').html(data.mon);
						$('#tue').html(data.tue);
						$('#wed').html(data.wed);
						$('#thu').html(data.thu);
						$('#fri').html(data.fri);
						$('#sat').html(data.sat);
						$('#total').html(data.total);
					},
					error: function (){ }
				});
					
				}
			});
	});
  
     
	$(window).load(function()
	{ 
		function getMonday(date) {
		var endDate;
		
		// previous Monday Start//
		var date = new Date(date); 
		var day = date.getDay(),
		diff = date.getDate() - day + (day == 0 ? -6:-6); 
		monday=new Date(date.setDate(diff));
		var curr_date = monday.getDate();
		var curr_month = monday.getMonth()+1;
		var curr_year = monday.getFullYear();
		var previous_date= curr_month + "-" + curr_date + "-" + curr_year;
		// previous Monday End//
		
		//Current Monday Start//
		var dates = new Date();
		var weekstart = dates.getDate() - dates.getDay() +1;    
		var weekend = weekstart + -1;       // end day is the first day + 6 
		var monday_dates = new Date(dates.setDate(weekstart));
		var sunday_dates = new Date(dates.setDate(weekend));
		var sun_date = [sunday_dates.getDate()];
		var sun_month = [sunday_dates.getMonth() + 1] ;
		var sun_year = [sunday_dates.getFullYear()];	
        var curr_sunday= sun_year + "-" + sun_month + "-" + sun_date;
        var mon_date = [monday_dates.getDate()];
		var mon_month = [monday_dates.getMonth() + 1] ;
		var mon_year = [monday_dates.getFullYear()];	
        var curr_monday= mon_year + "-" + mon_month + "-" + mon_date; 
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		var today = yyyy+'-'+mm+'-'+dd;
		var current_date = new Date(); 
		//Current Monday End//
		if (curr_sunday == today || curr_monday == today ) {
			$("#date").datepicker("setDate", new Date(date));
			/*sunday*/
		startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
		var sunday = [startDate.getDate()];
		var smonth = [startDate.getMonth() + 1] ;
		var syear = [startDate.getFullYear()];
		var sunday_date= syear + '-' + smonth + '-' + sunday;
		/*Monday*/
		monDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 1 );
		var monday = [monDate.getDate()];
		var mmonth = [monDate.getMonth() + 1] ;
		var myear = [monDate.getFullYear()];
		var monday_date= myear + '-' + mmonth + '-' + monday;
		/*Tuesday*/
		tueDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 2);
		var tuesday = [tueDate.getDate()];
		var tmonth = [tueDate.getMonth() + 1] ;
		var tyear = [tueDate.getFullYear()];
		var tuesday_date= tyear + '-' + tmonth + '-' + tuesday;
		/*Wednesday*/
		wedDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 3);
		var wednesday = [wedDate.getDate()];
		var wmonth = [wedDate.getMonth() + 1] ;
		var wyear = [wedDate.getFullYear()];
		var wednesday_date= wyear + '-' + wmonth + '-' + wednesday;
		/*Thursday*/
		thuDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 4);
		var thursday = [thuDate.getDate()];
		var thmonth = [thuDate.getMonth() + 1] ;
		var thyear = [thuDate.getFullYear()];
		var thursday_date= thyear + '-' + thmonth + '-' + thursday;
		/*friday*/
		friDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 5);
		var friday = [friDate.getDate()];
		var fmonth = [friDate.getMonth() + 1] ;
		var fyear = [friDate.getFullYear()];
		var friday_date= fyear + '-' + fmonth + '-' + friday;
		/*saturday*/
		endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
		var saturday = [endDate.getDate()];
		var samonth = [endDate.getMonth() + 1] ;
		var sayear = [endDate.getFullYear()];
		var saturday_date= sayear + '-' + samonth + '-' + saturday;
		}
		else{
			$("#date").datepicker("setDate", new Date());
		/*sunday*/
		startDate = new Date(current_date.getFullYear(), current_date.getMonth(), current_date.getDate() - current_date.getDay());
		var sunday = [startDate.getDate()];
		var smonth = [startDate.getMonth() + 1] ;
		var syear = [startDate.getFullYear()];
		var sunday_date= syear + '-' + smonth + '-' + sunday;
		/*Monday*/
		monDate = new Date(current_date.getFullYear(), current_date.getMonth(), current_date.getDate() - current_date.getDay() + 1 );
		var monday = [monDate.getDate()];
		var mmonth = [monDate.getMonth() + 1] ;
		var myear = [monDate.getFullYear()];
		var monday_date= myear + '-' + mmonth + '-' + monday;
		/*Tuesday*/
		tueDate = new Date(current_date.getFullYear(), current_date.getMonth(), current_date.getDate() - current_date.getDay() + 2);
		var tuesday = [tueDate.getDate()];
		var tmonth = [tueDate.getMonth() + 1] ;
		var tyear = [tueDate.getFullYear()];
		var tuesday_date= tyear + '-' + tmonth + '-' + tuesday;
		/*Wednesday*/
		wedDate = new Date(current_date.getFullYear(), current_date.getMonth(), current_date.getDate() - current_date.getDay() + 3);
		var wednesday = [wedDate.getDate()];
		var wmonth = [wedDate.getMonth() + 1] ;
		var wyear = [wedDate.getFullYear()];
		var wednesday_date= wyear + '-' + wmonth + '-' + wednesday;
		/*Thursday*/
		thuDate = new Date(current_date.getFullYear(), current_date.getMonth(), current_date.getDate() - current_date.getDay() + 4);
		var thursday = [thuDate.getDate()];
		var thmonth = [thuDate.getMonth() + 1] ;
		var thyear = [thuDate.getFullYear()];
		var thursday_date= thyear + '-' + thmonth + '-' + thursday;
		/*friday*/
		friDate = new Date(current_date.getFullYear(), current_date.getMonth(), current_date.getDate() - current_date.getDay() + 5);
		var friday = [friDate.getDate()];
		var fmonth = [friDate.getMonth() + 1] ;
		var fyear = [friDate.getFullYear()];
		var friday_date= fyear + '-' + fmonth + '-' + friday;
		/*saturday*/
		endDate = new Date(current_date.getFullYear(), current_date.getMonth(), current_date.getDate() - current_date.getDay() + 6);
		var saturday = [endDate.getDate()];
		var samonth = [endDate.getMonth() + 1] ;
		var sayear = [endDate.getFullYear()];
		var saturday_date= sayear + '-' + samonth + '-' + saturday;
		}
		document.getElementById("sunDate").value = sunday_date;
		document.getElementById("sunday").innerHTML =sunday;
		document.getElementById("sundays").innerHTML =sunday;
		
		document.getElementById("monDate").value = monday_date;
		document.getElementById("monday").innerHTML =monday;
		document.getElementById("mondays").innerHTML =monday;
		
		document.getElementById("tueDate").value = tuesday_date;
		document.getElementById("tuesday").innerHTML =tuesday;
		document.getElementById("tuesdays").innerHTML =tuesday;
		
		document.getElementById("wedDate").value = wednesday_date;
		document.getElementById("wednesday").innerHTML =wednesday;
		document.getElementById("wednesdays").innerHTML =wednesday;
		
		document.getElementById("thuDate").value = thursday_date;
		document.getElementById("thursday").innerHTML =thursday;
		document.getElementById("thursdays").innerHTML =thursday;
		
		document.getElementById("friDate").value = friday_date;
		document.getElementById("friday").innerHTML =friday;
		document.getElementById("fridays").innerHTML =friday;
		
		document.getElementById("satDate").value = saturday_date;
		document.getElementById("saturday").innerHTML =saturday;
		document.getElementById("saturdays").innerHTML =saturday;
		
		var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
		var week_month = months[endDate.getMonth()] ;
		var week_year = [endDate.getFullYear()];
		document.getElementById("endDate").innerHTML =week_month +' '+ saturday + ',' + ' ' + week_year;
		$.ajax({
				type: "POST",
				url: "getalldates",
				dataType: "json",
				data: { sunday:sunday_date, monday:monday_date, tuesday:tuesday_date, wedday:wednesday_date, thuday:thursday_date,friday:friday_date,
				satday:saturday_date},
				success:function(data)
				{
					$('#sun').html(data.sun);
					$('#mon').html(data.mon);
					$('#tue').html(data.tue);
					$('#wed').html(data.wed);
					$('#thu').html(data.thu);
					$('#fri').html(data.fri);
					$('#sat').html(data.sat);
					$('#total').html(data.total);
				},
				error: function (){ }
			});
		}
		
		  var previous=getMonday(new Date());
	});

   //View Emp Detail Admin Role
	function ViewEmpDetail()
	{
		var ename= document.getElementById("ename").value;
		var sundate=document.getElementById("sunDate").value;
		var mondate=document.getElementById("monDate").value;
		var tuedate=document.getElementById("tueDate").value;
		var weddate=document.getElementById("wedDate").value;
		var thudate=document.getElementById("thuDate").value;
		var fridate=document.getElementById("friDate").value;
		var satdate=document.getElementById("satDate").value;
	    jQuery.ajax({
			type: "POST",
			dataType: "json",
			url: "EmpDetails",
			data: {ename:ename},
			success:function(data){
				$('#clear').empty();
				$('.clear').html(data.html);
				$.ajax({
					type: "POST",
					url: "getalldates",
					dataType: "json",
					data: {ename:ename, sunday:sundate,monday:mondate,tuesday:tuedate,wedday:weddate,thuday:thudate,
					friday:fridate,satday:satdate},
					success:function(data)
					{   
						$('#sun').html(data.sun);
						$('#mon').html(data.mon);
						$('#tue').html(data.tue);
						$('#wed').html(data.wed);
						$('#thu').html(data.thu);
						$('#fri').html(data.fri);
						$('#sat').html(data.sat);
						$('#total').html(data.total);
					},
					error: function (){ }
				});
				
			},
			error:function (){ 
			}
			});
	}
	
	//View Emp Detail from Admin D2E
	function ViewEmpDetailD2E()
	{
		var ename= document.getElementById("ename").value;
	    var sundate=document.getElementById("sunDate").value;
		var mondate=document.getElementById("monDate").value;
		var tuedate=document.getElementById("tueDate").value;
		var weddate=document.getElementById("wedDate").value;
		var thudate=document.getElementById("thuDate").value;
		var fridate=document.getElementById("friDate").value;
		var satdate=document.getElementById("satDate").value;
		
	    jQuery.ajax({
			type: "POST",
			dataType: "json",
			url: "EmpDetailsD2E",
			data: {ename:ename},
			success:function(data){
				$('#clear').empty();
				$('.clear').html(data.html);
				$.ajax({
					type: "POST",
					url: "getalldates",
					dataType: "json",
					data: {ename:ename, sunday:sundate,monday:mondate,tuesday:tuedate,wedday:weddate,thuday:thudate,
					friday:fridate,satday:satdate},
					success:function(data)
					{   
						$('#sun').html(data.sun);
						$('#mon').html(data.mon);
						$('#tue').html(data.tue);
						$('#wed').html(data.wed);
						$('#thu').html(data.thu);
						$('#fri').html(data.fri);
						$('#sat').html(data.sat);
						$('#total').html(data.total);
					},
					error: function (){ }
				});
				
			},
			error:function (){ 
			}
			});
	}

	/* Calenter Previous button*/
    function Prev()
    {
		var role= document.getElementById("role").value;
		if(role == 'USR'){}
		else
		{
				var ename= document.getElementById("ename").value;
		}
		var current_date = new Date();
		var firstday = new Date(current_date.setDate(current_date.getDate() - current_date.getDay()-21));
		var pre_sunday = [firstday.getDate()];
		var pmonth = [firstday.getMonth() + 1] ;
		var pyear = [firstday.getFullYear()];
		var pre_sunday_date= pmonth + '-' + pre_sunday + '-' + pyear;  
		
		var textbox_value=document.getElementById("date").value;
		var current_week = new Date(textbox_value);
		var previous_sunday = new Date(current_week.setDate(current_week.getDate() - current_week.getDay()+0));
		var sunday = [previous_sunday.getDate()];
		var smonth = [previous_sunday.getMonth() + 1] ;
		var syear = [previous_sunday.getFullYear()];
		var sunday_date= smonth + '-' + sunday + '-' + syear; 
		
		if(pre_sunday_date==sunday_date){}
		else
		{ 
			/*sunday*/		
			var previous_sunday = new Date(current_week.setDate(current_week.getDate() - current_week.getDay()+-7)); 
			var sunday = [previous_sunday.getDate()];
			var smonth = [previous_sunday.getMonth() + 1] ;
			var syear = [previous_sunday.getFullYear()];
			var sunday_date= syear + '-' + smonth + '-' + sunday;
			document.getElementById("sunDate").value = sunday_date;
			document.getElementById("sunday").innerHTML =sunday;
			document.getElementById("sundays").innerHTML =sunday;
			/*monday*/
			var previous_monday = new Date(current_week.setDate(current_week.getDate() - current_week.getDay()+1));
			var monday = [previous_monday.getDate()];
			var mmonth = [previous_monday.getMonth() + 1] ;
			var myear = [previous_monday.getFullYear()];
			var monday_date= myear + '-' + mmonth + '-' + monday;
			document.getElementById("monDate").value = monday_date;
			document.getElementById("monday").innerHTML =monday;
			document.getElementById("mondays").innerHTML =monday;
			/*Tuesday*/
			var previous_tuesday = new Date(current_week.setDate(current_week.getDate() - current_week.getDay()+2));
			var tuesday = [previous_tuesday.getDate()];
			var tmonth = [previous_tuesday.getMonth() + 1] ;
			var tyear = [previous_tuesday.getFullYear()];
			var tuesday_date= tyear + '-' + tmonth + '-' + tuesday;
			document.getElementById("tueDate").value = tuesday_date;
			document.getElementById("tuesday").innerHTML =tuesday;
			document.getElementById("tuesdays").innerHTML =tuesday;
			/*Wednesday*/
			var previous_wedday = new Date(current_week.setDate(current_week.getDate() - current_week.getDay()+3));
			var wednesday = [previous_wedday.getDate()];
			var wmonth = [previous_wedday.getMonth() + 1] ;
			var wyear = [previous_wedday.getFullYear()];
			var wednesday_date= wyear + '-' + wmonth + '-' + wednesday;
			document.getElementById("wedDate").value = wednesday_date;
			document.getElementById("wednesday").innerHTML =wednesday;
			document.getElementById("wednesdays").innerHTML =wednesday;
			/*Thursday*/
			var previous_thuday = new Date(current_week.setDate(current_week.getDate() - current_week.getDay()+4));	
			var thursday = [previous_thuday.getDate()];
			var thmonth = [previous_thuday.getMonth() + 1] ;
			var thyear = [previous_thuday.getFullYear()];
			var thursday_date= thyear + '-' + thmonth + '-' + thursday;
			document.getElementById("thuDate").value = thursday_date;
			document.getElementById("thursday").innerHTML =thursday;
			document.getElementById("thursdays").innerHTML =thursday;
			/*friday*/
			var previous_friday = new Date(current_week.setDate(current_week.getDate() - current_week.getDay()+5));
			var friday = [previous_friday.getDate()];
			var fmonth = [previous_friday.getMonth() + 1] ;
			var fyear = [previous_friday.getFullYear()];
			var friday_date= fyear + '-' + fmonth + '-' + friday;
			document.getElementById("friDate").value = friday_date;
			document.getElementById("friday").innerHTML =friday;
			document.getElementById("fridays").innerHTML =friday;
			/*saturday*/
			var previous_satday = new Date(current_week.setDate(current_week.getDate() - current_week.getDay()+6));
			var saturday = [previous_satday.getDate()];
			var samonth = [previous_satday.getMonth() + 1] ;
			var sayear = [previous_satday.getFullYear()];
			var saturday_date= sayear + '-' + samonth + '-' + saturday;
			document.getElementById("satDate").value = saturday_date;
			document.getElementById("saturday").innerHTML =saturday;
			document.getElementById("saturdays").innerHTML =saturday;
			$("#date").datepicker("setDate", new Date(previous_monday));
			
			var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
			var week_month = months[previous_satday.getMonth()] ;
			var week_year = [previous_satday.getFullYear()];
			document.getElementById("endDate").innerHTML =week_month +' '+ saturday + ',' + ' ' + week_year;
			$.ajax({
					type: "POST",
					url: "getalldates",
					dataType: "json",
					data: {ename:ename, sunday:sunday_date,monday:monday_date,tuesday:tuesday_date,wedday:wednesday_date,thuday:thursday_date,
					friday:friday_date,satday:saturday_date},
					success:function(data)
					{   
						$('#sun').html(data.sun);
						$('#mon').html(data.mon);
						$('#tue').html(data.tue);
						$('#wed').html(data.wed);
						$('#thu').html(data.thu);
						$('#fri').html(data.fri);
						$('#sat').html(data.sat);
						$('#total').html(data.total);
					},
					error: function (){ }
				});
		}
	}
	/* Calendar Next Button*/
	function Next()
    {  
		var role= document.getElementById("role").value;
		if(role == 'USR'){}
		else
		{
				var ename= document.getElementById("ename").value;
		}
		var current_date = new Date;
		var firstday = new Date(current_date.setDate(current_date.getDate() - current_date.getDay()));
		var lastday = new Date(current_date.setDate(current_date.getDate() - current_date.getDay()+13));
		var last_saturday = [lastday.getDate()];
		var lmonth = [lastday.getMonth() + 1] ;
		var lyear = [lastday.getFullYear()];
		var last_saturday_date= lmonth + '-' + last_saturday + '-' + lyear; 
		
		var textbox_value=document.getElementById("date").value;
		var current_week = new Date(textbox_value);
		var next_satday = new Date(current_week.setDate(current_week.getDate() - current_week.getDay()+6));
		var current_satday = [next_satday.getDate()];
		var cmonth = [next_satday.getMonth() + 1 ] ;
		var cyear = [next_satday.getFullYear()];
		var current_satday_date= cmonth + '-' + current_satday + '-' + cyear; 
		if(current_satday_date==last_saturday_date){}
		else
		{
			/*sunday*/
			var next_sunday = new Date(current_week.setDate(current_week.getDate() - current_week.getDay()+7));
			var sunday = [next_sunday.getDate()];
			var smonth = [next_sunday.getMonth() + 1] ;
			var syear = [next_sunday.getFullYear()];
			var sunday_date= syear + '-' + smonth + '-' + sunday;
			document.getElementById("sunDate").value = sunday_date;
			document.getElementById("sunday").innerHTML =sunday;
			document.getElementById("sundays").innerHTML =sunday;
			/*monday*/
			var next_monday = new Date(current_week.setDate(current_week.getDate() - current_week.getDay()+1));
			var monday = [next_monday.getDate()];
			var mmonth = [next_monday.getMonth() + 1] ;
			var myear = [next_monday.getFullYear()];
			var monday_date= myear + '-' + mmonth + '-' + monday;
			document.getElementById("monDate").value = monday_date;
			document.getElementById("monday").innerHTML =monday;
			document.getElementById("mondays").innerHTML =monday;
			/*Tuesday*/
			var next_tuesday = new Date(current_week.setDate(current_week.getDate() - current_week.getDay()+2));
			var tuesday = [next_tuesday.getDate()];
			var tmonth = [next_tuesday.getMonth() + 1] ;
			var tyear = [next_tuesday.getFullYear()];
			var tuesday_date= tyear + '-' + tmonth + '-' + tuesday;
			document.getElementById("tueDate").value = tuesday_date;
			document.getElementById("tuesday").innerHTML =tuesday;
			document.getElementById("tuesdays").innerHTML =tuesday;
			/*Wednesday*/
			var next_wedday = new Date(current_week.setDate(current_week.getDate() - current_week.getDay()+3));
			var wednesday = [next_wedday.getDate()];
			var wmonth = [next_wedday.getMonth() + 1] ;
			var wyear = [next_wedday.getFullYear()];
			var wednesday_date= wyear + '-' + wmonth + '-' + wednesday;
			document.getElementById("wedDate").value = wednesday_date;
			document.getElementById("wednesday").innerHTML =wednesday;
			document.getElementById("wednesdays").innerHTML =wednesday;
			/*Thursday*/
			var next_thuday = new Date(current_week.setDate(current_week.getDate() - current_week.getDay()+4));	
			var thursday = [next_thuday.getDate()];
			var thmonth = [next_thuday.getMonth() + 1] ;
			var thyear = [next_thuday.getFullYear()];
			var thursday_date= thyear + '-' + thmonth + '-' + thursday;
			document.getElementById("thuDate").value = thursday_date;
			document.getElementById("thursday").innerHTML =thursday;
			document.getElementById("thursdays").innerHTML =thursday;
			/*friday*/
			var next_friday = new Date(current_week.setDate(current_week.getDate() - current_week.getDay()+5));
			var friday = [next_friday.getDate()];
			var fmonth = [next_friday.getMonth() + 1] ;
			var fyear = [next_friday.getFullYear()];
			var friday_date= fyear + '-' + fmonth + '-' + friday;
			document.getElementById("friDate").value = friday_date;
			document.getElementById("friday").innerHTML =friday;
			document.getElementById("fridays").innerHTML =friday;
			/*saturday*/
			var next_satday = new Date(current_week.setDate(current_week.getDate() - current_week.getDay()+6));
			var saturday = [next_satday.getDate()];
			var samonth = [next_satday.getMonth() + 1] ;
			var sayear = [next_satday.getFullYear()];
			var saturday_date= sayear + '-' + samonth + '-' + saturday;
			document.getElementById("satDate").value = saturday_date;
			document.getElementById("saturday").innerHTML =saturday;
			document.getElementById("saturdays").innerHTML =saturday;
			$("#date").datepicker("setDate", new Date(next_monday));
			
			var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
			var week_month = months[next_satday.getMonth()] ;
			var week_year = [next_satday.getFullYear()];
			document.getElementById("endDate").innerHTML =week_month +' '+ saturday + ',' + ' ' + week_year;
		
			$.ajax({
					type: "POST",
					url: "getalldates",
					dataType: "json",
					data: {ename:ename,sunday:sunday_date,monday:monday_date,tuesday:tuesday_date,wedday:wednesday_date,thuday:thursday_date,
					friday:friday_date,satday:saturday_date},
					success:function(data)
					{   
						$('#sun').html(data.sun);
						$('#mon').html(data.mon);
						$('#tue').html(data.tue);
						$('#wed').html(data.wed);
						$('#thu').html(data.thu);
						$('#fri').html(data.fri);
						$('#sat').html(data.sat);
						$('#total').html(data.total);
					},
					error: function (){ }
				});
		}
	}
	
 //Validation for D2s //
    $(function()
	{
		$("#timesheet-form").validate({
		rules: {
            jobid: "required",
			ccode: "required",
			shift: "required",
			ename: "required"
		},
        messages:
		{
            jobid: "Please select the JobID",
		    ccode: "Please select the CostCode",
		    shift: "Please select the Shifts",
			ename: "Please select the employee"
		},
        submitHandler: function(form)
		{
			return false; 
		}
		});
		 $('#submit').on('click', function() {
		 
		    var sunhours=document.getElementById("sun_hours").value;
			var monhours=document.getElementById("mon_hours").value;
			var tuehours=document.getElementById("tue_hours").value;
			var wedhours=document.getElementById("wed_hours").value;
			var thuhours=document.getElementById("thu_hours").value;
			var frihours=document.getElementById("fri_hours").value;
			var sathours=document.getElementById("sat_hours").value;
			var suntime= sunhours % 0.25;
			var montime= monhours % 0.25;
			var tuetime= tuehours % 0.25;
			var wedtime= wedhours % 0.25;
			var thutime= thuhours % 0.25;
			var fritime= frihours % 0.25;
			var sattime= sathours % 0.25;
			if(suntime != 0 )
			{
			document.getElementById("msg").innerHTML ="<span class='error'>You must enter an amount and between 0.25 and 16 hours in 0.25 increments</span>";
			}
			else if(montime != 0 )
			{
			document.getElementById("msg").innerHTML ="<span class='error'>You must enter an amount and between 0.25 and 16 hours in 0.25 increments</span>";
			}
			else if(tuetime != 0 )
			{
			document.getElementById("msg").innerHTML ="<span class='error'>You must enter an amount and between 0.25 and 16 hours in 0.25 increments</span>";
			}
			else if(wedtime != 0 )
			{
			document.getElementById("msg").innerHTML ="<span class='error'>You must enter an amount and between 0.25 and 16 hours in 0.25 increments</span>";
			}
			else if(thutime != 0 )
			{
			document.getElementById("msg").innerHTML ="<span class='error'>You must enter an amount and between 0.25 and 16 hours in 0.25 increments</span>";
			}
			else if(fritime != 0 )
			{
			document.getElementById("msg").innerHTML ="<span class='error'>You must enter an amount and between 0.25 and 16 hours in 0.25 increments</span>";
			}
			else if(sattime != 0 )
			{
			document.getElementById("msg").innerHTML ="<span class='error'>You must enter an amount and between 0.25 and 16 hours in 0.25 increments</span>";
			}
			else
			{     
				var role= document.getElementById("role").value;
				var date=document.getElementById("date").value;
				var sundate=document.getElementById("sunDate").value;
				var mondate=document.getElementById("monDate").value;
				var tuedate=document.getElementById("tueDate").value;
				var weddate=document.getElementById("wedDate").value;
				var thudate=document.getElementById("thuDate").value;
				var fridate=document.getElementById("friDate").value;
				var satdate=document.getElementById("satDate").value;
				var jobid=document.getElementById("jobid").value;
				var ccode=document.getElementById("ccode").value;
				var extra = $('#jobid option:selected').attr('data-id'); 
				var certs=document.getElementById("certs").value;
				var shift=document.getElementById("shift").value;
				var total=sunhours+monhours+tuehours+wedhours+thuhours+frihours+sathours;
				if (total == 0)
				{
					$('#timesheet-form').submit();
				}
				else
				{	if(role == 'USR'){var insert="PostLaborD2S";}
					else 
					{
						var ename= document.getElementById("ename").value; 
						if(ename == ''){
							$('#timesheet-form').submit();
						}
						else {
							 var insert="PostLaborD2S";
						}
					}
					
						jQuery.ajax({
							type: "POST",
							dataType: "json",
							url:insert ,
							data: {date: date,sundate: sundate,mondate: mondate,tuedate: tuedate,weddate: weddate,thudate: thudate,fridate: fridate,
							satdate: satdate,jobid:jobid, ccode:ccode, extra:extra, certs:certs,shift:shift,hours:monhours,sunhours:sunhours,tuehours:tuehours,
							wedhours:wedhours,thuhours:thuhours,frihours:frihours,sathours:sathours,ename:ename },
							success:function(data){
								$('#msg').html(data.msg);
								$.ajax({
									type: "POST",
									url: "getalldates",
									dataType: "json",
									data: {ename:ename,sunday:sundate,monday:mondate,tuesday:tuedate,wedday:weddate,thuday:thudate,
									friday:fridate,satday:satdate},
									success:function(data)
									{   
										$('#sun').html(data.sun);
										$('#mon').html(data.mon);
										$('#tue').html(data.tue);
										$('#wed').html(data.wed);
										$('#thu').html(data.thu);
										$('#fri').html(data.fri);
										$('#sat').html(data.sat);
										$('#total').html(data.total);
										
									},
									error: function (){ }
								});
							},
							error:function (){ 
							}
					});
					$('#sun_hours').val("");
					$('#mon_hours').val("");
					$('#tue_hours').val("");
					$('#wed_hours').val("");
					$('#thu_hours').val("");
					$('#fri_hours').val("");
					$('#sat_hours').val("");
					$('#timesheet-form').submit();
					document.getElementById("ename").focus();
					
				}
		}
		});
	});
	
	function validate(key)
	{
		var keycode = (key.which) ? key.which : key.keyCode;
		var sunhours=document.getElementById("sun_hours").value;
		var monhours=document.getElementById("mon_hours").value;
		var tuehours=document.getElementById("tue_hours").value;
		var wedhours=document.getElementById("wed_hours").value;
		var thuhours=document.getElementById("thu_hours").value;
		var frihours=document.getElementById("fri_hours").value;
		var sathours=document.getElementById("sat_hours").value;
		if (!(keycode==8 || keycode==46)&&(keycode < 48 || keycode > 57))
		{
			return false;
		}
		else
		{
			if (sunhours.length < 5)
			{
				return true;
			}
			else if (monhours.length < 5)
			{
				return true;
			}
			else if (tuehours.length < 5)
			{
				return true;
			}
			else if (wedhours.length < 5)
			{
				return true;
			}
			else if (thuhours.length <5)
			{
				return true;
			}
			else if (frihours.length <5)
			{
				return true;
			}
			else if (sathours.length <5)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	// LaborD2E Validation//
	$(function() {
		$("#timesheetD2Eform").validate({
		rules: {
            jobid: "required",
			ccode: "required",
			shift: "required",
			ename:"required"
		},
        messages:
		{
            jobid: "Please select the JobID",
		    ccode: "Please select the CostCode",
		    shift: "Please select the Shifts",
			ename: "Please select the employee"
		},
        submitHandler: function(form)
		{
			return false; 
		}
		});
		 $('#labore').on('click', function() {
		    var sunhours=document.getElementById("sun_hours").value;
			var monhours=document.getElementById("mon_hours").value;
			var tuehours=document.getElementById("tue_hours").value;
			var wedhours=document.getElementById("wed_hours").value;
			var thuhours=document.getElementById("thu_hours").value;
			var frihours=document.getElementById("fri_hours").value;
			var sathours=document.getElementById("sat_hours").value;
			var suntime= sunhours % 0.25;
			var montime= monhours % 0.25;
			var tuetime= tuehours % 0.25;
			var wedtime= wedhours % 0.25;
			var thutime= thuhours % 0.25;
			var fritime= frihours % 0.25;
			var sattime= sathours % 0.25;
			if(suntime != 0 )
			{
			document.getElementById("msg").innerHTML ="<span class='error'>You must enter an amount and between 0.25 and 16 hours in 0.25 increments</span>";
			}
			else if(montime != 0 )
			{
			document.getElementById("msg").innerHTML ="<span class='error'>You must enter an amount and between 0.25 and 16 hours in 0.25 increments</span>";
			}
			else if(tuetime != 0 )
			{
			document.getElementById("msg").innerHTML ="<span class='error'>You must enter an amount and between 0.25 and 16 hours in 0.25 increments</span>";
			}
			else if(wedtime != 0 )
			{
			document.getElementById("msg").innerHTML ="<span class='error'>You must enter an amount and between 0.25 and 16 hours in 0.25 increments</span>";
			}
			else if(thutime != 0 )
			{
			document.getElementById("msg").innerHTML ="<span class='error'>You must enter an amount and between 0.25 and 16 hours in 0.25 increments</span>";
			}
			else if(fritime != 0 )
			{
			document.getElementById("msg").innerHTML ="<span class='error'>You must enter an amount and between 0.25 and 16 hours in 0.25 increments</span>";
			}
			else if(sattime != 0 )
			{
			document.getElementById("msg").innerHTML ="<span class='error'>You must enter an amount and between 0.25 and 16 hours in 0.25 increments</span>";
			}
			else
			{  
		        var role= document.getElementById("role").value;
				var date=document.getElementById("date").value;
				var sundate=document.getElementById("sunDate").value;
				var mondate=document.getElementById("monDate").value;
				var tuedate=document.getElementById("tueDate").value;
				var weddate=document.getElementById("wedDate").value;
				var thudate=document.getElementById("thuDate").value;
				var fridate=document.getElementById("friDate").value;
				var satdate=document.getElementById("satDate").value;
				var jobid=document.getElementById("jobid").value;
				var ccode=document.getElementById("ccode").value;
				var extra = $('#jobid option:selected').attr('data-id'); 
				var ulocal=document.getElementById("ulocal").value;
				var uclass=document.getElementById("uclass").value;
				var certe=document.getElementById("certe").value;
				var shift=document.getElementById("shift").value;
				var total=sunhours+monhours+tuehours+wedhours+thuhours+frihours+sathours;
				if (total == 0)
				{
					$('#timesheetD2Eform').submit();
				}
				else
				{	
			       if(role == 'USR'){var insert="PostLaborD2E";}
					else 
					{
						var ename= document.getElementById("ename").value; 
						if(ename == ''){
							$('#timesheetD2Eform').submit();
						}
						else {
							 var insert="PostLaborD2E";
						}
					} 
					jQuery.ajax({
					type: "POST",
					dataType: "json",
					url: insert,
					data: {date:date, sundate:sundate, mondate:mondate, tuedate:tuedate, weddate:weddate, thudate:thudate, fridate:fridate,
						   satdate:satdate, jobid:jobid, ulocal:ulocal, uclass:uclass, ccode:ccode, extra:extra, certe:certe, shift:shift,
						   hours:monhours, sunhours:sunhours, tuehours:tuehours, wedhours:wedhours, thuhours:thuhours, frihours:frihours,
						   sathours:sathours, ename:ename },
					success:function(data){
						$('#msg').html(data.msg);
						$.ajax({
							type: "POST",
							url: "getalldates",
							dataType: "json",
							data: {ename:ename,sunday:sundate,monday:mondate,tuesday:tuedate,wedday:weddate,thuday:thudate,
							friday:fridate,satday:satdate},
							success:function(data)
							{   
								$('#sun').html(data.sun);
								$('#mon').html(data.mon);
								$('#tue').html(data.tue);
								$('#wed').html(data.wed);
								$('#thu').html(data.thu);
								$('#fri').html(data.fri);
								$('#sat').html(data.sat);
								$('#total').html(data.total);
								
							},
							error: function (){ }
						});
					},
					error:function (){ 
					}
				});
				$('#sun_hours').val("");
				$('#mon_hours').val("");
				$('#tue_hours').val("");
				$('#wed_hours').val("");
				$('#thu_hours').val("");
				$('#fri_hours').val("");
				$('#sat_hours').val("");
				$('#timesheetD2Eform').submit();
				document.getElementById("ename").focus();
			}
							
		}
   });
});
	
$(function () {
$("#checkstubDate").datepicker({
	dateFormat: " mm/dd/yy",
	maxDate:6,
	beforeShowDay: function (date) {
		var day = date.getDay();
		return [day == 6, ''];
	}
});
});	
$(function () {
$("#test").datepicker({
	dateFormat: " mm/dd/yy",
	minDate:  -45,
	maxDate:0,
    onClose: function ()
   {
        this.focus();
    }

	});
});	

$('#test').keydown(function(e) {
  if (e.keyCode == 9){}
  else{
    return false;}
});

$(function () {
$("#startdate").datepicker({
	dateFormat: " mm/dd/yy",
	//minDate:  -45,
	maxDate:0,
    });
});

$(function () {
$("#enddate").datepicker({
	dateFormat: " mm/dd/yy",
	//minDate:  -45,
	maxDate:0,
  });
});

	